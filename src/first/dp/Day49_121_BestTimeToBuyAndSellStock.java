package first.dp;

public class Day49_121_BestTimeToBuyAndSellStock {
    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{7,1,5,3,6,4}));
    }

    public static int maxProfit(int[] prices) {
        if (prices.length <= 1) return 0;
//        return first.dp(prices);
        return dpSpaceForTime(prices);
    }

    // This DP will encode an extra piece of information in the first.array to save the inner j loop of the below
    // Since the inner loop need to look back and determine whether we should sell or not, we will encode this inside the first.array
    // first.dp[] will become a pair of numbers.
    // first.dp[i] = Pair(v1,v2): v1: the maximum profit we could have if we are hold a stock, v2: the maximum value we could have if don't hold a stock
    // Init value: first.dp[0]=(-prices[0], 0)
    // Recurring relations:
    //   first.dp[i] = Pair(
    //       max(first.dp[i-1].v1, -prices[i]), -> either we have been holding in previous day OR we just bought it in day i
    //       max(first.dp[i-1].v2, first.dp[i-1].v1 + prices[i]) -> either we have no stock since previous day OR we just sold it in day i
    //   ) for i in [2, len(prices)-1]
    // Traversal order: i from left to right
    // Return value: the larger number in first.dp[len(prices)-1]
    private static int dpSpaceForTime(int[] prices) {
        int[][] dp = new int[prices.length][2];
        dp[0] = new int[]{-prices[0], 0};

        for (int i = 1; i < prices.length; i++) {
            dp[i] = new int[]{
                    Math.max(dp[i-1][0], -prices[i]),
                    Math.max(dp[i-1][1], dp[i-1][0] + prices[i])
            };
        }

        return Math.max(dp[dp.length-1][0], dp[dp.length-1][1]);
    }

    // first.dp[i]: maximum profit we could achieve if we could consider trading days up to day i
    // Init values: first.dp[0] = 0, first.dp[1] = max(prices[1]-prices[0], 0)
    // Recurring relations:
    //    first.dp[i] = max(
    //        prices[i]-prices[j], -> we will sell on day i, and see which days we could start at
    //        first.dp[j] -> we don't do anything on day i, just use previous days best results
    //    ) for each j in [0, i-1], for each i in [2, len(prices-1)]
    // Traverse order, i from small to big, j from small to big
    // Return first.dp[len(prices)-1]
    private static int dp(int[] prices) {
        int[] dp = new int[prices.length];
        dp[0] = 0; dp[1] = Math.max(prices[1]-prices[0], 0);

        for (int i = 2; i < prices.length; i++) {
            for (int j = 0; j <= i-1; j++) {
                dp[i] = Math.max(dp[i], prices[i]-prices[j]);
                dp[i] = Math.max(dp[i], dp[j]);
            }
        }

        return dp[prices.length-1];
    }


}
