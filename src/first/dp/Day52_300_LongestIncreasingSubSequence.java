package first.dp;

import java.util.Arrays;

public class Day52_300_LongestIncreasingSubSequence {
    public static void main(String[] args) {
        System.out.println(lengthOfLIS(new int[]{10,9,2,5,3,7,101,18}));
    }

    public static int lengthOfLIS(int[] nums) {
        if (nums.length == 0) return 0;
        if (nums.length == 1) return 1;

        return dp(nums);
    }

    // first.dp[i]: The longest increasing sub sequence ending at index i.
    // init value first.dp[i] = 1 for all i in [0, len(nums)-1]
    // first.dp[i] = max(first.dp[i], 1+first.dp[j]) for all j in [0, i-1] AND nums[i] >= nums[j], for all i in [1, len(nums)-1]
    // traversal order, i left to right
    // return maximumValueOf(first.dp[len(nums)-1])
    public static int dp(int[] nums) {
        int[] dp = new int[nums.length];
        for (int i = 0; i < dp.length; i++) {
            dp[i] = 1;
        }

        for (int i = 1; i < dp.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], 1+dp[j]);
                }
            }
        }

        return Arrays.stream(dp).max().getAsInt();
    }
}
