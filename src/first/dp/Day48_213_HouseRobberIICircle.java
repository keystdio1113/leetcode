package first.dp;

public class Day48_213_HouseRobberIICircle {
    public static void main(String[] args) {
        System.out.println(rob(new int[]{2,3,2}));
    }

    public static int rob(int[] nums) {
        if (nums.length == 1) return nums[0];
        if (nums.length == 2) return Math.max(nums[0], nums[1]);
        return dp(nums);

    }

    // first.dp[i]: Maximum value a robber can get if he only robs the house ranging [0,i]
    // Init values: first.dp[0] = nums[0], first.dp[1] = max(nums[0], nums[1])
    // Recurrence relations: first.dp[i] = max(first.dp[i-1], nums[i]+first.dp[i-2]) ranging [2,len(nums)-1]
    // Traverse order: left to right
    // return value: first.dp[len(nums)-1]

    // We will have two DP arrays, 1 where we would rob the first house, so we could only rob from [0, n-2]
    //   Init values: first.dp[0] = nums[0], first.dp[1] = nums[0] (We robbed house-0, so house-1 is not robbale),
    //   Recurrence relations: first.dp[i] = max(first.dp[i-1], nums[i]+first.dp[i-2]) ranging [2,n-2]
    //                         for house n-1, it will equal first.dp[n-2] as the last house is not robable.
    // the other one will not rob the first house, this will allows us to rob from [1, n-1]
    //   Init values: first.dp[0] = 0, first.dp[1] = nums[1] (We did not rob house-0, so house-1 is robbale)
    //   Recurrence relations: first.dp[i] = max(first.dp[i-1], nums[i]+first.dp[i-2]) ranging [2,n-1]
    //
    // Final return max(dp1[n-1], dp2[n-1]
    private static int dp(int[] nums) {
        int[] dp1 = new int[nums.length];
        dp1[0] = nums[0]; dp1[1] = nums[0];
        for (int i = 2; i < nums.length-1; i++) {
            dp1[i] = Math.max(dp1[i-1], nums[i]+dp1[i-2]);
        }
        dp1[dp1.length-1] = dp1[dp1.length-2];

        int[] dp2 = new int[nums.length];
        dp2[0] = 0; dp2[1] = nums[1];
        for (int i = 2; i < nums.length; i++) {
            dp2[i] = Math.max(dp2[i-1], nums[i]+dp2[i-2]);
        }


        return Math.max(dp1[nums.length-1], dp2[nums.length-1]);
    }
}
