package first.dp;

/**
 *
 * https://kamacoder.com/problempage.php?pid=1067
题目描述
    假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
    每次你可以爬至多m (1 <= m < n)个台阶。你有多少种不同的方法可以爬到楼顶呢？
    注意：给定 n 是一个正整数。
输入描述
    输入共一行，包含两个正整数，分别表示n, m
输出描述
    输出一个整数，表示爬到楼顶的方法数。
输入示例
    3 2
输出示例
    3
提示信息
    数据范围：
        1 <= m < n <= 32;
    当 m = 2，n = 3 时，n = 3 这表示一共有三个台阶，m = 2 代表你每次可以爬一个台阶或者两个台阶。
    此时你有三种方法可以爬到楼顶。
        1 阶 + 1 阶 + 1 阶段
        1 阶 + 2 阶
        2 阶 + 1 阶

 */
public class Day45_Kama57_StairClimber {
    public static void main(String[] args) {
        System.out.println(dp(2, 3));
    }


    // This is actually an extension of the stair climbing LC70 question
    // LC70 only allows 1 or 2 steps, here we allow up to m steps, so essentially for each steps we are just looping through
    // more possibilities.
    // 5 steps of DP
    //   first.dp[i]: number of ways to reach step i
    //   Init values: first.dp[0] = 1
    //   Recurring relations: first.dp[i] = sum(i-j >= 0 ? first.dp[i-j] : 0) where j is ranging from [1,m] and i from [1:n]
    //   traversal order: left to right
    //   return value first.dp[n]
    public static int dp(int m, int n) {
        int[] dp = new int[n+1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (i-j >= 0) {
                    dp[i] += dp[i-j];
                }
            }
        }
        return dp[n];
    }
}
