package first.dp;

import java.util.List;

public class Day46_139_WordBreak {
    public static void main(String[] args) {
        System.out.println(wordBreak("bb", List.of("a","b","bbb","bbbb")));
        System.out.println(wordBreak("applepenapple", List.of("apple", "pen")));
        System.out.println(wordBreak("leetcode", List.of("leet", "code")));

    }

    public static boolean wordBreak(String s, List<String> wordDict) {
        return dp1D(s, wordDict);
    }


    // DP 5 steps:
    //   first.dp[j]: whether true or not that we could make up this word for first.string s.substring(0, j)
    //   Init value: first.dp[0] = true
    //   Recurrence relationship:
    //     first.dp[j] = s.substring(j-len(word[i], j) === word[i] && first.dp[j-len(word[i])] for each i, and for each j
    //   Traversal order:
    //     for j outside of for i, small to large
    //   Return value first.dp[len(word)-1][len(s)]
    public static boolean dp1D(String s, List<String> wordDict) {
        boolean[] dp = new boolean[s.length()+1];
        dp[0] = true;
        for (int j = 1; j <= s.length(); j++) {
            for (int i = 0; i < wordDict.size(); i++) {
                String currWord = wordDict.get(i);
                int len = currWord.length();
                if (j >= len && dp[j-len] && currWord.equals(s.substring(j-len,j))) {
                    dp[j] = true;
                    break;
                }
            }
        }
        return dp[s.length()];
    }

    // this is actually a unbounded knapsack problem where we are trying to build up the words to the s.
    // DP 5 steps:
    //   first.dp[i][j] = whether we could build up the s.substring(0, j) considering word in range [0,i]
    //   init value:
    //       first.dp[0][j] = wordDict[0].equals(s.substring(0, j)) for j in range [0, len(s)]
    //       first.dp[i][0] = true
    //   Recurrence relations:
    //      first.dp[i][j] =
    //                 first.dp[i-1][j] OR (Can the word be made up without using the i-th word)
    //
    //      for i range [1, len(word)-1], for j range [0, len(s)]
    //   Traversal order:
    //      row by row, from left to right
    //   Return value: first.dp[len(word)-1][len(s)]
    public static boolean dp(String s, List<String> wordDict) {
        boolean[][] dp = new boolean[wordDict.size()][s.length()+1];

        for (int j = 0; j <= s.length(); j++) {
            dp[0][j] = wordDict.get(0).equals(s.substring(0, j));
        }

        for (int i = 0; i < wordDict.size(); i++) {
            dp[i][0] = true;
        }

        for (int i = 1; i < wordDict.size(); i++) {
            for (int j = 0; j < s.length(); j++) {
                dp[i][j] = dp[i-1][j];

                String currWord = wordDict.get(i);
                int lookBackJ = j - wordDict.get(i).length();
                if (lookBackJ >= 0) {
                    String wordToMatch = s.substring(lookBackJ, j);
                    dp[i][j] |= (currWord.equals(wordToMatch) && dp[i][lookBackJ]);
                }
            }
        }

        return dp[wordDict.size()-1][s.length()];
    }
}
