package first.dp;

public class Day52_718_MaximumLengthOfRepeatedSubArray {
    public static int findLength(int[] nums1, int[] nums2) {
        return dp(nums1, nums2);
    }

    // first.dp[i][j]: The maximum length of repeated continuous subsequence found that is ending in nums1[i] and nums2[j]
    // Init values:
    //     first.dp[0][j] = nums1[0] == nums2[j] ? 1 : 0 for all j in [0, len(nums2)-1]
    //     first.dp[i][0] = nums1[i] == nums2[0] ? 1 : 0 for all i in [0, len(nums1)-1]
    // Recurrence:
    //     first.dp[i-1][j-1]+1 -> if nums[i]==nums[j] for i in [1, len(nums1)-1] and j in [1, len(nums2)-1]
    // Traversal order:
    //     row by row going down in terms of i, inner loop left to right in j.
    // Return value: max(first.dp[i][j]) of every values
    public static int dp(int[] nums1, int[] nums2) {
        int[][] dp = new int[nums1.length][nums2.length];

        for (int i = 0; i < nums1.length; i++) {
            dp[i][0] = (nums1[i] == nums2[0]) ? 1 : 0;
        }

        for (int j = 0; j < nums2.length; j++) {
            dp[0][j] = (nums1[0] == nums2[j]) ? 1 : 0;
        }

        for (int i = 1; i < nums1.length; i++) {
            for (int j = 1; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    dp[i][j] = dp[i-1][j-1] + 1;
                }
            }
        }

        int max = -1;
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                max = Math.max(max, dp[i][j]);
            }
        }
        return max;
    }
}
