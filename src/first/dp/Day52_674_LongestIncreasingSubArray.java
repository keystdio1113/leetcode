package first.dp;

import java.util.Arrays;

public class Day52_674_LongestIncreasingSubArray {

    public static void main(String[] args) {
        System.out.println(findLengthOfLCIS(new int[]{1,3,5,4,7}));
    }
    public static int findLengthOfLCIS(int[] nums) {
        if (nums.length == 0) return 0;
        if (nums.length == 1) return 1;

        return dp(nums);
    }

    // first.dp[i]: The longest increasing continuous sub sequence ending at index i.
    // init value first.dp[i] = 1 for all i in [0, len(nums)-1]
    // first.dp[i] = max(first.dp[i], 1+first.dp[i-1] if nums[i] > nums[i-1]) for all i in [1, len(nums)-1]
    // traversal order, i left to right
    // return maximumValueOf(first.dp[len(nums)-1])
    public static int dp(int[] nums) {
        int[] dp = new int[nums.length];
        for (int i = 0; i < dp.length; i++) {
            dp[i] = 1;
        }

        for (int i = 1; i < dp.length; i++) {
            if (nums[i] > nums[i-1]) {
                dp[i] = Math.max(dp[i], 1+dp[i-1]);
            }
        }

        return Arrays.stream(dp).max().getAsInt();
    }
}
