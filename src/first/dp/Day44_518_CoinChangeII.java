package first.dp;

public class Day44_518_CoinChangeII {
    public static void main(String[] args) {
        System.out.println(change(5, new int[]{1,2,5}));
    }

    public static int change(int amount, int[] coins) {
        return dp(amount, coins);
    }


    // We are modeling this as an unbounded knapsack problem where we could take the same items multiple times
    // DP 5 steps:
    //   first.dp[i][j]: The number of ways to make amount j considering coins from [0..i]
    //   Init values:
    //      first.dp[0][j] = (j is a multiple of value[0]) ? 1 : 0
    //   Recurring relations:
    //      first.dp[i][j] = sum(
    //         first.dp[i-1][j] -> don't use the current coin
    //         first.dp[i][j-weight[i]]] if (j-weight[i]>=0) -> use the current coin
    //      ) for i in range [1,len(coins)), j in range [0, amount]
    //   Traversal order: row by row, and left to right in each row
    //   Return value: first.dp[coins.length-1][amount]
    private static int dp(int amount, int[] coins) {
        int[][] dp = new int[coins.length][amount+1];
        for (int j = 0; j <= amount; j++) {
            dp[0][j] = (j % coins[0] == 0) ? 1 : 0;
        }

        for (int i = 1; i < coins.length; i++) {
            for (int j = 0; j <= amount; j++) {
                dp[i][j] = dp[i-1][j] + (j-coins[i] >= 0 ? dp[i][j-coins[i]] : 0);
            }
        }

        return dp[coins.length-1][amount];
    }
}
