package first.dp;

public class Day48_198_HouseRobber {
    public static int rob(int[] nums) {
        if (nums.length == 1) return nums[0];
        return dp(nums);

    }

    // first.dp[i]: Maximum value a robber can get if he only robs the house ranging [0,i]
    // Init values: first.dp[0] = nums[0], first.dp[1] = max(nums[0], nums[1])
    // Recurrence relations: first.dp[i] = max(first.dp[i-1], nums[i]+first.dp[i-2]) ranging [2,len(nums)-1]
    // Traverse order: left to right
    // return value: first.dp[len(nums)-1]
    private static int dp(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = nums[0]; dp[1] = Math.max(nums[0], nums[1]);
        for (int i = 2; i < nums.length; i++) {
            dp[i] = Math.max(dp[i-1], nums[i]+dp[i-2]);
        }

        return dp[nums.length-1];
    }
}
