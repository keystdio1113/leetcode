package first.dp;

public class Day42_416_PartitionEqualSubsetSum {
    public static void main(String[] args) {
        System.out.println(canPartition(new int[]{1,2,5}));
        System.out.println(canPartition(new int[]{1,5,11,5}));
        System.out.println(canPartition(new int[]{1,2,3,5}));
    }


    public static boolean canPartition(int[] nums) {
//        int sum = 0; // will likely overflow... But the problem for nums[i] is within [0,100] and no more than 200 elements
//        for (int i = 0; i < nums.length; i++) {
//            sum += nums[i];
//        }
//        if (sum % 2 == 1) {
//            return false;
//        } else {
//            return backtrack(nums, sum, 0, 0);
//        }

        return dp(nums);
    }


    // first.dp[i][j]: The maximum value of items that can be achieved with item [0..i] and knapsack capacity of j
    // init values:
    //    first.dp[i][0] = 0 for the whole first column
    //    first.dp[0][j] = (items[0] <= j ? values[0] : 0)
    // recurring relationship:
    //    first.dp[i][j] = max(
    //       first.dp[i-1][j],
    //       first.dp[i-1][j-weight[j]] + value[j] -> only if j-weight[j] >= 0, zero otherwise
    //    ) for every j in range [0, B] and i in range [1,n)
    //  Traversal order:
    //    row by row, from left to right
    // Return value: first.dp[n][B] == B
    public static boolean dp(int[] nums) {
        int sum = 0; // will likely overflow... But the problem for nums[i] is within [0,100] and no more than 200 elements
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        if (sum % 2 == 1) {
            return false;
        }

        int[] weights = nums;
        int[] values = nums;
        int target = sum / 2;
        int[][] dp = new int[nums.length][target+1];
        for (int i = 0; i < nums.length; i++) {
            dp[i][0] = 0;
        }
        for (int j = 0; j <= target; j++) {
            dp[0][j] = (weights[0] <= j ? values[0] : 0);
        }

        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j <= target; j++) {
                dp[i][j] = Math.max(
                        dp[i-1][j],
                        j-weights[i] >= 0 ? dp[i-1][j-weights[i]] + values[i] : 0
                );
            }
        }

        return dp[nums.length-1][target] == target;
    }

    public static boolean backtrack(int[] nums, int totalSum, int sumPart, int currI) {
        if (sumPart*2 == totalSum) {
            return true;
        } else if (sumPart*2 > totalSum){
            return false;
        } else {
            boolean result = false;
            for (int i = currI; i < nums.length; i++) {
                result = result ||
                        backtrack(nums, totalSum, sumPart+nums[i], i+1) || // pick this into partition
                        backtrack(nums, totalSum, sumPart, i+1); // don't pick this into partition
            }
            return result;
        }
    }
}
