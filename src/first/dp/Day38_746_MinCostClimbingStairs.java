package first.dp;

public class Day38_746_MinCostClimbingStairs {
    public int minCostClimbingStairs(int[] cost) {
        return dp(cost);
    }

    /**
     *
     * first.dp[i]: the minimum cost to reach step i, where top is i=len(cost)
     * initial values: first.dp[0]=0, first.dp[1]=0, first.dp[i]=ANY for 2<=i<=len(cost)
     * recurring relations: first.dp[i] = min(cost[i-1] + first.dp[i-1], cost[i-2] + first.dp[i-2]) for 2<=i<=len(cost)
     * traverse order: left to right
     * return value: first.dp[len(cost)]
     */
    public static int dp(int[] cost) {
        int n = cost.length;
        if (n == 0) return 0;
        if (n == 1) return 0;

        int[] dp = new int[n+1];
        for (int i = 2; i <= n; i++) {
            dp[i] = Math.min(cost[i-1]+dp[i-1], cost[i-2]+dp[i-2]);
        }

        return dp[n];
    }
}
