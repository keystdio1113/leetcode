package first.dp;

public class Day51_714_BestTimeToBuyAndSellStockWithTransactionFee {
    public static int maxProfit(int[] prices, int fee) {
        return dpSpaceForTime(prices, fee);
    }

    // This DP will encode an extra piece of information in the first.array to save the inner j loop of the below
    // Since the inner loop need to look back and determine whether we should sell or not, we will encode this inside the first.array
    // first.dp[] will become a pair of numbers.
    // first.dp[i] = Pair(v1,v2): v1: the maximum profit we could have if we are hold a stock, v2: the maximum value we could have if don't hold a stock
    // Init value: first.dp[0]=(-prices[0], 0)
    // Recurring relations:
    //   first.dp[i] = Pair(
    //       max(first.dp[i-1].v1, first.dp[i-1].v2-prices[i]), -> either we have been holding in previous day OR we just bought it in day i
    //       max(first.dp[i-1].v2, first.dp[i-1].v1+prices[i]-fee) -> either we have no stock since previous day OR we just sold it in day i
    //   ) for i in [2, len(prices)-1]
    // Traversal order: i from left to right
    // Return value: the larger number in first.dp[len(prices)-1]
    private static int dpSpaceForTime(int[] prices, int fee) {
        int[][] dp = new int[prices.length][2];
        dp[0] = new int[]{-prices[0], 0};

        for (int i = 1; i < prices.length; i++) {
            dp[i] = new int[]{
                    Math.max(dp[i-1][0], dp[i-1][1] - prices[i]),
                    Math.max(dp[i-1][1], dp[i-1][0] + prices[i] - fee)
            };
        }

        return Math.max(dp[dp.length-1][0], dp[dp.length-1][1]);
    }
}
