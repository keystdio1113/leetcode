package first.dp;

public class Day53_1035_UncrossedLine {
    // this is esstentially 1143, maximum length of repeated subsequence
    public static int maxUncrossedLines(int[] nums1, int[] nums2) {
        return dp(nums1, nums2);
    }

    // first.dp[i][j]: The maximum length of repeated continuous subsequence found that is ending in s1[i] and s2[j]
    // Init values:
    //     first.dp[0][j] = s1[0] == s2[j] ? 1 : 0 for all j in [0, len(s2)-1] (ALSO if the previous element is non zero, take 1)
    //     first.dp[i][0] = s1[i] == s2[0] ? 1 : 0 for all i in [0, len(s1)-1] (ALSO if the previous element is non zero, take 1)
    // Recurrence:
    //     first.dp[i][j] =
    //        if s1[i] == s2[j]: first.dp[i-1][j-1] + 1
    //        else: max(first.dp[i-1][j], first.dp[i][j-1]): Think of this as we are just extending the previous case when we did not consider an extra characters.
    // Traversal order:
    //     row by row going down in terms of i, inner loop left to right in j.
    // Return value: max(first.dp[i][j]) of every values
    public static int dp(int[] nums1, int[] nums2) {
        int max = 0;
        int[][] dp = new int[nums1.length][nums2.length];

        for (int i = 0; i < nums1.length; i++) {
            dp[i][0] = (nums1[i] == nums2[0]) ? 1 : 0;
            if (i >= 1 && dp[i-1][0] == 1) {
                dp[i][0] = 1;
            }
            max = Math.max(dp[i][0], max);
        }

        for (int j = 0; j < nums2.length; j++) {
            dp[0][j] = (nums1[0] == nums2[j]) ? 1 : 0;
            if (j >= 1 && dp[0][j-1] == 1) {
                dp[0][j] = 1;
            }
            max = Math.max(dp[0][j], max);
        }


        for (int i = 1; i < nums1.length; i++) {
            for (int j = 1; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    // imagine we are comparing (c and c are the new elements we want to include)
                    //   abc c
                    //   ab c
                    // We should get the first.dp result of "abc" - "ab" and plus 1 to it.
                    dp[i][j] = dp[i-1][j-1] + 1;
                } else {
                    // imagine we are comparing (c and e are the new elements we want to include)
                    //   abc c
                    //   ab e
                    // We should get the first.dp result of "abcc" - "ab" (i,j-1) and "abc" - "abe" (i-1,j)
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }

                max = Math.max(dp[i][j], max);
            }

        }
        return max;
    }
}
