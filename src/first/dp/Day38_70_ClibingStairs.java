package first.dp;

public class Day38_70_ClibingStairs {
    public int climbStairs(int n) {
        return dp(n);
    }

    /**
     *
     * first.dp[i]: number of unique ways to reach step i, where the top is i=n
     * initial values: first.dp[0]=1, first.dp[1]=1, first.dp[i]=ANY for i>=2
     * recurrence: first.dp[i] = first.dp[i-2]+first.dp[i-1] for i>=2
     * traverse order: from left to right
     * return value: first.dp[n]
     */
    public static int dp(int n) {
        if (n == 0) return 1;
        if (n == 1) return 1;

        int[] dp = new int[n+1];
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }

        return dp[n];
    }
}
