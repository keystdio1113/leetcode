package first.dp;

public class Day60_647_PanlindromicSubstrings {
    public static void main(String[] args) {
        System.out.println(countSubstrings("aaa"));
        System.out.println(countSubstrings("abc"));
    }


    public static int countSubstrings(String s) {
        return dp(s);
    }

    // first.dp[i][j]: substring s[i,j] is a panlindrom or not. (Note i <= j, and boundary is inclusive)
    // Recurring relationship:
    //    first.dp[i][j] =
    //      if s[i] != s[j] -> false
    //      if s[i] == s[j]:
    //         if i == j -> true
    //         if i+1 == j -> true
    //         if i+2 == j -> true
    //         if i+2 < j -> there is at least 2 char between s[i] and s[j], so we would look at if first.dp[i+1][j-1]
    // Init values:
    //    Since first.dp[i][j] depends on value coming from left-bottom direction, so we need to initialize the entire bottom row of the DP table:
    //    and only first.dp[len(s)-1][len(s)-1] = true, the rest are false since the interval value is invalid
    // Traversal order:
    //    Bottom to top, left to right
    // Return value:
    //    Count how many true values in the first.dp table
    public static int dp(String s) {
        if (s.length() == 1) return 1;

        boolean[][] dp = new boolean[s.length()][s.length()];

        dp[s.length()-1][s.length()-1] = true;
        int count = 1;
        for (int i = s.length()-2; i >= 0; i--) {
            for (int j = 0; j < s.length(); j++) {
                if (i <= j) {
                    if (s.charAt(i) == s.charAt(j)) {
                        if (i == j) {
                            count++;
                            dp[i][j] = true;
                        } else if (i+1 == j || i+2 == j) {
                            count++;
                            dp[i][j] = true;
                        } else {
                            if (dp[i+1][j-1]) {
                                count++;
                                dp[i][j] = true;
                            }
                        }
                    }
                }
            }
        }
        return count;
    }
}
