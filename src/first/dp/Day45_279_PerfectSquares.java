package first.dp;

import java.util.ArrayList;
import java.util.List;

public class Day45_279_PerfectSquares {
    public static void main(String[] args) {
        System.out.println(numSquares(12));
        System.out.println(numSquares(13));
        System.out.println(numSquares(100));
        System.out.println(numSquares(101));
    }

    public static int numSquares(int n) {
        return dp(n);
    }

    // make it into an unbounded knapsack problem that figures out what is the minimum amount of stones to fill up the bag.
    // We will first make a list of perfect squares [1,4,9,...] with the last number less than or equal to n
    // Then we will do DP in 5 steps:
    //   first.dp[i][j]: minimum numbers to use considering items [0,i] to add up to a sum of j
    //   init value:
    //      first.dp[0][j] = j for all j in [0, n]. Since all the we can only have number 1, and it takes j number 1 to sum to j.
    //   recurrence:
    //      first.dp[i][j] = min(
    //         first.dp[i-1][j],               -> We don't use the current number
    //         first.dp[i][j-squares[i]] + 1      -> We use the current number
    //      ) for i in range [1, len(squares)-1], and j in range [0, n]
    //  Traversal: Row by row, left to right
    // Return value: first.dp[len(squares)-1][n]
    private static int dp(int n) {
        List<Integer> squares = new ArrayList<>();
        for (int i = 1; true; i++) {
            int square = i*i;
            if (square <= n) {
                squares.add(square);
            } else {
                break;
            }
        }

        int[][] dp = new int[squares.size()][n+1];
        for (int j = 0; j <= n; j++) {
            dp[0][j] = j;
        }

        for (int i = 1; i < squares.size(); i++) {
            for (int j = 0; j <= n; j++) {
                if (j-squares.get(i) >= 0) {
                    dp[i][j] = Math.min(
                            dp[i-1][j],
                            dp[i][j-squares.get(i)] + 1
                    );
                } else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[squares.size()-1][n];
    }
}
