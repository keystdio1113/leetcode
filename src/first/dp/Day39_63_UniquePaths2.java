package first.dp;

public class Day39_63_UniquePaths2 {
    public static void main(String[] args) {
        System.out.println(uniquePathsWithObstacles(new int[][]{
                new int[]{1, 0}
        }));

        System.out.println(uniquePathsWithObstacles(new int[][]{
                new int[]{0, 0, 0},
                new int[]{0, 1, 0},
                new int[]{0, 0, 0}
        }));
    }

    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid[0][0] == 1) return 0;
        return dp(obstacleGrid);
    }

    // 2D first.dp[i][j]: Unique path to reach coordinate (i,j)
    // initialize:
    //   first.dp[0][j] = 1 except for the values to the right of (0, jx) inclusive where jx is an obstacle if exists
    //   first.dp[i][0] = 1 except for the values below of (ix, 0) inclusive where ix is an obstacle if exists
    //   first.dp[i][j] = 0 for every point (i,j) that is an obstacle
    // Recurrence: for any non obstacle point (i,j): first.dp[i][j] = first.dp[i-1][j] + first.dp[i][j-1]
    // Traversal: left to right for each row from top to bottom
    // Return value: first.dp[i][j]
    public static int dp(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;

        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            if (obstacleGrid[i][0] != 1) {
                dp[i][0] = 1;
            } else {
                dp[i][0] = 0;
                break;
            }
        }

        for (int j = 0; j < n; j++) {
            if (obstacleGrid[0][j] != 1) {
                dp[0][j] = 1;
            } else {
                dp[0][j] = 0;
                break;
            }
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (obstacleGrid[i][j] == 1) {
                    dp[i][j] = 0;
                } else {
                    dp[i][j] = dp[i-1][j] + dp[i][j-1];
                }
            }
        }

        return dp[m-1][n-1];
    }
}
