package first.dp;

public class Day55_392_IsSubsequence {
    public static void main(String[] args) {
        System.out.println(isSubsequence("acb", "ahbgdc"));
    }

    public static boolean isSubsequence(String s, String t) {
//        return doublePointer(s, t);

        if (s.length()==0 && t.length()==0) return true;
        if (s.length()==0 && t.length()!=0) return true;
        if (s.length()!=0 && t.length()==0) return false;

        return dp(s, t);
    }

    private static boolean doublePointer(String s, String t) {
        int i = 0; // i stands for "everything before (exclusively) index i is fulfilled"
        for (int j = 0; j < t.length() && i < s.length(); j++) {
            if (s.charAt(i) == t.charAt(j)) {
                i++;
            }
        }
        return i == s.length();
    }

    // first.dp[i][j]: whether the first.string [s0,s1..si] is a subsequence of [t0,t1..tj]
    // Init value:
    //   left top corner: first.dp[0][0] = s0 == t0
    //   first row: first.dp[0][j] -> first.dp[0][j-1] || s0 == tj for j in [1, len(t)-1]
    //   first col: first.dp[i][0] -> false for i in [1, len(s)-1]
    // Recurrence relationship:
    //   first.dp[i][j] = first.dp[i][j-1] OR
    //              (first.dp[i-1][j] AND si is found in t after the value s_i-1)
    //              (first.dp[i-1][j-1] AND si==tj)
    //   ) for i in in [1, len(s)-1], j in [1, len(t)-1]
    // Return value: first.dp[i][j]
    private static boolean dp(String s, String t) {
        boolean[][] dp = new boolean[s.length()][t.length()];

        dp[0][0] = s.charAt(0) == t.charAt(0);
        for (int j = 1; j < t.length(); j++) {
            dp[0][j] = dp[0][j-1] || s.charAt(0) == t.charAt(j);
        }
        for (int i = 1; i < s.length(); i++) {
            dp[i][0] = false; // not necessary but for completeness
        }

        for (int i = 1; i < s.length(); i++) {
            for (int j = 1; j < t.length(); j++) {
                dp[i][j] = dp[i][j-1];
                dp[i][j] |= (dp[i-1][j-1] && s.charAt(i)==t.charAt(j));
                dp[i][j] |= (dp[i-1][j] && isSiFoundInTAfterTheValueSiminus1(s.charAt(i), s.charAt(i-1), t.substring(0, j+1)));
            }
        }

        return dp[s.length()-1][t.length()-1];

    }

    private static boolean isSiFoundInTAfterTheValueSiminus1(char si, char siMinus1, String t) {
        int siMinus1Index = -1;
        for (int i = 0; i < t.length(); i++) {
            if (t.charAt(i) == siMinus1) {
                siMinus1Index = i;
            }
        }
        if (siMinus1Index == -1) throw new IllegalStateException("This should not happened");

        for (int i = siMinus1Index+1; i < t.length(); i++) {
            if (t.charAt(i) == si) {
                return true;
            }
        }
        return false;
    }
}
