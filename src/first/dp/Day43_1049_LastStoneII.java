package first.dp;

import java.util.Arrays;

public class Day43_1049_LastStoneII {
    public static void main(String[] args) {
        System.out.println(lastStoneWeightII(new int[]{2,7,4,1,8,1}));
        System.out.println(lastStoneWeightII(new int[]{31,26,33,21,40}));
    }

    public static int lastStoneWeightII(int[] stones) {
        return dp(stones);
    }

    // Very hard insights. This is a 01 knapsack problem, what we want is to split the stones into two sets as close to
    // sum(stones) / 2 as possible. And use this two sets hit each other and will end up with the smallest value.
    // So this become 01 knapsack problem - maximize value problem, where value[] and weight[] is stones[], and capacity is sum(stones)/2
    public static int dp(int[] stones) {
        int sum = Arrays.stream(stones).sum();

        int B = sum/2;
        int[] weights = stones;
        int[] values = stones;
        int[][] dp = new int[stones.length][B+1];
        for (int j = 0; j <= B; j++) {
            dp[0][j] = (weights[0] <= j) ? values[0] : 0;
        }

        for (int i = 1; i < stones.length; i++) {
            for (int j = 1; j <= B; j++) {
                dp[i][j] = Math.max(
                        dp[i-1][j],
                        j-weights[i]>=0 ? dp[i-1][j-weights[i]]+values[i] : 0
                );
            }
        }

        int maxCapacity = dp[stones.length-1][B];
        int otherSet = sum - maxCapacity;
        return Math.abs(maxCapacity - otherSet);
    }
}
