package first.dp;

public class Day53_1143_MaximumLengthOfRepeatedSubSequence {
    public static void main(String[] args) {
        System.out.println(longestCommonSubsequence("xaxx", "a"));
//        System.out.println(longestCommonSubsequence("hofubmnylkra", "pqhgxgdofcvmr"));
//        System.out.println(longestCommonSubsequence("abcde", "ace"));
//        System.out.println(longestCommonSubsequence("abc", "abc"));
    }

    public static int longestCommonSubsequence(String text1, String text2) {
        return dp(text1, text2);
//        return bruteforce(text1, text2, 0, "");
    }

    // we will first generate every unique 2^len(t1) subsequence in t1, and validate whether it is a subsequence for t2
    // This is a lot of work, but lets see....
    public static int bruteforce(String t1, String t2, int currI, String currS) {
        int max = 0;

        // check the currS if it is a subsequence
        // two pointer approach, and everything before t1I is fulfilled by t2
        int t1I = 0;
        for (int t2I = 0; t2I < t2.length() && t1I < currS.length(); t2I++) {
            if (t2.charAt(t2I) == currS.charAt(t1I)) {
                t1I++;
            }
        }
        boolean isCurrSSubSequenceOfT2 = t1I == currS.length();
        if (isCurrSSubSequenceOfT2) {
            max = Math.max(max, currS.length());
        }

        for (int i = currI; i < t1.length(); i++) {
            int max1 = bruteforce(t1, t2, i+1, currS+t1.charAt(i));
            int max2 = bruteforce(t1, t2, i+1, currS);

            int currMax = Math.max(max1, max2);
            max = Math.max(max, currMax);
        }


        return max;
    }

    // first.dp[i][j]: The maximum length of repeated continuous subsequence found that is ending in s1[i] and s2[j]
    // Init values:
    //     first.dp[0][j] = s1[0] == s2[j] ? 1 : 0 for all j in [0, len(s2)-1] (ALSO if the previous element is non zero, take 1)
    //     first.dp[i][0] = s1[i] == s2[0] ? 1 : 0 for all i in [0, len(s1)-1] (ALSO if the previous element is non zero, take 1)
    // Recurrence:
    //     first.dp[i][j] =
    //        if s1[i] == s2[j]: first.dp[i-1][j-1] + 1
    //        else: max(first.dp[i-1][j], first.dp[i][j-1]): Think of this as we are just extending the previous case when we did not consider an extra characters.
    // Traversal order:
    //     row by row going down in terms of i, inner loop left to right in j.
    // Return value: max(first.dp[i][j]) of every values
    public static int dp(String text1, String text2) {
        int max = 0;
        int[][] dp = new int[text1.length()][text2.length()];

        for (int i = 0; i < text1.length(); i++) {
            dp[i][0] = (text1.charAt(i) == text2.charAt(0)) ? 1 : 0;
            if (i >= 1 && dp[i-1][0] == 1) {
                dp[i][0] = 1;
            }
            max = Math.max(dp[i][0], max);
        }

        for (int j = 0; j < text2.length(); j++) {
            dp[0][j] = (text1.charAt(0) == text2.charAt(j)) ? 1 : 0;
            if (j >= 1 && dp[0][j-1] == 1) {
                dp[0][j] = 1;
            }
            max = Math.max(dp[0][j], max);
        }


        for (int i = 1; i < text1.length(); i++) {
            for (int j = 1; j < text2.length(); j++) {
                if (text1.charAt(i) == text2.charAt(j)) {
                    // imagine we are comparing (c and c are the new elements we want to include)
                    //   abc c
                    //   ab c
                    // We should get the first.dp result of "abc" - "ab" and plus 1 to it.
                    dp[i][j] = dp[i-1][j-1] + 1;
                } else {
                    // imagine we are comparing (c and e are the new elements we want to include)
                    //   abc c
                    //   ab e
                    // We should get the first.dp result of "abcc" - "ab" (i,j-1) and "abc" - "abe" (i-1,j)
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }

                max = Math.max(dp[i][j], max);
            }

        }
        return max;
    }
}
