package first.dp;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class Day41_343_IntegerBreak {
    private static int[] memo;


    public static void main(String[] args) {
        System.out.println(integerBreak(31));
//        System.out.println(integerBreak(2));
//        System.out.println(integerBreak(10));
    }

    public static int integerBreak(int n) {
//        return first.dp(n);
//        return backtrace(n);
        return backtraceWithCache(n, new HashMap<>());
    }

    // first.dp[i]: the maximum product for integer i when splitting integer i
    // init value: first.dp[0]=NOT_USED, first.dp[1]=1, first.dp[2]=1
    // recurring relations:
    //   first.dp[i] = max(j*first.dp[i-j], j*(i-j)) for j ranging from [1,i-1], i ranging from [3,n]))
    //   The first product is we are breaking j into further level, while the second product is taking original.
    //   an example would be first.dp[6] = max(1*5, 1*first.dp[5], 2*3,2*first.dp[3], 3*3, 3*first.dp[3]), note the maximum value is actually 3*3.
    //   we don't always have to break it further.
    // Traversal order: left to right
    // Return value: first.dp[n]
    private static int dp(int n) {
        int[] dp = new int[n+1]; // since we want first.dp[n] to be the last element, so +1
        dp[1] = 1; dp[2] = 1;
        for (int i = 3; i <= n; i++) {
            int maxProduct = -1;
            for (int j = 1; j <= i-1; j++) {
                int prod1 = j * (i-j);
                int prod2 = j*dp[i-j];

                maxProduct = Math.max(maxProduct, prod1);
                maxProduct = Math.max(maxProduct, prod2);
            }
            dp[i] = maxProduct;
        }

        return dp[n];
    }

    private static int backtrace(int n) {
        if (n == 1) return 1;
        if (n == 2) return 1;
        if (n > 2) {
            int max = -1;
            for (int i = 1; i <= n-1; i++) {
                int j = n-i;
                int prod1 = i*j;
                int prod2 = i*backtrace(j);

                max = Math.max(max, prod1);
                max = Math.max(max, prod2);
            }
            return max;
        }

        throw new IllegalStateException("This should not happened");
    }

    private static int backtraceWithCache(int n, Map<Integer, Integer> cache) {
        if (n == 1) return 1;
        if (n == 2) return 1;

        if (cache.containsKey(n)) {
            return cache.get(n);
        }

        if (n > 2) {
            int max = -1;
            for (int i = 1; i <= n-1; i++) {
                int j = n-i;
                int prod1 = i*j;
                int prod2 = i*backtraceWithCache(j, cache);

                max = Math.max(max, prod1);
                max = Math.max(max, prod2);
            }
            cache.put(n, max);
            return max;
        }

        throw new IllegalStateException("This should not happened");
    }


//    public static int integerBreak(int n) {
//        if (n <= 3) {
//            return n - 1;
//        }
//
//        memo = new int[n + 1];
//        return dpLeetcode(n);
//    }

    public static int dpLeetcode(int num) {
        if (num <= 3) {
            return num;
        }

        if (memo[num] != 0) {
            return memo[num];
        }

        int ans = num;
        for (int i = 2; i < num; i++) {
            ans = Math.max(ans, i * dpLeetcode(num - i));
        }

        memo[num] = ans;
        return ans;
    }
}
