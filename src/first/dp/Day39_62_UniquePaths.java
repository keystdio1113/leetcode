package first.dp;

public class Day39_62_UniquePaths {
    public static void main(String[] args) {
        System.out.println(uniquePaths(3, 7));
        System.out.println(uniquePaths(3, 2));
    }

    public static int uniquePaths(int m, int n) {
        return dp(m, n);
    }

    // we would need a two-d first.dp first.array of size m, n (including 0)
    // first.dp[i][j]: stands for number of unique ways to reach coordinates (i,j)
    // initial values:
    //    first.dp[0][j], the first row is all 1
    //    first.dp[i][0], the first column is all 1, other values could have ANY value
    // Recurrence:
    //    first.dp[i][j] = first.dp[i-1][j] + first.dp[i][j-1]
    // Traverse order:
    //    fill in first.dp[1][j] from left to right, and then first.dp[2][j] left to right.... all the way to first.dp[m][n]
    // Return value:
    //    first.dp[m][n]
    public static int dp(int m, int n) {
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            dp[i][0] = 1;
        }

        for (int j = 0; j < n; j++) {
            dp[0][j] = 1;
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }
}
