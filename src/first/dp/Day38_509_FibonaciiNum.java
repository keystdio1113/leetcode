package first.dp;

public class Day38_509_FibonaciiNum {
    // Steps:
    //   fib[i] meaning: The i-th fibonacii number
    //   initial values: fib[0] = 0, fib[1] = 1, fib[i] = [ANY_VALUE_DOESNT_MATTER]
    //   recurrence: fib[i] = fib[i-2] + fib[i-1] for 2 <= i <= n
    //   traversal order: left to right of first.array
    //   return value: fib[n]
    public static int fib(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;

        int[] F = new int[n+1];
        F[0] = 0;
        F[1] = 1;
        for (int i = 2; i <= n; i++) {
            F[i] = F[i-1] + F[i-2];
        }
        return F[n];
    }
}
