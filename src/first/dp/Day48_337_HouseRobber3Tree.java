package first.dp;

import model.TreeNode;
import first.utils.Pair;

import java.util.Map;

public class Day48_337_HouseRobber3Tree {
    public static int rob(TreeNode root) {
//        return backtrackWithMemoization(root, new HashMap<>());
        Pair<Integer, Integer> pair = dp(root);
        return Math.max(pair.k, pair.v);
    }

    // We will have each node return a status of Pair(val1, val2) where:
    //   val1: maximum value we could get if we ROB current node
    //   val2: maximum value we could get if we DO NOT ROB current node.
    //
    private static Pair<Integer, Integer> dp(TreeNode root) {
        if (root == null) {
            return new Pair<>(0, 0);
        } else {
            Pair<Integer, Integer> left = dp(root.left);
            Pair<Integer, Integer> right = dp(root.right);

            int val1 = root.val + left.v + right.v;
            int val2 = Math.max(left.k, left.v) + Math.max(right.k, right.v);

            return new Pair<>(val1, val2);
        }
    }

    private static int backtrackWithMemoization(TreeNode root, Map<TreeNode, Integer> cache) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return root.val;
        if (cache.containsKey(root)) return cache.get(root);

        // at each stage, we could either rob the current house (Which means we will be considering robbing the grandchild node)
        // or don't rob the current, and consider robbing the child node
        int robCurrentValue = root.val;
        if (root.left != null) {
            robCurrentValue += backtrackWithMemoization(root.left.left, cache) + backtrackWithMemoization(root.left.right, cache);
        }
        if (root.right != null) {
            robCurrentValue += backtrackWithMemoization(root.right.left, cache) + backtrackWithMemoization(root.right.right, cache);
        }


        int doNotRobCurrentValue = backtrackWithMemoization(root.left, cache) + backtrackWithMemoization(root.right, cache);
        int result = Math.max(robCurrentValue, doNotRobCurrentValue);
        cache.put(root, result);
        return result;
    }
}
