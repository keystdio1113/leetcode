package first.dp;

public class Day60_516_LongestPalindromicSubsequence {
    public static void main(String[] args) {
        System.out.println(longestPalindromeSubseq("aaa"));
    }

    public static int longestPalindromeSubseq(String s) {
        return dp(s);
    }


    // first.dp[i[j]: The maximum palindrom subsequence found between s[i, j] (Inclusive bound, i <= j)
    // recurrence relationship:
    //    first.dp[i][j] = if (s[i] == s[j]) ->
    //                   i == j: 1
    //                   i+1 == j: 2
    //                   i+2 == j: 3
    //                   else: first.dp[i+1][i-1] + 2
    //               if (s[i] != s[j]) -> max(first.dp[i][j-1], first.dp[i+1][j-1], first.dp[i+1][j]): We either consider the new s[i] or the new s[j]
    //    for i in (len(s)-2 -> 0) and j in (i+1 -> len(s)-1)
    // Init value:
    //    first.dp[i][j] comes from left-bottom, bottom, and left
    //    so we need to initialize the bottom row, and we could only initialize:
    //        first.dp[len(s)-1][len(s)-1] = 1 and everything else has to be zero because the interval boundary doesn't make sense
    // Traverse order:
    //    bottom to top, left to right
    // Return value:
    //    Max value in the DP table
    public static int dp(String s) {
        int[][] dp = new int[s.length()][s.length()];

        int max = 1;
        dp[s.length()-1][s.length()-1] = 1;

        for (int i = s.length()-2; i >= 0; i--) {
            for (int j = i; j < s.length(); j++) {
                if (s.charAt(i) == s.charAt(j)) {
                    if (i == j) {
                        dp[i][j] = 1;
                    } else if (i+1 == j) {
                        dp[i][j] = 2;
                    } else if (i+2 == j) {
                        dp[i][j] = 3;
                    } else {
                        dp[i][j] = dp[i+1][j-1] + 2;
                    }
                } else {
                    dp[i][j] = Math.max(dp[i][j], dp[i][j-1]);
                    dp[i][j] = Math.max(dp[i][j], dp[i+1][j-1]);
                    dp[i][j] = Math.max(dp[i][j], dp[i+1][j]);
                }

                max = Math.max(max, dp[i][j]);
            }
        }

        return max;
    }
}
