package first.dp;

public class Day55_115_DistinctSubsequence {
    public static void main(String[] args) {
        System.out.println(numDistinct("rabbbit", "rabbit"));
    }

    public static int numDistinct(String s, String t) {
        return bruteforce(s, t, 0, 0, "");
    }

    public static int bruteforce(String s, String t, int decisionMade, int currI, String currString) {
        // I made a mistake of returning value at each of the recursion tree node
        // But actually I need to do it at the leaft node, so add in the decisionMade clause
        if (currString.length() > t.length()) {
            return 0;
        }
        if (currString.equals(t) && decisionMade == s.length()) {
            return 1;
        } else {
            int total = 0;
            for (int i = currI; i < s.length(); i++) {
                total += bruteforce(s, t, decisionMade+1, i+1, currString + s.charAt(i));
                total += bruteforce(s, t, decisionMade+1, i+1, currString);
            }
            return total;
        }
    }

    // first.dp[i][j]: Number of distinct subsequences that can be formed using [s0,s1,..,si] that is [t0,t1,..,tj]
//    public int first.dp(String s, String t) {
//
//    }
}
