package first.dp;

public class Day44_377_CombinationsSum4 {
    public static void main(String[] args) {
        System.out.println(combinationSum4(new int[]{1,2,3}, 4));
    }

    public static int combinationSum4(int[] nums, int target) {
        return dp1D(nums, target);
    }

    private static int dp(int[] nums, int target) {
        int[][] dp = new int[nums.length][target+1];
        for (int j = 0; j <= target; j++) {
            dp[0][j] = (j % nums[0] == 0) ? 1 : 0;
        }

        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j <= target; j++) {
                dp[i][j] = dp[i-1][j] + (j-nums[i] >= 0 ? dp[i][j-nums[i]] : 0);
            }
        }

        return dp[nums.length-1][target];
    }

    // We will try to solve it using 1D first.array as 2D first.array is giving me tons of difficulties
    // Five steps of DP:
    //   first.dp[j]: number of unique ways to sum up to j.
    //   Initialization: first.dp[0]=1, there is only one way to get sum of 0, take non of the numbers
    //   Recurrence relations:
    //     first.dp[j] = sum(first.dp[j-nums[i]]) for every number i. One example would be, take the input [1,2,3] and target 4.
    //        first.dp[0] = 1
    //        first.dp[1] = first.dp[0] + first.dp[-1] + first.dp[-2] = 1 -> [1]
    //        first.dp[2] = first.dp[1] + first.dp[0] + first.dp[-1] = 2 -> [1,1], [2]
    //        first.dp[3] = first.dp[2] + first.dp[1] + first.dp[0] = 4 -> [1,1,1],[2,1], [1,2], [3]
    //        first.dp[4] = first.dp[3] + first.dp[2] + first.dp[1] = 7 -> [1,1,1,1],[2,1,1],[1,2,1],[3,1] | [1,1,2],[2,2] | [1,3]
    //     We will add up the number of ways to build the previous capacity based on the current items
    //    Scan from left to right.
    //    return the last element first.dp[target]
    private static int dp1D(int[] nums, int target) {
        int[] dp = new int[target + 1];
        dp[0] = 1;

        for (int j = 1; j <= target; j++) {
            for (int num : nums) {
                if (j-num >= 0) {
                    dp[j] += dp[j-num];
                }
            }
        }

        return dp[target];
    }
}
