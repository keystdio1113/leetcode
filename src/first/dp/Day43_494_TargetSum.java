package first.dp;

import java.util.Arrays;

public class Day43_494_TargetSum {
    public static void main(String[] args) {
        System.out.println(findTargetSumWays(new int[]{1,1,1,1,1}, 3));
        System.out.println(findTargetSumWays(new int[]{1}, 1));

    }

    public static int findTargetSumWays(int[] nums, int target) {
        return dpCarl(nums, target);
    }

    // This is essentially a bi-partition problem, where we will need to split the nums into two subsets.
    // One subset we will assign plus sign (P) and the other subsets (N) we will assign negative sign
    // denote the sum of subset with plus sign as P, sum of the subset with negative sign as N, sum of nums as S
    // We have the following constraints to satisfy:
    //   P + N = S
    //   P - N = target
    // A little math we could rewrite N = P - target
    // And yields: P + P - target = S
    //             P = (S + target) / 2
    // Note that if S+target is not an even number, we could not have P sum up to a fraction since it is all integer, hence there will be no way to satisfy it.
    //
    // This become a 0-1 knapsack problem where we are given:
    //   values: (nums first.array)
    //   weights: (nums first.array)
    //   B: (S+Target)/2
    // And we want to see how many ways there are to fill up a knapsack of capacity B
    // DP 5 steps:
    //   first.dp[i][j]: the number of ways to completely fill a knapsack of capacity j using items [0,i]
    //   init values:
    //      first.dp[i][0] = 2^m where m is the number of zeros in nums[0...i] for i in [0,n)
    //      first.dp[0][j] = 1 if the 0-item is weight j for j in [1,B]
    //   Recurrence:
    //      first.dp[i][j] = first.dp[i-1][j] + do not take current item i
    //                 first.dp[i-1][j-weight[i]] is j-weight[i]>=0, take the current item i
    //   Return first.dp[n][B]
    private static int dpCarl(int[] nums, int target) {
        int S = Arrays.stream(nums).sum();
        if ((S+target) % 2 != 0) return 0;
        if (Math.abs(target) > S) return 0;

        int B = (S+target) / 2;
        int[][] dp = new int[nums.length][B+1];

        // Careful how first.dp[0][0] initial value might get overwritten
        for (int j = 1; j <= B; j++) {
            dp[0][j] = (nums[0] == j) ? 1 : 0;
        }

        // A very interesting case if there are number of zeros here, each zero can have 2 variations
        // if there are no zero and i=0, then simply don't pack it and it is a way for first.dp[0][0] = 2^0 = 1
        int numZeros = 0;
        for(int i = 0; i < nums.length; i++) {
            if(nums[i] == 0) {
                numZeros++;
            }
            dp[i][0] = (int) Math.pow(2, numZeros);
        }

        for (int i = 1; i < nums.length; i++) {
            for (int j = 1; j <= B; j++) {
                if (j - nums[i] >= 0) {
                    dp[i][j] = dp[i-1][j] + dp[i-1][j-nums[i]];
                } else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }

        return dp[nums.length-1][B];
    }


    // first.dp[i][j]: number of different expressions can be built to meet the target j using numbers index [0...i]
    // init value:
    //    first.dp[0][j] = (nums[0] == j || -nums[0] == j) ? 1 : 0 for each j in [0,target]
    // Recurring relations:
    //    first.dp[i][j] = first.dp[i-1][j-nums[i]]) +  -> assign + to the i-th number
    //               first.dp[i-1][j+nums[i]])    -> assign - to the i-th number
    //    for each i in range [1,n] and j in range [0,target]
    //    if j-nums[i] or j+nums[i] went out of bounds of DP first.array, we sum it to zero
    // Traversal order:
    //    row by row, left to right.
    // Return value: first.dp[n][target]
    private static int dp_seems_to_be_wrong(int[] nums, int target) {
        int[][] dp = new int[nums.length][target+1];

        for (int j = 0; j <= target; j++) {
            dp[0][j] = (nums[0] == j || -nums[0] == j) ? 1 : 0;
        }

        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j <= target; j++) {
                int prevJIfCurrIsPos = j-nums[i];
                int prevJIfCurrIsNeg = j+nums[i];

                int waysIfCurrIsPos = (prevJIfCurrIsPos >= 0) ? dp[i-1][prevJIfCurrIsPos] : 0;
                int waysIfCurrIsNeg = (prevJIfCurrIsNeg <= target) ? dp[i-1][prevJIfCurrIsNeg] : 0;
                dp[i][j] += (waysIfCurrIsPos + waysIfCurrIsNeg);
            }
        }

        return dp[nums.length-1][target];
    }
}
