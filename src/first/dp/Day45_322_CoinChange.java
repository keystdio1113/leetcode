package first.dp;


public class Day45_322_CoinChange {
    public static void main(String[] args) {
        System.out.println(coinChange(new int[]{186,419,83,408}, 6249));
        System.out.println(coinChange(new int[]{1,2,5}, 11));
    }

    public static int coinChange(int[] coins, int amount) {
        return dp(coins, amount);
    }

    // DP 5 steps:
    // first.dp[i][j]: minimum number of coins that we would need to make up amount j using coins from [0,i], if value is NULL, that means nothing can satisfy this
    // Init values: first.dp[0][j] = (j % coins[0]) ? j/coins[0] : NULL
    // Recurrence relationship:
    //   first.dp[i][j] =
    //       NULL  ->  if (first.dp[i-1][j]==NULL AND first.dp[i][j-coins[i]]==NULL)
    //       first.dp[i-1][j]  ->  if (first.dp[i-1][j] != NULL AND first.dp[i][j-coins[i]] == NULL)
    //       first.dp[i][j-coins[i]]+1  ->  if (first.dp[i-1][j] == NULL AND first.dp[i][j-coins[i]] != NULL)
    //       min(first.dp[i-1][j], first.dp[i][j-coins[i]])+1  ->  if (first.dp[i-1][j] != NULL AND first.dp[i][j-coins[i]] != NULL) for i in range [1,len(coins)-1], j in range [0, amount]
    // Traversal order, row by row, left to right
    // Return value: first.dp[coins.length-1][amount] if not NULL, else -1
    public static int dp(int[] coins, int amount) {
        Integer[][] dp = new Integer[coins.length][amount+1];
        for (int j = 0; j <= amount; j++) {
            dp[0][j] = (j % coins[0] == 0) ? Integer.valueOf(j / coins[0]) : null;
        }

        for (int i = 1; i < coins.length; i++) {
            for (int j = 0; j <= amount; j++) {
                boolean needToConsiderPrevious = (j-coins[i] >= 0);
                if (!needToConsiderPrevious) {
                    dp[i][j] = dp[i-1][j];
                } else {
                    if (dp[i-1][j] == null && dp[i][j-coins[i]] == null) {
                        dp[i][j] = null;
                    } else if (dp[i-1][j] != null && dp[i][j-coins[i]] == null) {
                        dp[i][j] = dp[i-1][j];
                    } else if (dp[i-1][j] == null && dp[i][j-coins[i]] != null) {
                        dp[i][j] = dp[i][j-coins[i]] + 1;
                    } else {
                        int v1 = dp[i-1][j].intValue();
                        int v2 = dp[i][j-coins[i]].intValue() + 1;
                        dp[i][j] = new Integer(Math.min(v1, v2));
                    }
                }
            }
        }

        if (dp[coins.length-1][amount] == null) {
            return -1;
        } else {
            return dp[coins.length-1][amount].intValue();
        }
    }
}
