package first.dp;

public class Day41_96_UniqueBinarySearchTrees {
    public static void main(String[] args) {
        System.out.println(numTrees(3));
        System.out.println(numTrees(1));
    }

    public static int numTrees(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        if (n == 2) return 2;

        return dp(n);
    }

    // first.dp[i]: number of unique BST using i nodes
    // init value: first.dp[0]=1 (empty BST), first.dp[1]=1, first.dp[2]=2
    // Recurring relations example: we have 3 nodes (1,2,3), hence there could three different root node options
    //   rootNode=1, 0-node left subtree, 2-node right subtree, so this subtree has first.dp[0]*first.dp[2] unique count.
    //   rootNode=2, 1-node left subtree, 1-node right subtree, so this subtree has first.dp[1]*first.dp[1] unique count.
    //   rootNode=3, 2-node left subtree, 0-node right subtree, so this subtree has first.dp[2]*first.dp[0] unique count.
    //   Note it doesn't matter what number of nodes left/right subtree has, since we are splitting the number range in the middle, it is sorted and that is fine.
    //   Abstractly, that means first.dp[3] is the same value whether it is (1,2,3) or (4,5,6) or (7,8,9)
    // Recurring relations then it is:
    //   first.dp[i]=sum(first.dp[j]*first.dp[i-1-j]) for j ranging from [0,i-1] and i ranging from [3,n]
    // Traverse order is 0 -> n
    // Return final value first.dp(n)
    public static int dp(int n) {
        int[] dp = new int[n+1];
        dp[0] = 1; dp[1] = 1; dp[2] = 2;

        for (int i = 3; i <= n; i++) {
            for (int j = 0; j <= i-1; j++) {
                dp[i] += (dp[j] * dp[i-1-j]);
            }
        }

        return dp[n];
    }
}
