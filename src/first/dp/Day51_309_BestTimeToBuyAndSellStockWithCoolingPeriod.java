package first.dp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Day51_309_BestTimeToBuyAndSellStockWithCoolingPeriod {
    public static int maxProfit(int[] prices) {
        return dp(prices);
//        return bruteforce(prices, 0, -1, 0, false, new HashMap<>());
    }

    // first.dp[i] = (
    //    maxProfitIfWeAreHolding,
    //    maxProfitIfWeAreNotHoldingButDidNotSellToday,
    //    maxProfitIfWeSoldToday,
    //    maxProfitIfWeAreOnCooldownToday
    // first.dp[0] = (-prices[0], 0, 0, 0)
    // first.dp[i] = (
    //     max(first.dp[i-1][0], first.dp[i-1][1]-prices[i], first.dp[i-1][3]-prices[i]),
    //     max(first.dp[i-1][1], first.dp[i-1][0]+prices[i]),
    //     max(first.dp[i-1][2], first.dp[i-1][0]+prices[i]),
    //     max(first.dp[i-1][2])
    // )
    // left to right
    // return the largest value in the last tuple first.dp[n][0] and first.dp[n][1]
    public static int dp(int[] prices) {
        int[][] dp = new int[prices.length][4];
        dp[0] = new int[]{-prices[0], 0, 0, 0};
        for (int i = 1; i < prices.length; i++) {
            dp[i] = new int[]{
                    Math.max(dp[i-1][0], Math.max(dp[i-1][1]-prices[i], dp[i-1][3]-prices[i])),
                    Math.max(dp[i-1][1], dp[i-1][3]),
                    dp[i-1][0]+prices[i],
                    dp[i-1][2]
            };
        }

        return Arrays.stream(dp[prices.length-1]).max().getAsInt();
    }

    public static int bruteforce(int[] prices, int currI, int previousSell, int currProfit, boolean isHolding, Map<String, Integer> cache) {
        String key = String.format("%s:%s:%s:%s", currI, previousSell, currProfit, isHolding);
        if (cache.containsKey(key)) return cache.get(key);
        int max = currProfit;

        for (int i = currI; i < prices.length; i++) {
            // buy if we could...
            if (!isHolding && (previousSell == -1 || currI-1 > previousSell)) {
                max = Math.max(max, bruteforce(prices, i+1, i, currProfit-prices[i], true, cache));
            }

            // sell if we could
            if (isHolding) {
                max = Math.max(max, bruteforce(prices, i+1, i, currProfit+prices[i], false, cache));
            }

            // or do nothing.
            max = Math.max(max, bruteforce(prices, i+1, previousSell, currProfit, isHolding, cache));
        }

        cache.put(key, max);
        return max;
    }
}
