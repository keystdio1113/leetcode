package first.dp;

public class Day43_474_OnesAndZeros {
    public static void main(String[] args) {
        System.out.println(findMaxForm(new String[]{"10","0","1"}, 1, 1));
        System.out.println(findMaxForm(new String[]{"10","0001","111001","1","0"}, 5, 3));
    }

    public static int findMaxForm(String[] strs, int m, int n) {
        return dp(strs, m, n);
    }

    // this can actually be made into a knapsack problem while there are two kinds of capacity to be worried about
    // capacity of 0s, and capacity of 1s.
    // We will make two different weights denoting each items with how many 0 and 1s they have: w0, w1
    // We will define the DP:
    //   first.dp[i][j][k]: The maximum number of items we can put in if we are using item [0-i], with a 0 capacity of j and 1 capacity of k
    //   Init values:
    //      first.dp[0][j][k] = (w0[0] <= j && w1[0] <= k) ? 1 : 0 for j in [0,m] and k in [0,n]
    //      first.dp[i][0][0] = 0 for i in range [1,len(strs)]
    //   Recurring relations:
    //      first.dp[i][j][k] = max(
    //         first.dp[i-1][j][k],
    //         first.dp[i-1][j-w0[i]][j-w1[i]] + 1 if (j-w0[i]>=0 && j-w1[i]>=0)
    //      ) for i in range [1,len(strs)), j in range [0,m], k in range [0,n]
    //   Traversal order, 3-nested for loops, i, j, k, small to large in all level
    //   Return first.dp[len(strs)-1][m][n]
    public static int dp(String[] strs, int m, int n) {
        int[] w0 = new int[strs.length];
        int[] w1 = new int[strs.length];

        for (int i = 0; i < strs.length; i++) {
            String curr = strs[i];
            int zeroCount = 0;
            int oneCount = 0;
            for (int c = 0; c < curr.length(); c++) {
                if (curr.charAt(c) == '0') zeroCount++;
                else oneCount++;
            }
            w0[i] = zeroCount;
            w1[i] = oneCount;
        }

        int[][][] dp = new int[strs.length][m+1][n+1];
        for (int j = 0; j <= m; j++) {
            for (int k = 0; k <= n; k++) {
                dp[0][j][k] = (w0[0] <= j && w1[0] <= k) ? 1 : 0;
            }
        }

        for (int i = 1; i < strs.length; i++) {
            for (int j = 0; j <= m; j++) {
                for (int k = 0; k <= n; k++) {
                    dp[i][j][k] = Math.max(
                            dp[i-1][j][k],
                            (j-w0[i]>=0 && k-w1[i]>=0) ? dp[i-1][j-w0[i]][k-w1[i]] + 1 : 0
                    );
                }
            }
        }


        return dp[strs.length-1][m][n];
    }
}
