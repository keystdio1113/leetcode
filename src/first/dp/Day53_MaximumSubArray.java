package first.dp;

public class Day53_MaximumSubArray {
    public static int maxSubArray(int[] nums) {
        if (nums.length == 0) return 0;
        return dp(nums);
    }

    // first.dp[i]: Maximum sum for the subarray that is ending on index i
    // init value first.dp[0]: nums[0]
    // Recurrence relationship:
    //   first.dp[i] = max(
    //      nums[i] -> we don't take any of the previous elements, and just use the single number
    //      first.dp[i-1]+nums[i] -> we consider the previous elements
    //   ) for i in [1, len(nums)-1]
    // Traversal order for i is from small to large
    // Return value: maximum value within first.dp[]
    public static int dp(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = nums[0];

        int max = dp[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.max(nums[i], dp[i-1]+nums[i]);
            max = Math.max(dp[i], max);
        }

        return max;
    }
}
