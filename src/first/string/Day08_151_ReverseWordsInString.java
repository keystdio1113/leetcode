package first.string;

public class Day08_151_ReverseWordsInString {
    public static void main(String[] args) {
        System.out.println(reverseNoExtraSpace("  hello world  "));

    }

    public static String reverseNoExtraSpace(String s) {
        char[] chars = s.toCharArray();

        reverseCharsInRange(chars, 0, chars.length-1);

        int i = 0;

        while (i < s.length()) {
            while (i<s.length() && chars[i] == ' ') i++;
            int k = i;
            while (k<s.length() && chars[k] != ' ') k++;
            reverseCharsInRange(chars, i, k-1);
            i = k;
        }

        // now we could have leading spaces, 1+ spaces between words, trailing spaces
        // let's do in place space removal

        int slow = 0; // slow denotes the beginning of valid index
        // invariant check: fast >= slow
        for (int fast = 0; fast < chars.length; fast++) {
            if (chars[fast] != ' ') {
                chars[slow] = chars[fast];
                slow++;
            }

            if (fast > 0 && chars[fast] == ' ' && chars[fast-1] != ' ') {
                chars[slow] = chars[fast];
                slow++;
            }
        }

        return new String(chars).substring(0, slow).trim();
    }

    // Inclusive range
    private static void reverseCharsInRange(char[] ch, int i, int j) {
        if (j - i <= 0) return;
        while (j - i > 0) {
            char tmp = ch[j];
            ch[j] = ch[i];
            ch[i] = tmp;

            j--;
            i++;
        }
    }

    public static String solutionMultiSpaceRegex(String s) {
        String[] split = s.trim().split("\\s+"); // need to trim first to get rid of trailing and leading whitespaces first
        StringBuilder sb = new StringBuilder();
        for (int i = split.length-1; i >= 0; i--) {
            sb.append(split[i]);
            sb.append(" ");
        }
        return sb.substring(0, sb.length()-1);
    }

    public static String solution(String s) {
        String[] split = s.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = split.length-1; i >= 0; i--) {
            if (split[i].length() > 0) {
                sb.append(split[i]);
                sb.append(" ");
            }
        }
        return sb.substring(0, sb.length()-1);
    }
}
