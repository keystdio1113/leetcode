package first.string;

public class Day08_541_ReverseStringII {
    public static void main(String[] args) {
        System.out.println(cleanerSolution("abcdefg", 2));
        System.out.println(cleanerSolution("abcd", 2));

    }

    public static String cleanerSolution(String s, int k) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i += 2*k) {
            int firstK = i + k;
            int secondK = i + 2*k;
            if (secondK <= s.length()) {
                addReverseString(sb, s, i, firstK);
                addForwardString(sb, s, firstK, secondK);
            } else {
                if (firstK <= s.length()) {
                    addReverseString(sb, s, i, i+k);
                    addForwardString(sb, s, i+k, s.length());
                } else {
                    addReverseString(sb, s, i, s.length());
                }
            }
        }
        return sb.toString();
    }

    public static String solution(String s, int k) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i <= s.length()-2*k) {
            int j = i + 2*k;
            addReverseString(sb, s, i, i+k);
            addForwardString(sb, s, i+k, i+2*k);
            i = j;
        }

        int remaining = s.length()-i;
        if (remaining < k) {
            addReverseString(sb, s, i, s.length());
        } else if (remaining >= k && remaining < 2*k) {
            addReverseString(sb, s, i, i+k);
            addForwardString(sb, s, i+k, i+remaining);
        }

        return sb.toString();
    }

    // @end is exclusive bound
    private static void addReverseString(StringBuilder sb, String s, int start, int end) {
        for (int i = end-1; i >= start; i--) {
            sb.append(s.charAt(i));
        }
    }

    // @end is exclusive bound
    private static void addForwardString(StringBuilder sb, String s, int start, int end) {
        for (int i = start; i < end; i++) {
            sb.append(s.charAt(i));
        }
    }
}
