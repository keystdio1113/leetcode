package first.string;

/**
 *
 右旋字符串

 题目描述
     字符串的右旋转操作是把字符串尾部的若干个字符转移到字符串的前面。给定一个字符串 s 和一个正整数 k，
     请编写一个函数，将字符串中的后面 k 个字符移到字符串的前面，实现字符串的右旋转操作。

     例如，对于输入字符串 "abcdefg" 和整数 2，函数应该将其转换为 "fgabcde"。
 输入描述
    输入共包含两行，第一行为一个正整数 k，代表右旋转的位数。第二行为字符串 s，代表需要旋转的字符串。
 输出描述
    输出共一行，为进行了右旋转操作后的字符串。
 输入示例
     2
     abcdefg
 输出示例
    fgabcde
 提示信息
     数据范围：
         1 <= k < 10000,
         1 <= s.length < 10000;

 */
public class Day08_kama_55_RightRotateStrings {
    public static void main(String[] args) {
        System.out.println(solution(2, "abcdefg"));
        System.out.println(inPlaceSolution(2, "abcdefg"));
        System.out.println(betterInPlaceSolution(2, "abcdefg"));
    }

    public static String betterInPlaceSolution(int k, String s) {
        char[] input = s.toCharArray();
        reverseCharsInRange(input, 0, s.length()-1);
        reverseCharsInRange(input, 0, k-1);
        reverseCharsInRange(input, k, s.length()-1);

        return new String(input);
    }

    private static void reverseCharsInRange(char[] ch, int i, int j) {
        if (j - i <= 0) return;
        while (j - i > 0) {
            char tmp = ch[j];
            ch[j] = ch[i];
            ch[i] = tmp;

            j--;
            i++;
        }
    }

    public static String inPlaceSolution(int k, String s) {
        char[] input = s.toCharArray();
        for (int time = 0; time < k; time++) {
            char last = input[input.length-1];
            for (int i = input.length-1; i >= 1; i--) {
                input[i] = input[i-1];
            }
            input[0] = last;
        }

        return new String(input);
    }

    public static String solution(int k, String s) {
        int stringEnd = s.length()-1;
        int start = stringEnd-k+1;

        // what if k > s.length???
        StringBuilder sb = new StringBuilder();
        sb.append(s.substring(start, s.length()));
        sb.append(s.substring(0, start));
        return sb.toString();
    }
}
