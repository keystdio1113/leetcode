package first.string;

import java.util.Arrays;

public class Day08_344_ReverseStringInPlace {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(reverseString(new char[]{'h','e','l','l','o'})));
        System.out.println(Arrays.toString(reverseString(new char[]{'h','e','l','l','o', 'W'})));

    }

    public static char[] reverseString(char[] s) {
        if (s.length <= 1) return s;

        int i = 0;
        int j = s.length - 1;

        while (i <= j) {
            char tmp = s[i];
            s[i] = s[j];
            s[j] = tmp;

            i++;
            j--;
        }
        return s;
    }
}
