package first.string;

public class Day09_459_repeatedSubstringPattern {
    public static void main(String[] args) {
//        System.out.println(dumb("abab"));
        System.out.println(dumb("aba"));
        System.out.println(dumb("abcabcabcabc"));
    }

    public static boolean dumb(String s) {
        boolean isValid = false;
        for (int substringLength = 1; substringLength < s.length(); substringLength++) {
            boolean currLengthIsValid = true;
            String substring = s.substring(0, substringLength);
            if (s.length() % substringLength != 0) {
                currLengthIsValid = false;
            } else {
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) != substring.charAt(i%substringLength)) {
                        currLengthIsValid = false;
                        break;
                    }
                }
                isValid = isValid || currLengthIsValid;
            }
        }

        return isValid;
    }
}
