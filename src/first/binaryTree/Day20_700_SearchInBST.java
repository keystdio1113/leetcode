package first.binaryTree;

import model.TreeNode;

public class Day20_700_SearchInBST {
    public TreeNode searchBST(TreeNode root, int val) {
        return searchRecursive(root, val);
    }

    public static TreeNode searchIterative(TreeNode root, int val) {
        TreeNode curr = root;
        while (curr != null) {
            if (curr.val > val) {
                curr = curr.left;
            } else if (curr.val < val) {
                curr = curr.right;
            } else {
                break;
            }
        }

        if (curr == null || curr.val != val) {
            return null;
        } else {
            return curr;
        }
    }

    public static TreeNode searchRecursive(TreeNode root, int val) {
        if (root == null) {
            return null;
        } else {
            if (root.val > val) {
                return searchRecursive(root.left, val);
            } else if (root.val < val) {
                return searchRecursive(root.right, val);
            } else {
                return root;
            }
        }
    }
}
