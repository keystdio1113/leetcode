package first.binaryTree;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Day17_257_binaryTreePaths {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<>();
        preorderTraverse(root, new ArrayList<>(), result);
        return result;
    }

    public void preorderTraverse(TreeNode root, List<String> path, List<String> results) {
        if (root != null) {
            if (root.left == null && root.right == null) {
                path.add(root.val + "");
                results.add(generatePath(path));
                path.remove(path.size()-1);
            } else {
                path.add(root.val + "");
                preorderTraverse(root.left, path, results);
                preorderTraverse(root.right, path, results);
                path.remove(path.size()-1);
            }
        }
    }

    private String generatePath(List<String> s) {
        if (s.size() == 1) {
            return s.get(0);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(s.get(0));
            for (int i = 1; i < s.size(); i++) {
                sb.append("->").append(s.get(i));
            }
            return sb.toString();
        }
    }
}
