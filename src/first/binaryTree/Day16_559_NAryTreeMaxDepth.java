package first.binaryTree;

import model.Node;

public class Day16_559_NAryTreeMaxDepth {
    private static int MAX_DEPTH = 0;

    public static void countDepthWise(Node root, int currDepth) {
        if (root != null) {
            MAX_DEPTH = Math.max(MAX_DEPTH, currDepth);
            for (Node n: root.children) {
                countDepthWise(n, currDepth+1);
            }
        }
    }

    public static int countHeightWise(Node root) {
        if (root == null) {
            return 0;
        } else {
            int maxHeight = 0;
            for (Node child: root.children) {
                // cannot do +1 here like in binary tree, because it is not guaranteed that a node will have a child
                // and we would have skipped adding the height, so have to do it at the end.
                int currChildHeight = countHeightWise(child);
                maxHeight = Math.max(maxHeight, currChildHeight);
            }
            return 1+maxHeight;
        }
    }
}
