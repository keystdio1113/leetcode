package first.binaryTree;

import model.TreeNode;

public class Day18_106_ConstructTreeWithInAndPostOrder {
    public static void main(String[] args) {
//        TreeNode root = buildTree(new int[]{9,3,15,20,7}, new int[]{9,15,7,20,3});

        TreeNode root = buildTree(new int[]{1,2}, new int[]{2,1});
    }

    public static TreeNode buildTree(int[] inorder, int[] postorder) {
        if (inorder.length > 0) {
            return build(inorder, postorder);
        } else {
            return null;
        }
    }

    // This only works because the elements are unique. But in general we should be doing.
    // Sample: inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
    // 1. We first find root node from the @postorder, which is the last element in the post order first.array (It is 3)
    // 2. Construct the node
    // 3. Find the left subtree and right subtree in @inorder. The left subtree is the left-side element of 3, and the right subtree is the other side
    //    In this case, left=[9], right=[15,20,7]
    // 4. Find the left subtree and right subtree in @postorder. Use the size of the left/right subtree from step 3 to derive it.
    //    In this case, we will find right first, which is [15,7,20], then the left is [9]
    // 5. Pass these pair of subtree to below layer to construct the left child and right child.
    public static TreeNode build(int[] inorder, int[] postorder) {
        int rootValue = postorder[postorder.length-1];
        TreeNode root = new TreeNode(rootValue);

        if (inorder.length == 1) {
            return root;
        }

        int rootValueInOrderIndex = 0;
        for (int i = 0; i < inorder.length; i++) {
            if (rootValue == inorder[i]) {
                rootValueInOrderIndex = i;
                break;
            }
        }

        int inOrderLeftChildL = 0;
        int inOrderLeftChildR = rootValueInOrderIndex-1;
        int inOrderRightChildL = rootValueInOrderIndex+1;
        int inOrderRightChildR = inorder.length-1;
        int[] inorderLeftChild = getSubArray(inorder, inOrderLeftChildL, inOrderLeftChildR);
        int[] inorderRightChild = getSubArray(inorder, inOrderRightChildL, inOrderRightChildR);

        if (inorderRightChild.length > 0) {
            int postOrderRightChildR = postorder.length - 1 - 1; // minus 1 for the root value, minus 1 again for the right interval
            int postOrderRightChildL = postOrderRightChildR - inorderRightChild.length + 1;
            int[] postOrderRightChild = getSubArray(postorder, postOrderRightChildL, postOrderRightChildR);
            root.right = build(inorderRightChild, postOrderRightChild);
        }

        if (inorderLeftChild.length > 0) {
            int postOrderLeftChildL = 0;
            int postOrderLeftChildR = postOrderLeftChildL + inorderLeftChild.length - 1;
            int[] postOrderLeftChild = getSubArray(postorder, postOrderLeftChildL, postOrderLeftChildR);
            root.left = build(inorderLeftChild, postOrderLeftChild);
        }

        return root;
    }

    // Both intervals are inclusive
    private static int[] getSubArray(int[] array, int l, int r) {
        if (l > r) {
            return new int[]{};
        }

        int size = r - l + 1;
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = array[i+l];
        }
        return result;
    }
}
