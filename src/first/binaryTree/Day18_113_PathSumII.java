package first.binaryTree;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Day18_113_PathSumII {
    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<List<Integer>> results = new ArrayList<>();
//        preOrder(root, 0, targetSum, new ArrayList<>(), results);
//        inOrder(root, 0, targetSum, new ArrayList<>(), results);
//        postOrder(root, 0, targetSum, new ArrayList<>(), results);
        return results;

    }

    public static void preOrder(TreeNode root, int currSum, int targetSum, List<Integer> tempResult, List<List<Integer>> result) {
        if (root != null) {
            int sum = currSum + root.val;
            tempResult.add(root.val);
            if (root.left == null && root.right == null) {
                if (sum == targetSum) {
                    result.add(List.copyOf(tempResult));
                }
            }

            preOrder(root.left, sum, targetSum, tempResult, result);
            preOrder(root.right, sum, targetSum, tempResult, result);
            tempResult.remove(tempResult.size()-1);
        }
    }

    public static void inOrder(TreeNode root, int currSum, int targetSum, List<Integer> tempResult, List<List<Integer>> result) {
        if (root != null) {
            int sum = currSum + root.val;
            tempResult.add(root.val);

            inOrder(root.left, sum, targetSum, tempResult, result);
            if (root.left == null && root.right == null) {
                if (sum == targetSum) {
                    result.add(List.copyOf(tempResult));
                }
            }
            inOrder(root.right, sum, targetSum, tempResult, result);
            tempResult.remove(tempResult.size()-1);
        }
    }

    public static void postOrder(TreeNode root, int currSum, int targetSum, List<Integer> tempResult, List<List<Integer>> result) {
        if (root != null) {
            int sum = currSum + root.val;
            tempResult.add(root.val);
            postOrder(root.left, sum, targetSum, tempResult, result);
            postOrder(root.right, sum, targetSum, tempResult, result);

            if (root.left == null && root.right == null) {
                if (sum == targetSum) {
                    result.add(List.copyOf(tempResult));
                }
            }
            tempResult.remove(tempResult.size()-1);
        }
    }
}
