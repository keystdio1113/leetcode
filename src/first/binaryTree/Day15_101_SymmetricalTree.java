package first.binaryTree;

import model.TreeNode;

public class Day15_101_SymmetricalTree {

    public boolean isSymmetric(TreeNode root) {
        return compare(root.left, root.right);
    }

    public static boolean compare(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        } else if (left == null && right != null) {
            return false;
        } else if (left != null && right == null) {
            return false;
        }

        boolean inside = compare(left.right, right.left);
        boolean outside = compare(left.left, right.right);
        return left.val == right.val && inside && outside;
    }
}
