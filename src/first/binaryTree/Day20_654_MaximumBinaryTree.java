package first.binaryTree;

import model.TreeNode;

public class Day20_654_MaximumBinaryTree {
    public static void main(String[] args) {

    }

    public TreeNode constructMaximumBinaryTree(int[] nums) {
        return build(nums);
    }

    public static TreeNode build(int[] nums) {
        if (nums.length == 0) {
            return null;
        } else {
            int maxValue = nums[0];
            int maxIndex = 0;
            for (int i = 0; i < nums.length; i++) {
                if (maxValue < nums[i]) {
                    maxIndex = i;
                    maxValue = nums[i];
                }
            }
            TreeNode root = new TreeNode(maxValue);
            int leftL = 0;
            int leftR = maxIndex - 1;
            int rightL = maxIndex + 1;
            int rightR = nums.length - 1;

            if (leftR - leftL >= 0) {
                int[] leftChild = getSubArray(nums, leftL, leftR);
                root.left = build(leftChild);
            }

            if (rightR - rightL >= 0) {
                int[] rightChild = getSubArray(nums, rightL, rightR);
                root.right = build(rightChild);
            }

            return root;
        }
    }

    private static int[] getSubArray(int[] nums, int l, int r) {
        int size = r - l + 1;
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = nums[i+l];
        }
        return result;
    }
}
