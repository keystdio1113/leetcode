package first.binaryTree;

import model.TreeNode;

public class Day18_105_ConstructTreeWithInAndPreOrder {
    public static void main(String[] args) {}

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return build(preorder, inorder);
    }


    // This only works because the elements are unique. But in general we should be doing.
    // Sample: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7],
    // 1. We first find root node from the @preorder, which is the first element in the pre order first.array (It is 3)
    // 2. Construct the node
    // 3. Find the left subtree and right subtree in @inorder. The left subtree is the left-side element of 3, and the right subtree is the other side
    //    In this case, left=[9], right=[15,20,7]
    // 4. Find the left subtree and right subtree in @preorder. Use the size of the left/right subtree from step 3 to derive it.
    //    In this case, we will find left first, which is [9], then the right is [20,15,7]
    // 5. Pass these pair of subtree to below layer to construct the left child and right child.
    public TreeNode build(int[] preorder, int[] inorder) {
        int rootValue = preorder[0];
        TreeNode root = new TreeNode(rootValue);

        if (inorder.length == 1) {
            return root;
        }

        int rootValueInOrderIndex = 0;
        for (int i = 0; i < inorder.length; i++) {
            if (rootValue == inorder[i]) {
                rootValueInOrderIndex = i;
                break;
            }
        }

        int inOrderLeftChildL = 0;
        int inOrderLeftChildR = rootValueInOrderIndex-1;
        int inOrderRightChildL = rootValueInOrderIndex+1;
        int inOrderRightChildR = inorder.length-1;
        int[] inorderLeftChild = getSubArray(inorder, inOrderLeftChildL, inOrderLeftChildR);
        int[] inorderRightChild = getSubArray(inorder, inOrderRightChildL, inOrderRightChildR);


        if (inorderLeftChild.length > 0) {
            int preOrderLeftChildL = 1;
            int preOrderLeftChildR = preOrderLeftChildL + inorderLeftChild.length - 1;
            int[] preOrderLeftChild = getSubArray(preorder, preOrderLeftChildL, preOrderLeftChildR);
            root.left = build(preOrderLeftChild, inorderLeftChild);
        }

        if (inorderRightChild.length > 0) {
            int preOrderRightChildR = preorder.length - 1;
            int preOrderRightChildL = preOrderRightChildR - inorderRightChild.length + 1;
            int[] preOrderRightChild = getSubArray(preorder, preOrderRightChildL, preOrderRightChildR);
            root.right = build(preOrderRightChild, inorderRightChild);
        }

        return root;
    }


    // Both intervals are inclusive
    private static int[] getSubArray(int[] array, int l, int r) {
        if (l > r) {
            return new int[]{};
        }

        int size = r - l + 1;
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = array[i+l];
        }
        return result;
    }
}
