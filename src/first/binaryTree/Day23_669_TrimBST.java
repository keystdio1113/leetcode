package first.binaryTree;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Day23_669_TrimBST {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(2, new TreeNode(1), new TreeNode(3));
        trimBST(root, 3, 4);
    }

    public static TreeNode trimBST(TreeNode root, int low, int high) {
        if (root == null) return root;
        TreeNode res = dumb(root, low, high);
        return res;

//        return smart(root, low, high);
    }

    /**
     * When we encounter each node there could be:
     *   1. node is NULL
     *   2. node.val is within the range
     *   3. node.val is smaller than low
     *   4. node.val is larger than high
     *
     *   For 3, we will need to cut node, process its right child, as its right subtree could contain some nodes that is in the range
     *   For 4, we will need to cut node, process its left child, as its left subtree could contain some nodes that is in the range
     */
    public static TreeNode smart(TreeNode root, int low, int high) {
        if (root == null) {
            return null;
        } else {
            if (root.val < low) {
                TreeNode right = smart(root.right, low, high);
                return right;
            } else if (root.val > high) {
                TreeNode left = smart(root.left, low, high);
                return left;
            } else {
                root.left = smart(root.left, low, high);
                root.right = smart(root.right, low, high);
                return root;
            }
        }
    }

    public static TreeNode dumb(TreeNode root, int low, int high) {
        List<Integer> keysToDelete = new ArrayList<>();
        List<TreeNode> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            TreeNode curr = q.remove(0);
            if (curr.val < low || curr.val > high) {
                keysToDelete.add(curr.val);
            }
            if (curr.left != null) q.add(curr.left);
            if (curr.right != null) q.add(curr.right);
        }

        for (int keyToDelete: keysToDelete) {
            root = deleteRecursivePromoteImmediateSuccessor(root, keyToDelete);
        }
        return root;
    }

    public static TreeNode deleteRecursivePromoteImmediatePredecessor(TreeNode root, int key) {
        if (root == null) {
            return null; // scenario 1, can't find it, this is equivalent to doing nothing
        }

        if (root.val == key) {
            if (root.left == null && root.right == null) { // scenario 2
                return null;
            } else if (root.left == null && root.right != null) { // scenario 3
                return root.right;
            } else if (root.left != null && root.right == null) { // scenario 4
                return root.left;
            } else {
                TreeNode ip = root.left;
                while (ip.right != null) {
                    ip = ip.right;
                }
                ip.right = root.right;
                return root.left; // promoting the left child to replace the deleted node
            }
        }

        if (root.val > key) {
            root.left = deleteRecursivePromoteImmediatePredecessor(root.left, key);
        } else {
            root.right = deleteRecursivePromoteImmediatePredecessor(root.right, key);
        }
        return root;
    }

    public static TreeNode deleteRecursivePromoteImmediateSuccessor(TreeNode root, int key) {
        if (root == null) {
            return null;
        } else {
            if (root.val == key) {
                if (root.left == null && root.right == null) {
                    return null;
                } else if (root.left != null && root.right == null) {
                    return root.left;
                } else if (root.left == null && root.right != null) {
                    return root.right;
                } else {
                    TreeNode is = root.right;
                    while (is.left != null) {
                        is = is.left;
                    }
                    is.left = root.left;
                    return root.right;
                }
            } else if (root.val < key) {
                root.right = deleteRecursivePromoteImmediateSuccessor(root.right, key);

            } else {
                root.left = deleteRecursivePromoteImmediateSuccessor(root.left, key);
            }
            return root;
        }
    }
}
