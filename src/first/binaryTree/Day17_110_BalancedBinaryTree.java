package first.binaryTree;

import model.TreeNode;

public class Day17_110_BalancedBinaryTree {
    private static boolean isBalanced = true;
    public boolean isBalanced(TreeNode root) {
        isBalanced = true;
        postOrderTraversal(root);
        return isBalanced;
    }

    public int postOrderTraversal(TreeNode root) {
        if (root == null) return 0;
        int leftHeight = postOrderTraversal(root.left);
        int rightHeight = postOrderTraversal(root.right);

        if (Math.abs(leftHeight-rightHeight) > 1) {
            isBalanced = false;
        }
        return 1 + Math.max(leftHeight, rightHeight);
    }

    // this is smart to return -1 to signal that this subtree is not balanced anymore.
    public int postOrderTraversalNotUsingStaticVariable(TreeNode root) {
        if (root == null) return 0;
        int leftHeight = postOrderTraversalNotUsingStaticVariable(root.left);
        if (leftHeight == -1) return -1;
        int rightHeight = postOrderTraversalNotUsingStaticVariable(root.right);
        if (rightHeight == -1) return -1;

        if (Math.abs(leftHeight-rightHeight) > 1) {
            return -1;
        } else {
            return 1 + Math.max(leftHeight, rightHeight);
        }
    }
}
