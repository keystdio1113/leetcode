package first.binaryTree;

import model.TreeNode;

public class Day21_530_MinimumAbsolutionDifferenceInBST {
    private static TreeNode prev;
    private static int minimumAbsDiff;

    public int getMinimumDifference(TreeNode root) {
        prev = null;
        minimumAbsDiff = Integer.MAX_VALUE;

        traverse(root);
        return minimumAbsDiff;
    }

    public void traverse(TreeNode root) {
        if (root != null) {
            traverse(root.left);
            if (prev != null) {
                int absDiff = Math.abs(root.val - prev.val);
                minimumAbsDiff = Math.min(absDiff, minimumAbsDiff);
            }
            prev = root;
            traverse(root.right);
        }
    }
}
