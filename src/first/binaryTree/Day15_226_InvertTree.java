package first.binaryTree;

import model.TreeNode;

public class Day15_226_InvertTree {
    public TreeNode invert_PostOrder(TreeNode root) {
        if (root != null) {
            invert_PostOrder(root.left);
            invert_PostOrder(root.right);
            TreeNode tmp = root.left;
            root.left = root.right;
            root.right = tmp;
        }
        return root;
    }

    public TreeNode invert_PreOrder(TreeNode root) {
        if (root != null) {
            TreeNode tmp = root.left;
            root.left = root.right;
            root.right = tmp;
            invert_PreOrder(root.left);
            invert_PreOrder(root.right);
        }
        return root;
    }

    public TreeNode invert_InOrder(TreeNode root) {
        if (root != null) {
            invert_InOrder(root.left);
            TreeNode tmp = root.left;
            root.left = root.right;
            root.right = tmp;
            invert_InOrder(root.left); // note that instead of right, we need to do left here.
        }
        return root;
    }
}
