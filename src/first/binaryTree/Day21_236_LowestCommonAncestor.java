package first.binaryTree;

import model.TreeNode;

import java.util.*;

public class Day21_236_LowestCommonAncestor {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
//        return findPathSolution(root, p, q);
        return find(root, p, q);
    }

    public static TreeNode findPathSolution(TreeNode root, TreeNode p, TreeNode q) {
        Map<TreeNode, TreeNode> parents = new HashMap<>();
        traverse(root, null, parents);

        List<TreeNode> pathToP = new ArrayList<>();
        TreeNode currP = p;
        do {
            pathToP.add(currP);
            currP = parents.get(currP);
        } while (currP != null);

        Set<Integer> pathToQ = new HashSet<>();
        TreeNode currQ = q;
        do {
            pathToQ.add(currQ.val);
            currQ = parents.get(currQ);
        } while (currQ != null);

        for (int i = 0; i < pathToP.size(); i++) {
            if (pathToQ.contains(pathToP.get(i).val)) {
                return pathToP.get(i);
            }
        }
        throw new IllegalStateException("This should not happened");
    }

    public static void traverse(TreeNode root, TreeNode prev, Map<TreeNode, TreeNode> parents) {
        if (root != null) {
            parents.put(root, prev);
            traverse(root.left, root, parents);
            traverse(root.right, root, parents);
        }
    }

    public static TreeNode find(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return null;
        if (root == p || root == q) return root;

        TreeNode left = find(root.left, p, q);
        TreeNode right = find(root.right, p, q);

        if (left == null && right == null) {
            return null;
        } else if (left != null && right == null) {
            return left;
        } else if (left == null && right != null) {
            return right;
        } else {
            return root;
        }
    }
}
