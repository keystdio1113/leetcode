package first.binaryTree;

import model.TreeNode;

public class Day20_617_MergeBinaryTree {
    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
//        return merge(root1, root2);
        return mergeNotUsingNewTree(root1, root2);
    }

    public static TreeNode mergeNotUsingNewTree(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) {
            return null;
        } else if (t1 == null && t2 != null) {
            return t2;
        } else if (t1 != null && t2 != null) {
            return t1;
        } else {
            t1.val = t1.val + t2.val;
            t1.left = mergeNotUsingNewTree(t1.left, t2.left);
            t1.right = mergeNotUsingNewTree(t1.right, t2.right);
            return t1;
        }
    }

    public static TreeNode merge(TreeNode root1, TreeNode root2) {
        if (root1 == null && root2 == null) {
            return null;
        } else if (root1 != null && root2 != null) {
            TreeNode root = new TreeNode(root1.val + root2.val);
            root.left = merge(root1.left, root2.left);
            root.right = merge(root1.right, root2.right);
            return root;
        } else if (root1 == null && root2 != null) {
            return root2;
        } else {
            return root1;
        }
    }
}
