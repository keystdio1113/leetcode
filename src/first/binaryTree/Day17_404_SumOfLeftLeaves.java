package first.binaryTree;

import model.TreeNode;

public class Day17_404_SumOfLeftLeaves {

    private static int SUM = 0;
    public int sumOfLeftLeaves(TreeNode root) {
//        return postOrder(root, false);

        SUM = 0;
        preOrder(root, false);
        return SUM;
    }

    public void preOrder(TreeNode root, boolean shouldAdd) {
        if (root != null) {
            if (root.left == null && root.right == null && shouldAdd) {
                SUM += root.val;
            }
            preOrder(root.left, true);
            preOrder(root.right, false);
        }
    }

    public int postOrder(TreeNode root, boolean shouldAdd) {
        if (root == null) {
            return 0;
        } else {
            int leftSum = postOrder(root.left, true);
            int rightSum = postOrder(root.right, false);

            int result = leftSum + rightSum;
            if (root.left == null && root.right == null && shouldAdd) {
                result += root.val;
            }
            return result;
        }
    }
}
