package first.binaryTree;

import model.TreeNode;

public class Day18_112_PathSum {
    private static boolean RESULT = false;
    public boolean hasPathSum(TreeNode root, int targetSum) {
//        RESULT = false;
//        preOrder(root, 0, targetSum);
//        return RESULT;

        return dfs(root, 0, targetSum);
    }

    public static void preOrder(TreeNode root, int currSum, int targetSum) {
        if (RESULT) return;
        if (root != null) {
            int sum = currSum + root.val;
            if (root.left == null && root.right == null) {
                if (sum == targetSum) {
                    RESULT = true;
                }
            }
            preOrder(root.left, sum, targetSum);
            preOrder(root.right, sum, targetSum);
        }
    }

    public static boolean dfs(TreeNode root, int currSum, int targetSum) {
        if (root == null) {
            return false;
        } else {
            int sum = currSum + root.val;
            if (root.left == null && root.right == null) {
                return targetSum == sum;
            }
            return dfs(root.left, sum, targetSum) || dfs(root.right, sum, targetSum);
        }
    }
}