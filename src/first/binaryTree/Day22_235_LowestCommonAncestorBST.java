package first.binaryTree;

import model.TreeNode;

public class Day22_235_LowestCommonAncestorBST {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // return treatItAsBTFind(root, p, q);
        // return usingBSTCharacteristicFind(root, p, q);
        return usingBSTCharacteristicFindRecursive(root, p, q);
    }

    public static TreeNode usingBSTCharacteristicFindRecursive(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return null;
        } else {
            if (p.val < root.val && q.val < root.val) {
                TreeNode left = usingBSTCharacteristicFind(root.left, p, q);
                return left;
            } else if (p.val > root.val && q.val > root.val) {
                TreeNode right = usingBSTCharacteristicFindRecursive(root.right, p, q);
                return right;
            } else {
                return root;
            }

        }
    }

    public static TreeNode usingBSTCharacteristicFind(TreeNode root, TreeNode p, TreeNode q) {
        // keep going down the tree until we hit some condition
        // We will:
        //   1. Go left if p and q are smaller than curr traversal
        //   2. Go right if p and q are larger than curr traversal
        // stop when we reach either p or q
        TreeNode curr = root;
        while (curr != p && curr != q) {
            if (p.val > curr.val && q.val > curr.val) {
                curr = curr.right;
            } else if (p.val < curr.val && q.val < curr.val) {
                curr = curr.left;
            } else {
                break; // p and q are on either side of the current node, no need to go down anymore.
            }
        }
        return curr;
    }

    public static TreeNode treatItAsBTFind(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return null;
        if (root == p || root == q) return root;

        TreeNode left = treatItAsBTFind(root.left, p, q);
        TreeNode right = treatItAsBTFind(root.right, p, q);

        if (left == null && right == null) {
            return null;
        } else if (left != null && right == null) {
            return left;
        } else if (left == null && right != null) {
            return right;
        } else {
            return root;
        }
    }
}
