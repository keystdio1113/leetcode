package first.binaryTree;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Day18_513_BottomLeftTreeValue {
    private static int RESULT = -1;
    private static int MAX_DEPTH = -1;
    public int findBottomLeftValue(TreeNode root) {
//        List<List<Integer>> level = levelOrder(root);
//        List<Integer> lastRow = level.get(level.size()-1);
//        return lastRow.get(0);

        RESULT = -1;
        MAX_DEPTH = -1;
//        preOrder(root, 1);
//        inOrder(root, 1);
        postOrder(root, 1);
        return RESULT;
    }

    public static void preOrder(TreeNode root, int currDepth) {
        if (root != null) {
            if (currDepth > MAX_DEPTH) {
                RESULT = root.val;
                MAX_DEPTH = currDepth;
            }
            preOrder(root.left, currDepth + 1);
            preOrder(root.right, currDepth + 1);
        }
    }

    public static void inOrder(TreeNode root, int currDepth) {
        if (root != null) {
            inOrder(root.left, currDepth + 1);
            if (currDepth > MAX_DEPTH) {
                RESULT = root.val;
                MAX_DEPTH = currDepth;
            }
            inOrder(root.right, currDepth + 1);
        }
    }

    public static void postOrder(TreeNode root, int currDepth) {
        if (root != null) {
            postOrder(root.left, currDepth + 1);
            postOrder(root.right, currDepth + 1);
            if (currDepth > MAX_DEPTH) {
                RESULT = root.val;
                MAX_DEPTH = currDepth;
            }
        }
    }

    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        List<TreeNode> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelCount = q.size();
            List<Integer> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelCount; i++) {
                TreeNode curr = q.remove(0);
                currLevel.add(curr.val);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            result.add(currLevel);
        }

        return result;
    }
}
