package first.binaryTree.levelBaseTraversal;

import model.TreeNode;

import java.util.*;

public class Day15_107_LevelOrderTraversalII {
    public static void main(String[] args) {

    }

    public List<List<Integer>> solution(TreeNode root) {
        if (root == null) {
            return new ArrayList<>();
        }

        Stack<List<Integer>> stack = new Stack<>();
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelSize = q.size();
            List<Integer> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelSize; i++) {
                TreeNode curr = q.remove();
                currLevel.add(curr.val);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            stack.push(currLevel);
        }

        List<List<Integer>> result = new ArrayList<>();
        while (!stack.isEmpty()) {
            result.add(stack.pop());
        }
        return result;

    }
}
