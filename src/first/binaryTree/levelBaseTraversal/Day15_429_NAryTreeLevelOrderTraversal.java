package first.binaryTree.levelBaseTraversal;

import model.Node;

import java.util.ArrayList;
import java.util.List;

public class Day15_429_NAryTreeLevelOrderTraversal {
    public static void main(String[] args) {

    }

    public static List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        List<Node> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelCount = q.size();
            List<Integer> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelCount; i++) {
                Node curr = q.remove(0);
                currLevel.add(curr.val);
                q.addAll(curr.children);
            }
            result.add(currLevel);
        }
        return result;
    }
}
