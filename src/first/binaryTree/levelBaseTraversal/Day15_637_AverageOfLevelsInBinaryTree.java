package first.binaryTree.levelBaseTraversal;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day15_637_AverageOfLevelsInBinaryTree {
    public static void main(String[] args) {

    }

    public static List<Double> solution(TreeNode root) {
        if (root == null) {
            return new ArrayList<>();
        }
        List<List<Integer>> nodesByLevel = levelOrder(root);
        return nodesByLevel.stream()
                .map(l -> average(l))
                .collect(Collectors.toList());
    }

    private static double average(List<Integer> nums) {
        double sum = 0.0;
        for (int n: nums) {
            sum += (double)n;
        }
        return sum / (double) nums.size();
    }


    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        List<TreeNode> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelCount = q.size();
            List<Integer> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelCount; i++) {
                TreeNode curr = q.remove(0);
                currLevel.add(curr.val);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            result.add(currLevel);
        }

        return result;
    }
}
