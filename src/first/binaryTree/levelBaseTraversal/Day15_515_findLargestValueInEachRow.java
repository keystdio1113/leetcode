package first.binaryTree.levelBaseTraversal;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day15_515_findLargestValueInEachRow {

    public static List<Integer> solution(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        List<List<Integer>> level = levelOrder(root);
        return level.stream()
                .map(l -> findMax(l))
                .collect(Collectors.toList());
    }

    private static int findMax(List<Integer> nums) {
        int max = nums.get(0);
        for (int n: nums) {
            max = Math.max(max, n);
        }
        return max;
    }

    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        List<TreeNode> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelCount = q.size();
            List<Integer> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelCount; i++) {
                TreeNode curr = q.remove(0);
                currLevel.add(curr.val);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            result.add(currLevel);
        }

        return result;
    }
}
