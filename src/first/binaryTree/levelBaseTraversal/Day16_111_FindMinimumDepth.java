package first.binaryTree.levelBaseTraversal;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Day16_111_FindMinimumDepth {
    private static int MIN_DEPTH = Integer.MAX_VALUE;

    // use pre order traversal to get the minimum depth.
    public static void preOrderTraversal(TreeNode root, int currDepth) {
        if (root != null) {
            if (currDepth > MIN_DEPTH) {
                return;
            }

            if (root.left == null && root.right == null) {
                MIN_DEPTH = Math.min(currDepth, MIN_DEPTH);
            }
            preOrderTraversal(root.left, currDepth + 1);
            preOrderTraversal(root.right, currDepth + 1);
        }
    }

    public static int postOrderTraversal(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            int leftMin = postOrderTraversal(root.left);
            int rightMin = postOrderTraversal(root.right);

            if (root.left == null && root.right != null) return 1 + rightMin;
            if (root.left != null && root.right == null) return 1 + leftMin;
            return 1 + Math.min(leftMin, rightMin);
        }
    }

    public static int levelTraversal(TreeNode root) {
        int level = 0;
        if (root == null) {
            return level;
        }

        List<TreeNode> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelCount = q.size();
            List<Integer> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelCount; i++) {
                TreeNode curr = q.remove(0);
                if (curr.left == null && curr.right == null) {
                    return level+1;
                }
                currLevel.add(curr.val);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            level++;
        }

        throw new IllegalStateException("This should not occurred, leaf node must be encountered if tree is not empty");
    }
}
