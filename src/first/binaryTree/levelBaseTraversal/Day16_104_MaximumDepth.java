package first.binaryTree.levelBaseTraversal;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Day16_104_MaximumDepth {
    private static int MAX_DEPTH = 0;
    public static void main(String[] args) {

    }

    // This also works because MAX_DEPTH = MAX_HEIGHT in a tree.
    // Use post order to get the height
    public static int countHeightWise(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            int leftHeight = countHeightWise(root.left);
            int rightHeight = countHeightWise(root.right);
            return 1+Math.max(leftHeight, rightHeight);
        }
    }

    // use pre order to get the depth
    public static void countDepthWise(TreeNode root, int currDepth) {
        if (root != null) {
            MAX_DEPTH = Math.max(currDepth, MAX_DEPTH);
            countDepthWise(root.left, currDepth+1);
            countDepthWise(root.right, currDepth+1);
        }
    }

    public static int levelWiseTraversal(TreeNode root) {
        int level = 0;
        if (root == null) {
            return level;
        }

        List<TreeNode> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelCount = q.size();
            List<Integer> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelCount; i++) {
                TreeNode curr = q.remove(0);
                currLevel.add(curr.val);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            level++;
        }
        return level;
    }
}
