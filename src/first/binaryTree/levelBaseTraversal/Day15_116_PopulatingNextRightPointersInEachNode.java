package first.binaryTree.levelBaseTraversal;

import java.util.ArrayList;
import java.util.List;

public class Day15_116_PopulatingNextRightPointersInEachNode {

    public static Node solution(Node root) {
        if (root == null) return root;
        List<List<Node>> levels = levelOrder(root);
        for (List<Node> level: levels) {
            for (int i = 0; i < level.size(); i++) {
                if (i == level.size()-1) {
                    level.get(i).next = null;
                } else {
                    level.get(i).next = level.get(i+1);
                }
            }
        }

        return root;
    }

    public static List<List<Node>> levelOrder(Node root) {
        List<List<Node>> result = new ArrayList<>();
        if (root == null) {
            return new ArrayList<>();
        }

        List<Node> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelCount = q.size();
            List<Node> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelCount; i++) {
                Node curr = q.remove(0);
                currLevel.add(curr);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            result.add(currLevel);
        }
        return result;
    }

    static class Node {
        public int val;
        public Node left;
        public Node right;
        public Node next;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, Node _left, Node _right, Node _next) {
            val = _val;
            left = _left;
            right = _right;
            next = _next;
        }
    };
}
