package first.binaryTree.levelBaseTraversal;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Day15_102_LevelOrderTraversal {
    public static void main(String[] args) {

    }

    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        List<TreeNode> q = new ArrayList<>();
        q.add(root);
        while (!q.isEmpty()) {
            int currLevelCount = q.size();
            List<Integer> currLevel = new ArrayList<>();
            for (int i = 0; i < currLevelCount; i++) {
                TreeNode curr = q.remove(0);
                currLevel.add(curr.val);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            result.add(currLevel);
        }

        return result;
    }
}
