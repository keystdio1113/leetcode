package first.binaryTree;

import model.TreeNode;

public class Day22_701_InsertIntoBST {
    public TreeNode insertIntoBST(TreeNode root, int val) {
//        if (root == null) {
//            return new TreeNode(val);
//        }
//
//        insert(root, val);
//        return root;

        return insertRecursive(root, val);
    }

    public TreeNode insertRecursive(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        } else {
            if (val < root.val) {
                root.left = insertRecursive(root.left, val);
            } else {
                root.right = insertRecursive(root.right, val);
            }
            return root;
        }
    }

    public void insert(TreeNode root, int val) {
        // keep going down the BST with rules, root.val > val go left, else go right, until:
        //   1. We hit a leaf node, and insert new node in the place
        //   2. We hit a node that we want to go the direction we want to go, but that path is NULL, and insert it at that place.
        TreeNode prev = null;
        TreeNode curr = root;

        while (curr != null) {
            prev = curr;
            if (curr.val < val) {
                curr = curr.right;
            } else {
                curr = curr.left;
            }
        }
        if (val < prev.val) {
            prev.left = new TreeNode(val);
        } else {
            prev.right = new TreeNode(val);
        }
    }
}
