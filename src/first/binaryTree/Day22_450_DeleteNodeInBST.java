package first.binaryTree;

import model.TreeNode;

public class Day22_450_DeleteNodeInBST {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        TreeNode left = new TreeNode(3, new TreeNode(2), new TreeNode(4));
        TreeNode right = new TreeNode(6, null, new TreeNode(7));
        root.left = left;
        root.right = right;

        deleteIteratively(root, 5);
    }

    /**
     *
     * There are five different scenarios to considered:
     *   1. Node to delete doesn't exist... easy, do nothing
     *   2. Node to delete is a leaf node, just cut it
     *   3. Node to delete has NULL left, non-NULL right, have parent's point to its non-NULL right
     *   4. Node to delete has NULL right, non-NULL left, have parent's point to its non-NULL left
     *   5. Node to delete has both left and right child, need some elaborations
     *      (Promoting right child)
     *      a. Find the node's immediate successor IS
     *      b. IS.left = node.left
     *      c. parent of deletedNode connect to right child (depending on edge direction)
     *
     */
    public TreeNode deleteNode(TreeNode root, int key) {
//        if (root == null) return root;
//        if (root.left == null && root.right == null) {
//            if (root.val == key) {
//                return null;
//            } else {
//                return root;
//            }
//        }
//
//        TreeNode dummyRoot = new TreeNode(Integer.MAX_VALUE);
//        dummyRoot.left = root;
//        deleteCorrect(dummyRoot, key);
//        return dummyRoot.left;
        return deleteRecursivePromoteImmediatePredecessor(root, key);
//        return deleteRecursivePromoteImmediateSuccessor(root, key);
    }

    public static TreeNode deleteRecursivePromoteImmediatePredecessor(TreeNode root, int key) {
        if (root == null) {
            return null; // scenario 1, can't find it, this is equivalent to doing nothing
        }

        if (root.val == key) {
            if (root.left == null && root.right == null) { // scenario 2
                return null;
            } else if (root.left == null && root.right != null) { // scenario 3
                return root.right;
            } else if (root.left != null && root.right == null) { // scenario 4
                return root.left;
            } else {
                TreeNode ip = root.left;
                while (ip.right != null) {
                    ip = ip.right;
                }
                ip.right = root.right;
                return root.left; // promoting the left child to replace the deleted node
            }
        }

        if (root.val > key) {
            root.left = deleteRecursivePromoteImmediatePredecessor(root.left, key);
        } else {
            root.right = deleteRecursivePromoteImmediatePredecessor(root.right, key);
        }
        return root;
    }

    public static TreeNode deleteRecursivePromoteImmediateSuccessor(TreeNode root, int key) {
        if (root == null) {
            return null; // scenario 1, can't find it, this is equivalent to doing nothing
        }

        if (root.val == key) {
            if (root.left == null && root.right == null) { // scenario 2
                return null;
            } else if (root.left == null && root.right != null) { // scenario 3
                return root.right;
            } else if (root.left != null && root.right == null) { // scenario 4
                return root.left;
            } else {
                TreeNode is = root.right;
                while (is.left != null) {
                    is = is.left;
                }
                is.left = root.left;
                return root.right; // promoting the right child to replace the deleted node
            }
        }

        if (root.val > key) {
            root.left = deleteRecursivePromoteImmediateSuccessor(root.left, key);
        } else {
            root.right = deleteRecursivePromoteImmediateSuccessor(root.right, key);
        }
        return root;
    }

    /**
     *
     * Hard to get right:
     *   1. It is hard to maintain previous, and it will break when we are deleting the root node. Use dummyRoot
     */
    public static void deleteIteratively(TreeNode dummyRoot, int key) {
        // find the key we want to delete
        TreeNode prev = dummyRoot;
        TreeNode toDelete = prev.left;

        while (toDelete != null) {
            if (toDelete.val < key) {
                prev = toDelete;
                toDelete = toDelete.right;
            } else if (toDelete.val > key) {
                prev = toDelete;
                toDelete = toDelete.left;
            } else {
                break;
            }
        }

        if (toDelete == null) return; // scenario 1
        if (toDelete.left == null && toDelete.right == null) { // scenario 2
            if (prev.val < toDelete.val) {
                prev.right = null;
            } else {
                prev.left = null;
            }
        } else if (toDelete.left == null && toDelete.right != null) { // scenario 3
            if (prev.val < toDelete.val) {
                prev.right = toDelete.right;
            } else {
                prev.left = toDelete.right;
            }
        } else if (toDelete.left != null && toDelete.right == null) { // scenario 4
            if (prev.val < toDelete.val) {
                prev.right = toDelete.left;
            } else {
                prev.left = toDelete.left;
            }
        } else { // scenario 5
            TreeNode is = toDelete.right; // find immediate successor
            while (is.left != null) {
                is = is.left;
            }
            is.left = toDelete.left;
            if (prev.val < toDelete.val) {
                prev.right = toDelete.right;
            } else {
                prev.left = toDelete.right;
            }
        }
    }

    public static void deleteWrongComplicatedFirstThought(TreeNode root, int key) {
        // find the key we want to delete
        TreeNode prev = null;
        TreeNode toDelete = root;

        while (toDelete != null) {
            if (toDelete.val < key) {
                prev = toDelete;
                toDelete = toDelete.right;
            } else if (toDelete.val > key) {
                prev = toDelete;
                toDelete = toDelete.left;
            } else {
                break;
            }
        }

        if (toDelete == null) return;
        //   if it is a leaf node, just remove it.
        //   else
        //       1. If node has left child
        //            find its immediate predecessor (IP) in the BST
        //            replace the value of the key with the IP
        //            if the IP has a left child, attached it to the right child of its parent
        //            disconnect the IP
        //       2. If node has right child
        //            find its immediate successor (IS)
        //            replace the value of the key with the IS
        //            if the IS has a right child, attached it to the left child of its parent
        //            disconnect the IS

        if (toDelete.left == null && toDelete.right == null) {
            if (prev.val < toDelete.val) {
                prev.right = null;
            } else {
                prev.left = null;
            }
        } else {
            if (toDelete.left != null) {
                TreeNode ipPrev = toDelete;
                TreeNode ip = toDelete.left;
                while (ip.right != null) {
                    ipPrev = ip;
                    ip = ip.right;
                }
                toDelete.val = ip.val;
                if (toDelete == ipPrev) {
                    ipPrev.left = ip.left;
                } else {
                    ipPrev.right = ip.left;
                }
            } else if (toDelete.right != null) {
                TreeNode isPrev = toDelete;
                TreeNode is = toDelete.right;
                while (is.left != null) {
                    isPrev = is;
                    is = isPrev.right;
                }
                toDelete.val = is.val;
                if (toDelete == isPrev) {
                    isPrev.right = is.right;
                } else {
                    isPrev.left = is.right;
                }
            }
        }
    }
}
