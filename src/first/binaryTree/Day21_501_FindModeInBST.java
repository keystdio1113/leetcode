package first.binaryTree;

import model.TreeNode;

import java.util.*;

public class Day21_501_FindModeInBST {
    private static TreeNode prev;
    private static int maxCount;
    private static int currCount;

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode right2 = new TreeNode(2, new TreeNode(2), null);
        root.right = right2;

        findMode(root);
    }

    public static int[] findMode(TreeNode root) {
        // Dumb way of using hashmap to count
//        Map<Integer, Integer> count = new HashMap<>();
//        traverseWithHashMap(root, count);
//
//        Queue<Pair<Integer, Integer>> sortedList = new PriorityQueue<>((o1, o2) -> -1*(o1.v - o2.v));
//        for (int k: count.keySet()) {
//            sortedList.add(new Pair<>(k, count.get(k)));
//        }
//        int mostFrequentCount = sortedList.peek().v;
//        int modeCount = (int) sortedList.stream().filter(o -> o.v == mostFrequentCount).count();
//        int[] result = new int[modeCount];
//        for (int i = 0; i < modeCount; i++) {
//            result[i] = sortedList.remove().k;
//        }
//        return result;

        // traverse twice, once for maxCount, the other find all elements that match the count
        prev = null;
        maxCount = 1;
        currCount = 1;
        traverseToFindMaxCount(root);

        prev = null;
        currCount = 1;
        Set<Integer> results = new HashSet<>();
        traverseToOutputNodesWithMaxCount(root, results);
        if (currCount == maxCount && !results.contains(prev.val)) {
            results.add(prev.val);
        } // oh whoa....
        int[] res = new int[results.size()];
        int i = 0;
        for (int n: results) {
            res[i] = n;
            i++;
        }
        return res;

        // traverse once
//        prev = null;
//        currCount = 1;
//        maxCount = 1;
//        List<Integer> results = new ArrayList<>();
//        singleTraverse(root, results);
//        int[] res = new int[results.size()];
//        for (int i = 0; i < results.size(); i++) {
//            res[i] = results.get(i);
//        }
//        return res;
    }

    public static void singleTraverse(TreeNode root, List<Integer> results) {
        if (root != null) {
            singleTraverse(root.left, results);
            if (prev != null) {
                if (prev.val == root.val) {
                    currCount++;
                } else {
                    currCount = 1;
                }
            }
            prev = root;
            if (currCount == maxCount) {
                results.add(root.val);
            }

            if (currCount > maxCount) {
                maxCount = Math.max(currCount, maxCount);
                results.clear();
                results.add(root.val); // this is rather inefficient as the list could be huge and there are a lot of removes and adds
            }
            singleTraverse(root.right, results);
        }
    }

    public static void traverseToOutputNodesWithMaxCount(TreeNode root, Set<Integer> modeNode) {
        if (root != null) {
            traverseToOutputNodesWithMaxCount(root.left, modeNode);
            if (prev != null) {
                if (prev.val == root.val) {
                    currCount++;
                } else {
                    if (currCount == maxCount) {
                        modeNode.add(prev.val);
                    }
                    currCount = 1;
                }
            }
            prev = root;
            traverseToOutputNodesWithMaxCount(root.right, modeNode);
        }
    }

    public static void traverseToFindMaxCount(TreeNode root) {
        if (root != null) {
            traverseToFindMaxCount(root.left);
            if (prev != null) {
                if (prev.val == root.val) {
                    currCount++;
                    maxCount = Math.max(currCount, maxCount);
                } else {
                    currCount = 1;
                }
            }
            prev = root;
            traverseToFindMaxCount(root.right);
        }
    }

    public static void traverseWithHashMap(TreeNode root, Map<Integer, Integer> count) {
        if (root != null) {
            if (!count.containsKey(root.val)) {
                count.put(root.val, 0);
            }
            count.put(root.val, count.get(root.val)+1);
            traverseWithHashMap(root.left, count);
            traverseWithHashMap(root.right, count);
        }
    }
}
