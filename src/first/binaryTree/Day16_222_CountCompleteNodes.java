package first.binaryTree;

import model.TreeNode;

public class Day16_222_CountCompleteNodes {
    public int countNodes(TreeNode root) {
        return count(root);
    }

    public int count(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            return 1 + count(root.left) + count(root.right);
        }
    }

    public static int countExploitingCompleteTreeCarl(TreeNode root) {
        if (root == null) return 0;

        int leftDepth = 0;
        TreeNode left = root;
        while (left != null) {
            left = left.left;
            leftDepth++;
        }
        int rightDepth = 0;
        TreeNode right = root;
        while (right != null) {
            right = right.right;
            rightDepth++;
        }
        if (leftDepth == rightDepth) return (2 << (leftDepth-1))-1;

        return 1 + countExploitingCompleteTreeCarl(root.left) + countExploitingCompleteTree(root.right);
    }

    public static int countExploitingCompleteTree(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            int leftNodeCount = getNodeCountIfFullTree(root.left);
            int rightNodeCount = getNodeCountIfFullTree(root.right);

            int left = leftNodeCount == -1 ? countExploitingCompleteTree(root.left) : leftNodeCount;
            int right = rightNodeCount == -1 ? countExploitingCompleteTree(root.right) : rightNodeCount;

            return 1 + left + right;
        }
    }

    private static int getNodeCountIfFullTree(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            int leftDepth = 0;
            int rightDepth = 0;
            TreeNode curr = root;
            while (curr != null) {
                leftDepth++;
                curr = curr.left;
            }

            curr = root;
            while (curr != null) {
                rightDepth++;
                curr = curr.right;
            }
            if (leftDepth == rightDepth) {
                return (int)(Math.pow(2.0, leftDepth)) - 1;
            } else {
                return -1;
            }
        }
    }
}
