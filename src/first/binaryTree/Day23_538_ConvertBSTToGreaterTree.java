package first.binaryTree;

import model.TreeNode;

public class Day23_538_ConvertBSTToGreaterTree {
    private static int remainingSum;
    private static TreeNode prev;
    public static TreeNode convertBST(TreeNode root) {
        if (root == null) return root;

//        remainingSum = sum(root);
//        solve(root);
//        return root;

        prev = null;
        reverseInOrder(root);
        return root;
    }

    public static void reverseInOrder(TreeNode root) {
        if (root != null) {
            reverseInOrder(root.right);
            if (prev == null) {
                prev = root;
            } else {
                root.val += prev.val;
                prev = root;
            }
            reverseInOrder(root.left);
        }
    }

    public static void solve(TreeNode root) {
        // get an reversed in order traversal first:
        //   [8, 7, 6, 5, 4, 3, 2, 1, 0]
        //   [0, 1, 2, 3, 4, 5, 6, 7, 8]
        // compute a running sum for this
        //   [0, 1, 3, 6,10,15,21,28,36]

        // get the sum of all nodes 36

        // In order traversal with variables @root, @remainingSum:
        //   @root @root.val @remainingSum
        //    0       36          36
        //    1       36          36-@root -> 35
        //    2       35          35-@root -> 33
        //    3       33          33-@root -> 30
        inOrder(root);
    }

    public static void inOrder(TreeNode root) {
        if (root != null) {
            inOrder(root.left);

            int currVal = root.val;
            root.val = remainingSum;
            remainingSum -= currVal;

            inOrder(root.right);
        }
    }

    public static int sum(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            return root.val + sum(root.left) + sum(root.right);
        }
    }
}
