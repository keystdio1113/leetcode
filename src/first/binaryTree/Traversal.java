package first.binaryTree;

import model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Traversal {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(5,
                new TreeNode(4, new TreeNode(1), new TreeNode(2)),
                new TreeNode(6)
        );

        System.out.println("Preorder traversal: " + preOrder(node));
        System.out.println("Preorder iterative traversal: " + preOrderIterative(node));
        System.out.println("Inorder traversal: " + inOrder(node));
        System.out.println("Inorder iterative traversal: " + inOrderIterative(node));
        System.out.println("Postorder traversal: " + postOrder(node));
        System.out.println("Postorder iterative traversal: " + postOrderIterative(node));
    }

    public static List<Integer> preOrderIterative(TreeNode node) {
        List<Integer> result = new ArrayList<>();
        if (node == null) {
            return result;
        }

        Stack<TreeNode> stack = new Stack<>();
        stack.push(node);
        while (!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            result.add(curr.val);
            if (curr.right != null) stack.push(curr.right); // so smart here, as we are looking for order M-L-R, so we revert it this from stack
            if (curr.left != null) stack.push(curr.left);
        }

        return result;
    }

    public static List<Integer> preOrder(TreeNode node) {
        List<Integer> result = new ArrayList<>();
        preOrderHelper(node, result);

        return result;
    }

    private static void preOrderHelper(TreeNode node, List<Integer> result) {
        if (node != null) {
            result.add(node.val);
            preOrderHelper(node.left, result);
            preOrderHelper(node.right, result);
        }
    }

    public static List<Integer> inOrderIterative(TreeNode node) {
        List<Integer> result = new ArrayList<>();
        if (node == null) {
            return result;
        }

        Stack<TreeNode> stack = new Stack<>();
        TreeNode curr = node;
        while (curr != null || !stack.isEmpty()) {
            if (curr != null) {
                // traverse all the way to the bottom first via going down the left path
                stack.push(curr);
                curr = curr.left;
            } else {
                curr = stack.pop();
                result.add(curr.val);
                // going right
                curr = curr.right;
            }
        }
        return result;
    }

    public static List<Integer> inOrder(TreeNode node) {
        List<Integer> result = new ArrayList<>();
        inOrderHelper(node, result);

        return result;
    }

    private static void inOrderHelper(TreeNode node, List<Integer> result) {
        if (node != null) {
            inOrderHelper(node.left, result);
            result.add(node.val);
            inOrderHelper(node.right, result);
        }
    }

    public static List<Integer> postOrderIterative(TreeNode node) {
        List<Integer> result = new ArrayList<>();
        if (node == null) {
            return result;
        }

        Stack<TreeNode> stack = new Stack<>();
        stack.push(node);
        List<Integer> tempList = new ArrayList<>();
        while (!stack.isEmpty()) {
            TreeNode curr = stack.pop();
            // we do slight moderation from the pre-order traversal. In preorder, we get, M-L-R, but here we want L-R-M
            // so we will flip the two stack pushes to become M-R-L, and then reverse the resulting list to get L-R-M
            tempList.add(curr.val);
            if (curr.left != null) stack.push(curr.left);
            if (curr.right != null) stack.push(curr.right);
        }

        for (int i = tempList.size()-1; i >= 0; i--) {
            result.add(tempList.get(i));
        }
        return result;
    }

    public static List<Integer> postOrder(TreeNode node) {
        List<Integer> result = new ArrayList<>();
        postOrderHelper(node, result);

        return result;
    }

    private static void postOrderHelper(TreeNode node, List<Integer> result) {
        if (node != null) {
            postOrderHelper(node.left, result);
            postOrderHelper(node.right, result);
            result.add(node.val);
        }
    }
}
