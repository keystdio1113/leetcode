package first.binaryTree;

import model.TreeNode;

public class Day23_108_ConvertSortedArrayToBST {
    public static void main(String[] args) {
        sortedArrayToBST(new int[]{-10,-3,0,5,9});
    }

    public static TreeNode sortedArrayToBST(int[] nums) {
        return build(nums);
    }

    public static TreeNode build(int[] nums) {
        int mid = nums.length / 2;
        int[] leftSubTree = getSubArray(nums, 0, mid-1);
        int[] rightSubTree = getSubArray(nums, mid+1, nums.length-1);

        TreeNode root = new TreeNode(nums[mid]);
        if (leftSubTree.length > 0) {
            root.left = build(leftSubTree);
        }

        if (rightSubTree.length > 0) {
            root.right = build(rightSubTree);
        }

        return root;
    }

    private static int[] getSubArray(int[] nums, int l, int h) {
        int size = h - l + 1;
        if (size <= 0) {
            return new int[]{};
        } else {
            int[] result = new int[size];
            for (int i = 0; i < size; i++) {
                result[i] = nums[l+i];
            }
            return result;
        }
    }
}
