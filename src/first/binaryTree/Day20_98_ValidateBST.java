package first.binaryTree;

import model.TreeNode;

import java.util.List;

public class Day20_98_ValidateBST {
    private static TreeNode prev;
    public boolean isValidBST(TreeNode root) {
//        return validateWRONG_SOLUTION(root);

//        List<Integer> inOrder = new ArrayList<>();
//        getInOrderTraversal(root, inOrder);
//        for (int i = 0; i < inOrder.size()-1; i++) {
//            if (inOrder.get(i) >= inOrder.get(i+1)) {
//                return false;
//            }
//        }
//        return true;

        prev = null;
        return inOrderValidation(root);
    }

    public static boolean inOrderValidation(TreeNode root) {
        if (root == null) {
            return true;
        } else {
            boolean leftValid = inOrderValidation(root.left);
            boolean midValid = true;
            if (prev != null && root.val <= prev.val) {
                midValid = false;
                // return midValid; // Note this is optional as we could end recursion right here.
            }
            prev = root;
            boolean rightValid = inOrderValidation(root.right);
            return midValid && leftValid && rightValid;
        }
    }

    public static void getInOrderTraversal(TreeNode root, List<Integer> order) {
        if (root != null) {
            getInOrderTraversal(root.left, order);
            order.add(root.val);
            getInOrderTraversal(root.right, order);
        }
    }

    // This is a wrong solution because this ignores the property of BST.
    //   "EVERY" descendent nodes in the left subtree must be smaller than the node, this solution only looks at child nodes
    //   "EVERY" descendent nodes in the right subtree must be larger than the node, this solution only looks at child nodes
    //      10
    //    2    19
    //        5   21
    //   The "5" node is wrong
    public static boolean validateWRONG_SOLUTION(TreeNode root) {
        if (root == null) {
            return true;
        } else {
            boolean leftIsBST = validateWRONG_SOLUTION(root.left);
            boolean rightIsBST = validateWRONG_SOLUTION(root.right);

            boolean currentIsBST = true;
            if (root.left != null) {
                currentIsBST = root.val > root.left.val;
            }
            if (root.right != null) {
                currentIsBST = root.val < root.right.val;
            }

            return currentIsBST && leftIsBST && rightIsBST;
        }
    }
}
