package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day28_93_RestoreIPAddress {
    public static void main(String[] args) {
        System.out.println(restoreIpAddresses("1111"));
    }

    public static List<String> restoreIpAddresses(String s) {
        List<String> result = new ArrayList<>();
//        restore(result, s, new ArrayList<>(), 0);
        restoreCarlGraph(result, s, new ArrayList<>(), 0);
        return result;
    }

    public static void restore(List<String> result, String s, List<String> path, int startI) {
        // this is different from normal partitioning as we have a fixed size of partitions desired
        // So we need to stop early rather than letting startI go all the way to the of @s
        if (path.size() == 3) {
            path.add(s.substring(startI));
            boolean allValid = path.stream().allMatch(n -> {
                long value = Long.parseLong(n);
                if (value == 0) {
                    return n.length() == 1;
                } else {
                    return !n.startsWith("0") && value >= 0 && value <= 255;
                }
            });
            if (allValid) {
                result.add(String.join(".", path));
            }
            path.remove(path.size()-1);
        } else {
            for (int i = startI+1; i < s.length(); i++) {
                path.add(s.substring(startI, i));
                restore(result, s, path, i);
                path.remove(path.size()-1);
            }
        }
    }

    public static void restoreCarlGraph(List<String> result, String s, List<String> path, int startI) {
        if (path.size() == 3) {
            String lastSubstring = s.substring(startI);
            if (isValidNumber(lastSubstring)) {
                result.add(String.join(".", path) + "." + lastSubstring);
            }
        } else {
            for (int i = startI+1; i < s.length(); i++) {
                String substring = s.substring(startI, i);
                if (isValidNumber(substring)) {
                    path.add(substring);
                    restoreCarlGraph(result, s, path, i);
                    path.remove(path.size()-1);
                }
            }
        }
    }

    private static boolean isValidNumber(String n) {
        if (n.length() == 0) return false;
        long value = -1;
        try {
            value = Long.parseLong(n);
        } catch (Exception e) {
            return false; // Not sure about this assumptions...
        }

        if (value == 0) {
            return n.length() == 1;
        } else {
            return !n.startsWith("0") && value >= 0 && value <= 255;
        }
    }
}
