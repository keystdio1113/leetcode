package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.List;

public class Day27_131_PalindromPartition {
    public static void main(String[] args) {
        System.out.println(partition("aab"));
    }

    public static List<List<String>> partition(String s) {
        List<List<String>> results = new ArrayList<>();
//        if (s.length() == 1) {
//            return List.of(List.of(s));
//        } else if (s.length() == 2) {
//            results.add(List.of(s.charAt(0) + "", s.charAt(1) + ""));
//            if (s.charAt(0) == s.charAt(1)) {
//                results.add(List.of(s));
//            }
//        } else {
//            boolean[] sticks = new boolean[s.length()-1];
//            solveWithMatchStickAnalogy(results, s, sticks, 0);
//        }
//        return results;

        partitioning(results, s, new ArrayList<>(), 0);
        return results;
    }

    public static void partitioning(List<List<String>> results, String s, List<String> curr, int startI) {
        if (startI == s.length()) {
            results.add(new ArrayList<>(curr));
        } else {
            // We currently has strings [startI, i]
            for (int i = startI; i < s.length(); i++) {
                String substring = s.substring(startI, i+1);
                if (isPalin(substring)) {
                    curr.add(substring);
                    partitioning(results, s, curr, i+1);
                    curr.remove(curr.size()-1);
                }
            }
        }
    }

    // Define partitioning
    // String: [0, 1, 2, 3]
    // Partitioning first.array: [F, F, F], each index means if we want to put a match stick to the right of s[i]. this first.array will always be size s.length-1
    // We will then specially handle size of 1 and 2, then the rest we could backtrack.
    public static void solveWithMatchStickAnalogy(List<List<String>> results, String s, boolean[] sticks, int level) {
        if (level == sticks.length) {
            List<String> partitionStrings = convertSticksToPartition(s, sticks);
            for (String partition: partitionStrings) {
                if (!isPalin(partition)) {
                    return;
                }
            }
            results.add(partitionStrings);
        } else {
            sticks[level] = false;
            solveWithMatchStickAnalogy(results, s, sticks, level+1);
            sticks[level] = false;

            sticks[level] = true;
            solveWithMatchStickAnalogy(results, s, sticks, level+1);
            sticks[level] = false;
        }
    }

    // String: [0, 1, 2, 3]
    // Partitioning first.array: [F, F, F], each index means if we want to put a match stick to the right of s[i]. this first.array will always be size s.length-1
    private static List<String> convertSticksToPartition(String s, boolean[] sticks) {
        List<String> partition = new ArrayList<>();
        int i = 0;
        int j = 0;
        while (j < sticks.length) {
            if (sticks[j]) {
                partition.add(s.substring(i, j+1));
                i = j+1;
            }
            j++;
        }
        partition.add(s.substring(i, j+1));
        return partition;
    }

    private static boolean isPalin(String s) {
        if (s.length() == 0) {
            return true;
        } else {
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) != s.charAt(s.length()-i-1)) {
                    return false;
                }
            }
            return true;
        }
    }
}
