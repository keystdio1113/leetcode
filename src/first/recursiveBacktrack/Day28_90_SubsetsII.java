package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day28_90_SubsetsII {
    public static void main(String[] args) {
        System.out.println(subsetsWithDup(new int[]{1, 1}));
    }

    public static List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> results = new ArrayList<>();
        Arrays.sort(nums);

        build(results, new ArrayList<>(), nums, new boolean[nums.length], 0);
        return results;
    }

    private static void build(List<List<Integer>> results, List<Integer> path, int[] nums, boolean[] used, int startI) {
        if (startI > nums.length) {
            return;
        } else {
            results.add(new ArrayList<>(path));
            for (int i = startI; i < nums.length; i++) {
                if (i >= 1 && nums[i] == nums[i-1] && !used[i-1]) {
                    continue;
                }
                path.add(nums[i]);
                used[i] = true;
                build(results, path, nums, used, i+1);
                path.remove(path.size()-1);
                used[i] = false;
            }
        }
    }
}
