package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.List;

public class Day28_78_Subsets {
    public static void main(String[] args) {
        System.out.println(subsets(new int[]{1, 2, 3}));
    }

    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> results = new ArrayList<>();
//        buildCollectAtLeafNode(results, new ArrayList<>(), nums, 0);
        buildAsExpandingNodes(results, new ArrayList<>(), nums, 0);
        return results;
    }

    public static void buildCollectAtLeafNode(List<List<Integer>> results, List<Integer> path, int[] nums, int startI) {
        if (startI == nums.length) {
            results.add(new ArrayList<>(path));
        } else {
            path.add(nums[startI]);
            buildCollectAtLeafNode(results, path, nums, startI+1);
            path.remove(path.size()-1);

            buildCollectAtLeafNode(results, path, nums, startI+1);
        }
    }

    public static void buildAsExpandingNodes(List<List<Integer>> results, List<Integer> path, int[] nums, int startI) {
        if (startI <= nums.length) { // not necessary, as this will be controlled by the for loop in the backtrack step anyway
            results.add(new ArrayList<>(path));
            for (int i = startI; i < nums.length; i++) {
                path.add(nums[i]);
                buildAsExpandingNodes(results, path, nums, i+1);
                path.remove(path.size()-1);
            }
        }
    }
}
