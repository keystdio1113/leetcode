package first.recursiveBacktrack;

import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day25_216_CombinationSumIII {
    public static void main(String[] args) {
        System.out.println(combinationSum3(3, 9));
    }

    public static List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> results = new ArrayList<>();
        build(results, new HashSet<>(), k, n, 0, 1);
        return results;
    }

    public static void build(List<List<Integer>> results, Set<Integer> curr, int k, int n, int sum, int startI) {
        if (curr.size() == k) {
            if (sum == n) {
                results.add(new ArrayList<>(curr));
            }
        } else {
            if (startI > (n-sum)) {
                return;
            }
            for (int i = startI; i <= 9; i++) {
                curr.add(i);
                build(results, curr, k, n, sum + i, i+1);
                curr.remove(i);
            }
        }
    }
}
