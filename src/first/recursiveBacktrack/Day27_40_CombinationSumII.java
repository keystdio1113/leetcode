package first.recursiveBacktrack;

import java.util.*;
import java.util.stream.Collectors;

public class Day27_40_CombinationSumII {
    public static void main(String[] args) {
        System.out.println(combinationSum2(new int[]{10,1,2,7,6,1,5}, 8));
    }

    public static List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> results = new ArrayList<>();

        Arrays.sort(candidates);
        solveLayerDedupping(results, candidates, new ArrayList<>(), 0, target, 0, new boolean[candidates.length]);
        return results;
    }


    private static void solveLayerDedupping(List<List<Integer>> results, int[] candidates, List<Integer> curr, int currSum, int target, int startI, boolean[] used) {
        if (currSum > target) {
            return;
        } else if (currSum == target) {
            results.add(new ArrayList<>(curr));
        } else {
            for (int i = startI; i < candidates.length; i++) {
                if (i>0 && candidates[i]==candidates[i-1] && !used[i-1]) {
                    continue;
                }
                curr.add(candidates[i]);
                used[i] = true;
                solveLayerDedupping(results, candidates, curr, currSum+candidates[i], target, i+1, used);
                curr.remove(curr.size()-1);
                used[i] = false;
            }
        }
    }

    // this timeouts... :(
    private static void solveSetDedupping(List<List<Integer>> results, int[] candidates, List<Integer> curr, int currSum, int target, int startI, Set<String> visited) {
        if (currSum > target) {
            return;
        } else if (currSum == target) {
            String normalizedSolution = curr.stream().sorted().map(i -> i + "").collect(Collectors.joining(","));
            if (!visited.contains(normalizedSolution)) {
                results.add(new ArrayList<>(curr));
                visited.add(normalizedSolution);
            }
        } else {
            for (int i = startI; i < candidates.length; i++) {
                curr.add(candidates[i]);
                solveSetDedupping(results, candidates, curr, currSum+candidates[i], target, i+1, visited);
                curr.remove(curr.size()-1);
            }
        }
    }
}
