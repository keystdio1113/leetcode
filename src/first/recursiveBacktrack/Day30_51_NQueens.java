package first.recursiveBacktrack;

import first.utils.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Day30_51_NQueens {
    public static void main(String[] args) {
        System.out.println(solveNQueens(9));
    }

    public static List<List<String>> solveNQueens(int n) {
        ChessBoard cb = new ChessBoard(n);
        List<Pair<Integer, Integer>> validCoords = new ArrayList<>();
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                validCoords.add(new Pair<>(row, col));
            }
        }

        List<List<String>> results = new ArrayList<>();
//        find(results, n, cb, validCoords, 0, null);

        int diagnolCount = 2*(n-1) + 1;
        findWithExclusionArrays(results, n, cb, validCoords, 0, new int[n], new int[n], new int[diagnolCount], new int[diagnolCount]);
        return results;
    }


    public static void findWithExclusionArrays(List<List<String>> results, int n, ChessBoard cb, List<Pair<Integer, Integer>> validCoords,
                                               int startI,
                                               int[] rowValid,
                                               int [] colValid,
                                               int[] diagnolValid,
                                               int[] antiDiagnolValid
    ) {
        if (cb.queens.size() > n) return;
        if (cb.queens.size() == n) {
            results.add(cb.toOutputFormat());
        } else {
            for (int i = startI; i < validCoords.size(); i++) {
                Pair<Integer, Integer> currCoord = validCoords.get(i);
                int row = currCoord.k;
                int col = currCoord.v;
                int diagnol = row-col + n-1;
                int antiDiagnol = col+row;

                if (rowValid[row] + colValid[col] + diagnolValid[diagnol] + antiDiagnolValid[antiDiagnol] == 0) {
                    rowValid[row]++;
                    colValid[col]++;
                    diagnolValid[diagnol]++;
                    antiDiagnolValid[antiDiagnol]++;
                    cb.addQueen(row, col);
                    find(results, n, cb, validCoords, i+1, currCoord);
                    cb.removeQueen();
                    rowValid[row]--;
                    colValid[col]--;
                    diagnolValid[diagnol]--;
                    antiDiagnolValid[antiDiagnol]--;
                }
            }
        }
    }



    // Used too many data structures and slow down a bit
    // but passed N=8, will optimize next time maybe
    public static void find(List<List<String>> results, int n, ChessBoard cb, List<Pair<Integer, Integer>> validCoords, int startI, Pair<Integer, Integer> prev) {
        if (cb.queens.size() > n) return;
        if (cb.queens.size() == n) {
            if (cb.isValid()) {
                results.add(cb.toOutputFormat());
            }
        } else {
            for (int i = startI; i < validCoords.size(); i++) {
                Pair<Integer, Integer> currCoord = validCoords.get(i);

                if (prev != null && (
                        prev.k == currCoord.k || // same row
                        prev.v == currCoord.v || // same column
                        Math.abs(prev.k-currCoord.k) == Math.abs(prev.v- currCoord.v) // same diagnol
                )) {
                    continue;
                }
                cb.addQueen(currCoord.k, currCoord.v);
                find(results, n, cb, validCoords, i+1, currCoord);
                cb.removeQueen();
            }
        }
    }

    private static class ChessBoard {
        private int n;
        private boolean[][] board;
        public List<Pair<Integer, Integer>> queens;

        public ChessBoard(int n) {
            this.n = n;
            this.board = new boolean[n][n];
            this.queens = new ArrayList<>();
        }

        public void addQueen(int row, int col) {
            board[row][col] = true;
            queens.add(new Pair<>(row, col));
        }

        public void removeQueen() {
            Pair<Integer, Integer> removed = queens.remove(queens.size()-1);
            board[removed.k][removed.v] = false;
        }

        public boolean isValid() {
            if (queens.size() != n) {
                throw new IllegalStateException(String.format("We need %s queens, we only have %s now", n, queens.size()));
            }
            Set<Integer> rows = queens.stream().map(q -> q.k).collect(Collectors.toSet());
            if (rows.size() != n) {
                return false;
            }
            Set<Integer> cols = queens.stream().map(q -> q.v).collect(Collectors.toSet());
            if (cols.size() != n) {
                return false;
            }

            for (int i = 0; i < queens.size(); i++) {
                for (int j = i+1; j < queens.size(); j++) {
                    Pair<Integer, Integer> q1 = queens.get(i);
                    Pair<Integer, Integer> q2 = queens.get(j);

                    int rowDiff = Math.abs(q1.k - q2.k);
                    int colDiff = Math.abs(q1.v - q2.v);
                    if (rowDiff == colDiff) {
                        return false;
                    }
                }
            }

            return true;
        }

        public List<String> toOutputFormat() {
            List<String> results = new ArrayList<>();
            for (int row = 0; row < n; row++) {
                StringBuilder colStrSb = new StringBuilder();
                for (int col = 0; col < n; col++) {
                    if (board[row][col]) {
                        colStrSb.append("Q");
                    } else {
                        colStrSb.append(".");
                    }
                }
                results.add(colStrSb.toString());
            }
            return results;
        }
    }
}
