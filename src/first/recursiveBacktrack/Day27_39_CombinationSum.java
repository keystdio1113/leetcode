package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.List;

public class Day27_39_CombinationSum {
    public static void main(String[] args) {
        System.out.println(combinationSum(new int[]{2,3,6,7}, 7));
    }

    public static List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> results = new ArrayList<>();
        solve(results, candidates, new ArrayList<>(), 0, target, 0);
        return results;
    }

    private static void solve(List<List<Integer>> results, int[] candidates, List<Integer> curr, int currSum, int target, int startI) {
        if (currSum > target) {
            return;
        } else if (currSum == target) {
            results.add(new ArrayList<>(curr));
        } else {
            for (int i = startI; i < candidates.length; i++) {
                curr.add(candidates[i]);
                solve(results, candidates, curr, currSum+candidates[i], target, i);
                curr.remove(curr.size()-1);
            }
        }
    }
}
