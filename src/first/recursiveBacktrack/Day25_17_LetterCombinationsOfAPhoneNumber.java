package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day25_17_LetterCombinationsOfAPhoneNumber {
    public static void main(String[] args) {
        System.out.println(letterCombinations("23"));
    }

    public static List<String> letterCombinations(String digits) {
        if (digits.length() == 0) {
            return List.of();
        }

        Map<String, List<String>> numberToLetters = Map.of(
                "2", List.of("a", "b", "c"),
                "3", List.of("d", "e", "f"),
                "4", List.of("g", "h", "i"),
                "5", List.of("j", "k", "l"),
                "6", List.of("m", "n", "o"),
                "7", List.of("p", "q", "r", "s"),
                "8", List.of("t", "u", "v"),
                "9", List.of("w", "x", "y", "z")
        );

        List<String> results = new ArrayList<>();
        build(digits, results, numberToLetters, "", 0);
        return results;
    }

    private static void build(String digits, List<String> results, Map<String, List<String>> numbersToLetters, String curr, int startI) {
        if (curr.length() == digits.length()) {
            results.add(curr);
        } else {
            for (int i = startI; i < digits.length(); i++) {
                List<String> lettersForNum = numbersToLetters.get(digits.charAt(i) + "");
                for (String letter: lettersForNum) {
                    build(digits, results, numbersToLetters, curr + letter, i+1);
                }
            }
        }
    }

    private static void buildFaster(String digits, List<String> results, Map<String, List<String>> numbersToLetters, String curr, int startI) {
        if (curr.length() == digits.length()) {
            results.add(curr);
        } else {
            List<String> lettersForNum = numbersToLetters.get(digits.charAt(startI) + "");
            for (String letter: lettersForNum) {
                buildFaster(digits, results, numbersToLetters, curr + letter, startI+1);
            }
        }
    }
}
