package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day29_46_Permutations {
    public static void main(String[] args) {
        System.out.println(permute(new int[]{1, 2, 3}));
    }

    public static List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> results = new ArrayList<>();
        build(results, new HashSet<>(), new ArrayList<>(), nums);
        return results;
    }

    public static void build(List<List<Integer>> results, Set<Integer> used, List<Integer> path, int[] nums) {
        if (path.size() == nums.length) {
            results.add(new ArrayList<>(path));
        } else {
            for (int i = 0; i < nums.length; i++) {
                if (!used.contains(nums[i])) {
                    path.add(nums[i]);
                    used.add(nums[i]);
                    build(results, used, path, nums);
                    path.remove(path.size()-1);
                    used.remove(nums[i]);
                }
            }
        }
    }
}
