package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day29_491_NonDecreasingSubSequence {
    public static void main(String[] args) {
        System.out.println(findSubsequences(new int[]{4, 6, 7, 7}));
    }

    public static List<List<Integer>> findSubsequences(int[] nums) {
        List<List<Integer>> results = new ArrayList<>();
        buildOneHashSetDedupForEachParents(results, new ArrayList<>(), nums, 0);
//        buildOneHashSetDedupForAll(results, new HashSet<>(), new ArrayList<>(), nums, 0);
        return results;
    }

    public static void buildDeduppingUsingSet(List<List<Integer>> results, List<Integer> path, int[] nums, Set<String> visited, int startI) {
        if (visited.contains(path.toString())) {
            return;
        }
        if (path.size() >= 2) {
            results.add(new ArrayList<>(path));
            visited.add(path.toString());
        }
        for (int i = startI; i < nums.length; i++) {
            if (path.size() == 0 || nums[i] >= path.get(path.size()-1)) {
                path.add(nums[i]);
                buildDeduppingUsingSet(results, path, nums, visited, i+1);
                path.remove(path.size()-1);
            }
        }
    }

    public static void buildMultiHashMapsDedup(List<List<Integer>> results, List<Integer> path, int[] nums, Set<Integer> visited, int startI) {
        if (path.size() >= 2) {
            results.add(new ArrayList<>(path));
        }
        for (int i = startI; i < nums.length; i++) {
            if (visited.contains(nums[i])) {
                continue;
            }
            visited.add(nums[i]);
            if (path.size() == 0 || nums[i] >= path.get(path.size()-1)) {
                path.add(nums[i]);
                buildDeduppingUsingSet(results, path, nums, new HashSet<>(), i+1);
                path.remove(path.size()-1);
            }
        }
    }

    public static void buildOneHashSetDedupForEachParents(List<List<Integer>> results, List<Integer> path, int[] nums, int startI) {
        if (path.size() >= 2) {
            results.add(new ArrayList<>(path));
        }

        HashSet<Integer> visited = new HashSet<>();
        for (int i = startI; i < nums.length; i++) {
            boolean hasUsed = visited.contains(nums[i]);
            boolean nonDeceasing = path.isEmpty() || (nums[i] >= path.get(path.size()-1));
            if (hasUsed || !nonDeceasing) {
                continue;
            }

            visited.add(nums[i]);
            path.add(nums[i]);
            buildOneHashSetDedupForEachParents(results, path, nums, i+1);
            path.remove(path.size()-1);
        }
    }

    public static void buildOneHashSetDedupForAll(List<List<Integer>> results, Set<Integer> visited, List<Integer> path, int[] nums, int startI) {
        if (path.size() >= 2) {
            results.add(new ArrayList<>(path));
        }

        for (int i = startI; i < nums.length; i++) {
            boolean hasUsed = visited.contains(nums[i]);
            boolean nonDeceasing = path.isEmpty() || (nums[i] >= path.get(path.size()-1));
            if (hasUsed || !nonDeceasing) {
                continue;
            }

            visited.add(nums[i]);
            path.add(nums[i]);
            buildOneHashSetDedupForAll(results, visited, path, nums, i+1);
            path.remove(path.size()-1);
            visited.remove(nums[i]);
        }
    }
}
