package first.recursiveBacktrack;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day30_37_Sudoku {
    public static void main(String[] args) {
        solveSudoku(new char[][]{
                new char[]{'5','3','.','.','7','.','.','.','.'},
                new char[]{'6','.','.','1','9','5','.','.','.'},
                new char[]{'.','9','8','.','.','.','.','6','.'},
                new char[]{'8','.','.','.','6','.','.','.','3'},
                new char[]{'4','.','.','8','.','3','.','.','1'},
                new char[]{'7','.','.','.','2','.','.','.','6'},
                new char[]{'.','6','.','.','.','.','2','8','.'},
                new char[]{'.','.','.','4','1','9','.','.','5'},
                new char[]{'.','.','.','.','8','.','.','7','9'},
        });
    }

    private static final List<Character> ALL_POTENTIAL_CHARS = List.of('1','2','3','4','5','6','7','8','9');

    public static void solveSudoku(char[][] board) {
        // these indicates if we could put a number in row:
        //    rowValid[0][2] = 0 -> means we could put in number 2 in row 0
        //    rowValid[0][2] = 1 -> means we could NOT put in number 2 in row 0
        int[][] rowValid = new int[9][10];
        int[][] colValid = new int[9][10];
        int[][] boxValid = new int[9][10];

        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (board[row][col] != '.') {
                    int d = Integer.parseInt(board[row][col] + "");
                    int box = 3*(row/3) + (col/3);
                    rowValid[row][d] = 1;
                    colValid[col][d] = 1;
                    boxValid[box][d] = 1;
                }
            }
        }

        if (solve(0, board, rowValid, colValid, boxValid)) {
            System.out.println("YEAH");
            int a = 0;
        } else {
            throw new IllegalStateException("No solution found :)");
        }
    }

    private static boolean solve(int startI, char[][] board, int[][] rowValid, int[][] colValid, int[][] boxValid) {
        int row = startI / 9;
        int col = startI % 9;
        int box = 3*(row/3) + (col/3);

        // if (row == board.length-1 && col == board.length-1) { Not this because we need to go pass the last element
        if (row == board.length) {
            if (isValidFinal(board)) {
                return true;
            }
        } else {
            if (board[row][col] == '.') {
                for (char potential: ALL_POTENTIAL_CHARS) {
                    int d = Integer.parseInt(potential+ "");
                    if (rowValid[row][d] + colValid[col][d] + boxValid[box][d] == 0) {
                        rowValid[row][d]++;
                        colValid[col][d]++;
                        boxValid[box][d]++;

                        board[row][col] = potential;
                        boolean val = solve(startI+1, board, rowValid, colValid, boxValid);
                        if (val) {
                            return true;
                        }
                        board[row][col] = '.';
                        rowValid[row][d]--;
                        colValid[col][d]--;
                        boxValid[box][d]--;
                    }
                }
            } else {
                return solve(startI+1, board, rowValid, colValid, boxValid);
            }
        }
        return false;
    }

    private static Set<Character> getPotentials(List<Character> allPotentialChars, int row, int col, char[][] board) {
        Set<Character> results = new HashSet<>(ALL_POTENTIAL_CHARS);
        for (int i = 0; i < 9; i++) {
            if (board[row][i] != '.') {
                results.remove(board[row][i]);
            }
        }

        for (int i = 0; i < 9; i++) {
            if (board[i][col] != '.') {
                results.remove(board[i][col]);
            }
        }

        int squareRow = row / 3;
        int squareCol = col / 3;

        for (int i = squareRow; i < squareRow+3; i++) {
            for (int j = squareCol; j < squareCol+3; j++) {
                if (board[i][j] != '.') {
                    results.remove(board[i][j]);
                }
            }
        }

        return results;
    }

    private static boolean isValidFinal(char[][] board) {
        for (int row = 0; row < board.length; row++) {
            Set<Character> rowNumbers = new HashSet<>();
            for (int col = 0; col < board.length; col++) {
                rowNumbers.add(board[row][col]);
            }
            if (rowNumbers.size() != 9) return false;
        }

        for (int col = 0; col < board.length; col++) {
            Set<Character> colNumbers = new HashSet<>();
            for (int row = 0; col < board.length; col++) {
                colNumbers.add(board[row][col]);
            }
            if (colNumbers.size() != 9) return false;
        }

        for (int squareI = 0; squareI < 9; squareI++) {
            int startRow = (squareI / 3) * 3;
            int startCol = (squareI % 3) * 3;
            Set<Character> squareNumbers = new HashSet<>();
            for (int row = startRow; row < startRow + 3; row++) {
                for (int col = startCol; col < startCol + 3; col++) {
                    squareNumbers.add(board[row][col]);
                }
            }
            if (squareNumbers.size() != 9) return false;
        }

        return true;
    }
}
