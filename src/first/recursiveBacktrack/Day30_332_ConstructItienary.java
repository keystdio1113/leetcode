package first.recursiveBacktrack;

import java.util.*;

public class Day30_332_ConstructItienary {
    public static List<String> RESULTS = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println(findItinerary(List.of(
                List.of("MUC","LHR"),
                List.of("JFK","MUC"),
                List.of("SFO","SJC"),
                List.of("LHR","SFO")
        )));

        System.out.println(findItinerary(List.of(
                List.of("JFK","SFO"),
                List.of("JFK","ATL"),
                List.of("SFO","ATL"),
                List.of("ATL","JFK"),
                List.of("ATL","SFO")
        )));

        System.out.println(findItinerary(List.of(
                List.of("JFK","KUL"),
                List.of("JFK","NRT"),
                List.of("NRT","JFK")
        )));

        System.out.println(findItinerary(List.of(
                List.of("EZE","AXA"),
                List.of("TIA","ANU"),
                List.of("ANU","JFK"),
                List.of("JFK","ANU"),
                List.of("ANU","EZE"),
                List.of("TIA","ANU"),
                List.of("AXA","TIA"),
                List.of("TIA","JFK"),
                List.of("ANU","TIA"),
                List.of("JFK","TIA")
        )));
    }

    public static List<String> findItinerary(List<List<String>> tickets) {
        return findByFirstConstructGraphThenRunDFS(tickets);
    }

    public static List<String> findByFirstConstructGraphThenRunDFS(List<List<String>> tickets) {
        Map<String, Map<String, Integer>> cityToSortedCities = buildGraph(tickets);
//        System.out.println(cityToSortedCities);
        List<String> path = new ArrayList<>();
        Set<String> visited = new HashSet<>();
        path.add("JFK");
        if (recursiveFind(cityToSortedCities, visited, path, tickets.size())) {
            return RESULTS;
        } else {
            return List.of();
        }
    }

    private static boolean recursiveFind(Map<String, Map<String, Integer>> cityToSortedCities, Set<String> usedEdges, List<String> path, int totalEdges) {
//        System.out.println("Current path: " + path);
        if (path.size() == totalEdges+1) {
            RESULTS = new ArrayList<>(path);
            return true;
        } else {
            String lastCity = path.get(path.size()-1);
            // we could have NULL destinations for this current source
            Map<String, Integer> destinationsWithQuota = Optional.ofNullable(cityToSortedCities.get(lastCity))
                    .orElse(Map.of());

            for (String destination: destinationsWithQuota.keySet()) {
                int remainingQuota = destinationsWithQuota.get(destination);
                if (remainingQuota > 0) {
                    path.add(destination);
                    destinationsWithQuota.put(destination, remainingQuota-1);
                    if (recursiveFind(cityToSortedCities, usedEdges, path, totalEdges)) {
                        return true;
                    };
                    path.remove(path.size()-1);
                    destinationsWithQuota.put(destination, remainingQuota);
                }
            }
            return false;
        }
    }

    private static Map<String, Map<String, Integer>> buildGraph(List<List<String>> tickets) {
        Map<String, Map<String, Integer>> graph = new HashMap<>();
        for (List<String> ticket: tickets) {
            String src = ticket.get(0);
            String dst = ticket.get(1);

            if (!graph.containsKey(src)) {
                graph.put(src, new TreeMap<>());
            }
            if (!graph.get(src).containsKey(dst)) {
                graph.get(src).put(dst, 0);
            }

            int existingDstValue = graph.get(src).get(dst);
            graph.get(src).put(dst, existingDstValue+1);
        }
        return graph;
    }
}
