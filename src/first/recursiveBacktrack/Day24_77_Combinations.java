package first.recursiveBacktrack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day24_77_Combinations {
    public static void main(String[] args) {
        System.out.println(combine(6, 2));
    }

    public static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> results = new ArrayList<>();
        combine(new HashSet<>(), results, n, k, 1);
        return results;
    }

    /**
     * Why I cannot get this cutting right LOLLL?
     *
     */
    public static void combine(Set<Integer> curr, List<List<Integer>> results, int n, int k, int startI) {
        // one way to cut unnecessary branches.
        // Let's say n=6, k=4, we should stop when the path contains [4], so startI > n-k+1
//        if (curr.size() + (n-startI+1) < k) {
//            return;
//        }

        if (curr.size() == k) {
            List<Integer> clone = new ArrayList<>(curr); // assume it make hard copies
            results.add(clone);
        } else {
            for (int i = startI; i <= n; i++) {
                curr.add(i);
                // another way to cut unnecessary branch.
                // Let's say n=6, k=4, we should stop spawning new branch when the path contains
                if (curr.size() + (n-i+1+1) >= k) {
                    combine(curr, results, n, k, i + 1);
                }
                curr.remove(i);
            }
        }
    }
}
