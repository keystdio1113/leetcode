package first.recursiveBacktrack;

import java.util.*;

public class Day29_47_PermutationsII {
    public static void main(String[] args) {
        System.out.println(permuteUnique(new int[]{1, 1, 2}));
    }


    public static List<List<Integer>> permuteUnique(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> results = new ArrayList<>();
        build(results, new boolean[nums.length], new ArrayList<>(), nums);
        return results;
    }

    public static void build(List<List<Integer>> results, boolean[] used, List<Integer> path, int[] nums) {
        if (path.size() == nums.length) {
            results.add(new ArrayList<>(path));
        } else {
            for (int i = 0; i < nums.length; i++) {
                if (used[i]) continue; // we have used this element before
                if (i>0 && nums[i]==nums[i-1] && !used[i-1]) continue; // we are going to have repeated path here.

                path.add(nums[i]);
                used[i] = true;
                build(results, used, path, nums);
                path.remove(path.size()-1);
                used[i] = false;
            }
        }
    }
}
