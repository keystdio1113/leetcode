package first.utils;

public class Pair<K, V> {
    public K k;
    public V v;
    public Pair(K k, V v) {
        this.k = k;
        this.v = v;
    }

    @Override
    public String toString() {
        return String.format("[%s, %s]", k, v);
    }
}
