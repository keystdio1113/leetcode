package first.utils;

import java.util.Arrays;

public class PrintUtils {
    public static void print2DMatrixLike(int[][] a) {
        System.out.println("[");
        for (int i = 0; i < a.length; i++) {
            System.out.println("\t" + Arrays.toString(a[i]) + ",");
        }
        System.out.println("]");
    }
}
