package first.hashTable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Day06_349_IntersectionOfTwoArrays {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(intersection(new int[]{1,2,2,1}, new int[]{2,2})));

    }


    public static int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> s1 = new HashSet<>();
        for (int i: nums1) {
            s1.add(i);
        }
        Set<Integer> intersects = new HashSet<>();
        for (int i: nums2) {
            if (s1.contains(i)) {
                intersects.add(i);
            }
        }
        int[] results = new int[intersects.size()];
        int i = 0;
        for (int num: intersects) {
            results[i] = num;
            i++;
        }
        return results;
    }
}
