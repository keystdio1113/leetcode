package first.hashTable;

public class Day07_383_RansomNote {
    public static void main(String[] args) {
        System.out.println(solution("aa", "adawdaw"));
        System.out.println(solution("a", "b"));
        System.out.println(solution("aa", "ab"));
    }

    public static boolean solution(String ransomNote, String magazine) {
        int[] mCount = new int[26];

        for (int i = 0; i < magazine.length(); i++) {
            mCount[magazine.charAt(i) - 'a']++;
        }

        for (int i = 0; i < ransomNote.length(); i++) {
            mCount[ransomNote.charAt(i) - 'a']--;
        }

        for (int i: mCount) {
            if (i < 0) {
                return false;
            }
        }

        return true;
    }
}
