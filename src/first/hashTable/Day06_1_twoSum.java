package first.hashTable;

import java.util.*;

public class Day06_1_twoSum {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(twoSumCarl(new int[]{3,3}, 6)));
        System.out.println(Arrays.toString(twoSumCarl(new int[]{3,2,3}, 6)));
        System.out.println(Arrays.toString(twoSumCarl(new int[]{2,7,11,15}, 9)));
        System.out.println(Arrays.toString(twoSumCarl(new int[]{2,4,11,3}, 6)));

    }

    public static int[] twoSumCarl(int[] nums, int target) {
        Map<Integer, Integer> visitedNumToIndex = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int currNum = nums[i];
            int toFind = target - currNum;
            if (visitedNumToIndex.containsKey(toFind)) {
                return new int[]{i, visitedNumToIndex.get(toFind)};
            } else {
                visitedNumToIndex.put(currNum, i); // this is kind of neat, we don't need to de-duped value
            }
        }
        throw new IllegalStateException();
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        Map<Integer, List<Integer>> numToIndexes = new HashMap();
        for (int i = 0; i < nums.length; i++) {
            if (!numToIndexes.containsKey(nums[i])) {
                numToIndexes.put(nums[i], new ArrayList<>());
            }
            numToIndexes.get(nums[i]).add(i);
        }

        for (int i = 0; i < nums.length; i++) {
            int currNum = nums[i];
            int toFindNum = target - currNum;
            if (numToIndexes.containsKey(toFindNum)) {
                res[0] = i;

                for (int j: numToIndexes.get(toFindNum)) {
                    if (i != j) {
                        res[1] = j;
                        return res;
                    }
                }
            }
        }

        throw new IllegalStateException();
    }
}
