package first.hashTable;

import java.util.HashSet;
import java.util.Set;

public class Day06_202_HappyNumber {
    public static void main(String[] args) {
        System.out.println(isHappy(2));
        System.out.println(isHappy(19));

    }

    public static boolean isHappy(int n) {
        Set<Integer> seen = new HashSet<>();
        while (true) {
            n = getDigitSquareSum(n);
            if (n == 1) {
                return true;
            } else {
                if (seen.contains(n)) {
                    return false;
                } else {
                    seen.add(n);
                }
            }
        }
    }

    private static int getDigitSquareSum(int n) {
        int sum = 0;
        while (n > 0) {
            sum += (n % 10) * (n % 10);
            n = n / 10;
        }
        return sum;
    }
}
