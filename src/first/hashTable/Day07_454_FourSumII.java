package first.hashTable;

import java.util.HashMap;
import java.util.Map;

public class Day07_454_FourSumII {
    public static void main(String[] args) {
        int[] nums1 = new int[]{1,2};
        int[] nums2 = new int[]{-2,-1};
        int[] nums3 = new int[]{-1,2};
        int[] nums4 = new int[]{0,2};
        System.out.println(bruteforce(nums1, nums2, nums3, nums4));
        System.out.println(smart(nums1, nums2, nums3, nums4));
    }

    public static int smart(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        Map<Integer, Integer> twoSumToOccurenceTimes = new HashMap<>();
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                int twoSum = nums1[i] + nums2[j];
                if (!twoSumToOccurenceTimes.containsKey(twoSum)) {
                    twoSumToOccurenceTimes.put(twoSum, 0);
                }
                twoSumToOccurenceTimes.put(twoSum, twoSumToOccurenceTimes.get(twoSum)+1);
            }
        }

        int count = 0;
        for (int k = 0; k < nums3.length; k++) {
            for (int l = 0; l < nums4.length; l++) {
                int twoSum = nums3[k] + nums4[l];
                int target = 0 - twoSum;
                if (twoSumToOccurenceTimes.containsKey(target)) {
                    count += twoSumToOccurenceTimes.get(target);
                }
            }
        }

        return count;
    }



    public static int bruteforce(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        int n = nums1.length;
        int count = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < n; k++) {
                    for (int l = 0; l < n; l++) {
                        if (nums1[i] + nums2[j] + nums3[k] + nums4[l] == 0) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }
}
