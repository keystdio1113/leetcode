package first.queueNstack;

import java.util.LinkedList;
import java.util.Queue;

public class Day10_225_ImplementStacksWithTwoQueues {
    public static void main(String[] args) {

    }

    class CarlStack {

        Queue<Integer> l;

        public CarlStack() {
            this.l = new LinkedList<>();
        }

        public void push(int x) {
            l.add(x);
        }

        public int pop() {
            int size = l.size();
            for (int i = 0; i < l.size()-1; i++) {
                l.add(l.remove());
            }
            return l.remove();
        }

        public int top() {
            int res = pop();
            this.push(res);

            return res;
        }

        public boolean empty() {
            return l.isEmpty();
        }
    }

    class MyStack {
        Queue<Integer> l1;
        Queue<Integer> l2;

        public MyStack() {
            this.l1 = new LinkedList<>();
            this.l2 = new LinkedList<>();
        }

        public void push(int x) {
            l1.add(x);
        }

        public int pop() {
            backwardsFilled1To2();
            int res = l2.remove();
            backwardsFilled2To1();
            return res;
        }

        public int top() {
            backwardsFilled1To2();
            int res = l2.peek();
            backwardsFilled2To1();
            return res;
        }

        public boolean empty() {
            return l1.isEmpty() && l2.isEmpty();
        }

        private void backwardsFilled1To2() {
            if (l1.isEmpty()) {
                return;
            } else {
                int curr = l1.remove();
                backwardsFilled1To2();
                l2.add(curr);
            }
        }

        private void backwardsFilled2To1() {
            if (l2.isEmpty()) {
                return;
            } else {
                int curr = l2.remove();
                backwardsFilled2To1();
                l1.add(curr);
            }
        }
    }
}
