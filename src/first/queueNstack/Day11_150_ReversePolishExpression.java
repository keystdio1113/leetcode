package first.queueNstack;

import java.util.Stack;

public class Day11_150_ReversePolishExpression {
    public static void main(String[] args) {
//        System.out.println(evalRPN_forward(new String[]{"2","1","+","3","*"}));
        System.out.println(evalRPN_forward(new String[]{"4","13","5","/","+"}));

    }

    public static int evalRPN_forward(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String token: tokens) {
            int res;
            if (token.equals("+")) {
                res = stack.pop() + stack.pop();
                stack.push(res);
            } else if (token.equals("-")) {
                int o2 = stack.pop();
                int o1 = stack.pop(); // this is interesting.... because the order operand matters here
                res = o1 - o2;
                stack.push(res);
            } else if (token.equals("*")) {
                res = stack.pop() * stack.pop();
                stack.push(res);
            } else if (token.equals("/")) {
                int o2 = stack.pop();
                int o1 = stack.pop();
                res = o1 / o2;
                stack.push(res);
            } else {
                stack.push(Integer.parseInt(token));
            }
        }
        return stack.pop();
    }

    public static int evalRPN_me(String[] tokens) {
        Stack<String> stack = new Stack<>();
        stack.push(tokens[tokens.length-1]);

        int result = 0;
        for (int i = tokens.length-2; i >= 0; i--) {
            String token = tokens[i];
            stack.push(token);

            while (doublePeekIsNumber(stack)) {
                int op1 = Integer.parseInt(stack.pop());
                int op2 = Integer.parseInt(stack.pop()); // this is interesting.... ordering matters for - and / sign
                String operand = stack.pop();
                int currResult;
                if (operand.equals("+")) {
                    currResult = op1 + op2;
                } else if (operand.equals("-")) {
                    currResult = op1 - op2;
                } else if (operand.equals("*")) {
                    currResult = op1 * op2;
                } else if (operand.equals("/")) {
                    currResult = op1 / op2;
                } else {
                    throw new IllegalArgumentException("Unknown operand: " + operand);
                }
                stack.push(currResult + "");
            }
        }

        return Integer.parseInt(stack.pop());
    }

    private static boolean doublePeekIsNumber(Stack<String> s) {
        if (s.size() < 2) {
            return false;
        } else {
            String s1 = s.pop();
            String s2 = s.pop();

            boolean res = isNumber(s1) && isNumber(s2);
            s.push(s2);
            s.push(s1);
            return res;
        }
    }

    private static boolean isNumber(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
