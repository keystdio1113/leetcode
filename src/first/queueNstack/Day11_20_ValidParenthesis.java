package first.queueNstack;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Day11_20_ValidParenthesis {
    public static void main(String[] args) {
        System.out.println(CarlSolution("()"));
        System.out.println(CarlSolution("()[]{}"));
        System.out.println(CarlSolution("(]"));
    }

    public static boolean CarlSolution(String s) {
        Map<Character, Character> validOpenToClose = new HashMap<>();
        validOpenToClose.put('(', ')');
        validOpenToClose.put('[', ']');
        validOpenToClose.put('{', '}');

        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char curr = s.charAt(i);
            if (validOpenToClose.containsKey(curr)) {
                stack.push(validOpenToClose.get(curr));
            } else {
                if (!stack.isEmpty() && stack.peek() == curr) {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    public static boolean solution(String s) {
        Map<Character, Character> validCloseToOpen = new HashMap<>();
        validCloseToOpen.put(')', '(');
        validCloseToOpen.put('}', '{');
        validCloseToOpen.put(']', '[');

        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char curr = s.charAt(i);
            if (validCloseToOpen.containsKey(curr)) {
                if (!stack.isEmpty() && stack.peek() == validCloseToOpen.get(curr)) {
                    stack.pop();
                } else {
                    return false;
                }
            } else {
                stack.push(curr);
            }
        }
        return stack.isEmpty();
    }
}
