package first.queueNstack;

import java.util.*;

public class Day13_347_TopKFrequentElements {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(sizeKHeap(new int[]{1, 1, 1, 2, 2, 3}, 2)));
        System.out.println(Arrays.toString(sizeKHeap(new int[]{1}, 1)));
    }

    public static int[] sizeKHeap(int[] nums, int k) {
        Map<Integer, Integer> count = new HashMap<>();
        for (int n: nums) {
            if (!count.containsKey(n)) {
                count.put(n, 0);
            }
            count.put(n, count.get(n)+1);
        }

        PriorityQueue<Pair> minHeap = new PriorityQueue<>((p1, p2) -> (p1.r - p2.r));
        for (int num: count.keySet()) {
            minHeap.add(new Pair(num, count.get(num)));
            if (minHeap.size() > k) {
                minHeap.poll();
            }
        }

        int[] res = new int[k];
        for (int i = 0; i < k; i++) {
            res[i] = minHeap.poll().l;
        }

        return res;
    }

    // this results in a O(nlogn) as the priority queue is storing every potential N elements
    public static int[] firstAttempt(int[] nums, int k) {

        Map<Integer, Integer> count = new HashMap<>();
        for (int n: nums) {
            if (!count.containsKey(n)) {
                count.put(n, 0);
            }
            count.put(n, count.get(n)+1);
        }

        PriorityQueue<Pair> maxHeap = new PriorityQueue<>((p1, p2) -> -1*(p1.r - p2.r));
        for (int num: count.keySet()) {
            maxHeap.add(new Pair(num, count.get(num)));
        }

        int[] res = new int[k];
        for (int i = 0; i < k; i++) {
            res[i] = maxHeap.poll().l;
        }

        return res;
    }


    static class Pair {
        int l;
        int r;

        public Pair(int l, int r) {
            this.l = l;
            this.r = r;
        }
    }
}
