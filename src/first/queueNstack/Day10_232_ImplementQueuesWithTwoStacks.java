package first.queueNstack;

import java.util.Stack;

public class Day10_232_ImplementQueuesWithTwoStacks {
    public static void main(String[] args) {
        CarlQueue q = new CarlQueue();
        q.push(1);
        q.push(2);
        System.out.println(q.peek());
        System.out.println(q.pop());
        System.out.println(q.empty());

    }

    static class CarlQueue {
        Stack<Integer> sIn;
        Stack<Integer> sOut;

        public CarlQueue() {
            this.sIn = new Stack<>();
            this.sOut = new Stack<>();
        }

        public void push(int x) {
            sIn.push(x);
        }

        public int pop() {
            if (sOut.isEmpty()) {
                while (!sIn.isEmpty()) {
                    sOut.push(sIn.pop());
                }
            }
            return sOut.pop();
        }

        public int peek() {
            int res = this.pop();
            sOut.push(res);

            return res;
        }

        public boolean empty() {
            return sIn.isEmpty() && sOut.isEmpty();
        }
    }

    class MyQueue {
        Stack<Integer> s1;
        Stack<Integer> s2;

        public MyQueue() {
            this.s1 = new Stack<>();
            this.s2 = new Stack<>();
        }

        public void push(int x) {
            s1.push(x);
        }

        public int pop() {
            transfer1To2();
            int res = s2.pop();
            transfer2To1();
            return res;
        }

        public int peek() {
            transfer1To2();
            int res = s2.peek();
            transfer2To1();
            return res;
        }

        public boolean empty() {
            return s1.isEmpty() && s2.isEmpty();
        }

        private void transfer1To2() {
            while (!s1.isEmpty()) {
                s2.push(s1.pop());
            }
        }

        private void transfer2To1() {
            while (!s2.isEmpty()) {
                s1.push(s2.pop());
            }
        }
    }
}
