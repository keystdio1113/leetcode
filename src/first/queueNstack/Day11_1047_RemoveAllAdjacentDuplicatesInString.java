package first.queueNstack;

import java.util.Stack;

public class Day11_1047_RemoveAllAdjacentDuplicatesInString {
    public static void main(String[] args) {
        System.out.println(mySolution("abbaca"));
        System.out.println(mySolution("azxxzy"));

    }

    public static String mySolution(String s) {
        Stack<Character> stack = new Stack<>();
        stack.push(s.charAt(0));

        for (int i = 1; i < s.length(); i++) {
            char curr = s.charAt(i);
            if (!stack.isEmpty() && curr == stack.peek()) {
                stack.pop();
            } else {
                stack.push(curr);
            }
        }

        StringBuilder sb = new StringBuilder();
        while (stack.isEmpty()) {
            sb.insert(0, stack.pop());
        }
        // LOL did not realized I could start adding things to the top
        // reverseBuildString(sb, stack);
        return sb.toString();

    }

    private static void reverseBuildString(StringBuilder sb, Stack<Character> stack) {
        if (!stack.isEmpty()) {
            char curr = stack.pop();
            reverseBuildString(sb, stack);
            sb.append(curr);
        }
    }
}
