package first.queueNstack;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class Day13_239_SlidingWindowMaximum {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(bruteForce(new int[]{1,3,-1,-3,5,3,6,7}, 3)));
        System.out.println(Arrays.toString(useAQueueToStoreDuplicateElements(new int[]{1,3,-1,-3,5,3,6,7}, 3)));
//        System.out.println(Arrays.toString(bruteForce(new int[]{1}, 1)));
    }

    public static int[] useAQueueToStoreDuplicateElements(int[] nums, int k) {
        MonotonicQueue monotonicQueue = new MonotonicQueue();
        int[] res = new int[nums.length-k+1];

        for (int i = 0; i < k; i++) {
            monotonicQueue.push(nums[i]);
        }

        res[0] = monotonicQueue.getMaxNum();
        for (int i = k; i < nums.length; i++) {
            monotonicQueue.pop(nums[i-k]);
            monotonicQueue.push(nums[i]);
            res[i-k+1] = monotonicQueue.getMaxNum();
        }

        return res;
    }

    static class MonotonicQueue {
        private Deque<Integer> deq;
        public MonotonicQueue() {
            deq = new ArrayDeque<>();
        }

        public void push(int i) {
            while (!deq.isEmpty() && i > deq.getLast()) {
                deq.removeLast();
            }
            deq.addLast(i);
        }

        public void pop(int i) {
            if (deq.size() != 0 && deq.getFirst() == i) {
                deq.pop();
            }
        }

        public int getMaxNum() {
            if (deq.size() == 0) {
                throw new IllegalStateException("No element is queue yet");
            } else {
                return deq.getFirst();
            }
        }

        public String toString() {
            return deq.toString();
        }
    }


    public static int[] bruteForce(int[] nums, int k) {
        if (nums.length < k) {
            return new int[]{};
        }

        // got an off by one error here hmmm...
        int[] res = new int[nums.length-k + 1];
        for (int i = 0; i <= nums.length-k; i++) {
            int max = Integer.MIN_VALUE;
            for (int j = i; j < (i+k); j++) {
                max = Math.max(max, nums[j]);
            }
            res[i] = max;
        }

        return res;
    }
}
