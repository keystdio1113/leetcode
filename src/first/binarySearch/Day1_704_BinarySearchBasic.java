package first.binarySearch;

public class Day1_704_BinarySearchBasic {
    public static void main(String[] args) {
        System.out.println(search(new int[]{-1,0,3,5,9,12}, 9));
        System.out.println(search(new int[]{-1,0,3,5,9,12}, 2));
    }


    public static int search(int[] nums, int target) {
//        return searchLeftClosedRightClosed(nums, target);
        return searchLeftClosedRightOpened(nums, target);
    }

    private static int searchLeftClosedRightClosed(int[] nums, int target) {
        int l = 0;
        int r = nums.length-1;

        // [0, 5]
        // l = 0, r = 5, m = 2
        //   [0, 1], 2, [3, 5]
        // l = 0, r = 1, m = 0
        //   [0, -1], 0, [1, 1]
        // l = 0, r = 6, m = 3
        //   [0, 2], 3, [4, 6]
        while (l <= r) {
            int m = l + ((r-l)/2);
            if (nums[m] == target) {
                return m;
            } else if (nums[m] < target) {
                l = m+1;
            } else {
                r = m-1;
            }
        }

        return -1;
    }

    private static int searchRecursiveLCRC(int [] nums, int target, int l, int r) {
        if (l > r) {
            return -1;
        } else {
            int m = l + ((r-l)/2);
            if (nums[m] == target) {
                return m;
            } else if (nums[m] < target) {
                return searchRecursiveLCRC(nums, target, m+1, r);
            } else {
                return searchRecursiveLCRC(nums, target, l, m-1);
            }
        }
    }

    private static int searchLeftClosedRightOpened(int[] nums, int target) {
        int l = 0;
        int r = nums.length;

        // [0, 6)
        // l = 0, r = 6, m = 3
        //   [0, 3), 3, [4, 6)
        while (l < r) {
            int m = l + ((r-l)/2);
            if (nums[m] == target) {
                return m;
            } else if (nums[m] < target) {
                l = m+1;
            } else {
                r = m;
            }
        }

        return -1;
    }

    private static int searchRecursiveLCRO(int [] nums, int target, int l, int r) {
        if (l >= r) {
            return -1;
        } else {
            int m = l + ((r-l)/2);
            // [0, 6)
            //   l = 0, r = 6, m = 3
            // [0,3), 3, [4, 6)
            if (nums[m] == target) {
                return m;
            } else if (nums[m] < target) {
                return searchRecursiveLCRO(nums, target, m+1, r);
            } else {
                return searchRecursiveLCRO(nums, target, l, m);
            }
        }
    }
}
