package first.twoPointers;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Lots of de-dup operations that has to be done. De-duping is pretty hard LOL.....
 *
 * This one is actually very interesting:
 *  1. Sorting make double pointer approach possible
 *  1. De-duping while traversing technique is amazing.
 */
public class Day07_15_ThreeSum {
    public static void main(String[] args) {
//        System.out.println(solution(new int[]{-1,0,1,2,-1,-4}));
        System.out.println(doublePointer(new int[]{-1,0,1,2,-1,-4}));
        System.out.println(doublePointer(new int[]{0,0,0,0}));
    }

    public static List<List<Integer>> doublePointer(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> results = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) return results;
            if (i >= 1 && nums[i] == nums[i-1]) continue;

            int left = i+1;
            int right = nums.length - 1;
            while (right > left) {
                int threeSum = nums[i] + nums[left] + nums[right];
                if (threeSum > 0) {
                    right--;
                } else if (threeSum < 0) {
                    left++;
                } else {
                    List<Integer> currResult = List.of(nums[i], nums[left], nums[right]);
                    results.add(currResult);
                    while (right > left && nums[left+1] == nums[left]) left++; // use right > left to guard is amazing
                    while (right > left && nums[right-1] == nums[right]) right--;

                    // these need to be kept as the above two while loops are not guaranteed to be run.
                    left++;
                    right--;
                }
            }
        }
        return results;
    }

    public static List<List<Integer>> solution(int[] nums) {
        Map<Integer, Set<Set<Integer>>> twoSumToPairIndexes = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (i == j) {
                    continue;
                }
                int twoSum = nums[i] + nums[j];
                if (!twoSumToPairIndexes.containsKey(twoSum)) {
                    twoSumToPairIndexes.put(twoSum, new HashSet<>());
                }
                Set<Integer> indexPair = new HashSet<>();
                indexPair.add(i);
                indexPair.add(j);
                twoSumToPairIndexes.get(twoSum).add(indexPair);
            }
        }

        Set<Set<Integer>> dedupedIndexTuple = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            int currNum = nums[i];
            int target = 0 - currNum;
            if (twoSumToPairIndexes.containsKey(target)) {
                Set<Set<Integer>> indexPairs = twoSumToPairIndexes.get(target);
                for (Set<Integer> indexPair: indexPairs) {
                    if (!indexPair.contains(i)) {
                        Set<Integer> triplet = new HashSet<>();
                        triplet.add(i);
                        triplet.addAll(indexPair);
                        dedupedIndexTuple.add(triplet);
                    }
                }
            }
        }

        Set<Triplet> allResults = new HashSet<>();
        for (Set<Integer> indexTuple: dedupedIndexTuple) {
            List<Integer> result = new ArrayList<>();
            for (int i: indexTuple) {
                result.add(nums[i]);
            }
            allResults.add(new Triplet(result));
        }

        return allResults.stream().map(triplet -> triplet.values).collect(Collectors.toList());
    }

    static class Triplet {
        List<Integer> values;

        public Triplet(List<Integer> values) {
            if (values.size() != 3) {
                throw new IllegalArgumentException();
            }

            this.values = values;
        }

        @Override
        public int hashCode() {
            return 2;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Triplet)) {
                return false;
            } else {
                Map<Integer, Integer> thisCount = new HashMap<>();
                for (int i: this.values) {
                    if (!thisCount.containsKey(i)) {
                        thisCount.put(i, 0);
                    }
                    thisCount.put(i, thisCount.get(i) + 1);
                }

                Map<Integer, Integer> otherCount = new HashMap<>();
                for (int i: ((Triplet) o).values) {
                    if (!otherCount.containsKey(i)) {
                        otherCount.put(i, 0);
                    }
                    otherCount.put(i, otherCount.get(i) + 1);
                }

                if (thisCount.size() != otherCount.size()) {
                    return false;
                } else {
                    for (int k: thisCount.keySet()) {
                        if (!otherCount.containsKey(k) || !thisCount.get(k).equals(otherCount.get(k))) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
    }
}
