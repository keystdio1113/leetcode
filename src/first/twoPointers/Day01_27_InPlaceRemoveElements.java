package first.twoPointers;

public class Day01_27_InPlaceRemoveElements {

    public static void main(String[] args) {
        System.out.println(removeDoublePointer(new int[]{0,1,2,2,3,0,4,2}, 2));
    }

    /**
     * Notes:
     *  1. Forgot to decrement i as we could be repeating removing elements
     *  2. Missed edge case when there is only 1 element
     *  3. Off by one in the for loops
     */
    public static int removeBruteForce(int[] nums, int val) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == val) {
                for (int j = i+1; j < nums.length; j++) {
                    nums[j-1] = nums[j];
                }
                i--;
                count++;
                nums[nums.length-1] = -1;
            }
        }
        return nums.length - count;
    }

    /**
     * Key thinkings:
     *  1. Using two pointers, we use the fast pointer to find the correct values for the slow pointer
     *  2. The fast pointer will move faster because it could potentially skip some elements we don't want, but it doesn't necessarily HAS TO BE faster
     * Missed:
     *  1. I seem to think I should remove slow + 1, but this is fence-post ish thing here and +1 is already done.
     */
    public static int removeDoublePointer(int[] nums, int val) {
        int slow = 0;

        for (int fast = 0; fast < nums.length; fast++) {
            if (nums[fast] != val) {
                nums[slow] = nums[fast];
                slow++;
            } else {
                // don't do anything but only move the fast pointer forward
            }
        }
        return slow;
    }

}
