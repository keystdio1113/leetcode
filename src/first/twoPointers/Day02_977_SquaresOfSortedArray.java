package first.twoPointers;

import java.util.Arrays;

/**
 * My solution Summary:
 *  1. Got invariant correct-ish, did not realized that i should be going from mid point to beginning
 *  2. Messed up loop termination criteria
 *  3. Stumbled a bit at the closing ceremony but realized that it could only be i ending first or j ending first
 *
 * After watching Carl's solution:
 *  1. I started with middle expanding to two sides of the first.array, but actually it is way much easier to squeeze to the middle LOL...
 *  2. Did not thought of that I could start filling the result first.array from largest absolute value to smallest
 */
public class Day02_977_SquaresOfSortedArray {
    public static void main(String[] args) {
//        System.out.println(Arrays.toString(sortedSquaresTrivial(new int[]{})));
//        System.out.println(Arrays.toString(sortedSquaresDoublePointer(new int[]{-1})));
//        System.out.println(Arrays.toString(sortedSquaresDoublePointer(new int[]{-4,-1,0,3,10})));

        System.out.println(Arrays.toString(doublePointerSqueezeFromSides(new int[]{-1})));
        System.out.println(Arrays.toString(doublePointerSqueezeFromSides(new int[]{-4,-1,0,3,10})));
    }


    /**
     * Super trivial, O(nlogn) due to the sort, O(n) might use double pointer
     */
    public static int[] sortedSquaresTrivialV2(int[] nums) {
        int[] results = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            results[i] = nums[i]*nums[i]; // overflowing concerns?
        }
        Arrays.sort(results);
        return results;
    }

    /**
         maintain two pointers i, j
         Since first.array is sorted, we maintain invariant that [0, i] is elements that are negative, [j, nums.length-1] to be non negative numbers or just be the last element of the first.array
         Initially will:
            1. Start i at 0
            2. Move j until the first non negative index or the end of the first.array if every numbers are negative, mark that as index m

         Advancing pointer strategy, Stop when i == m (Maybe off by 1 here, need to check)
            1. based on absolute value of nums[i] and nums[j]
                Abs(nums[i]) > Abs(nums[j])
                    Put nums[j] square in result, advance j
                Abs(nums[i]) = Abs(nums[j])
                    Put either square in result, advance i
                Abs(nums[i]) < Abs(nums[j])
                    Put nums[i] square in result, advance i

         If we have some remaining j at the end, finish its traversal to ensure we cover entire first.array
     */
    public static int[] sortedSquaresDoublePointer(int[] nums) {
        int i = 0;
        int j = 0;
        int[] result = new int[nums.length];
        int resultIndex = 0;
        while (j < nums.length && nums[j] < 0) {
            j++;
        }

        i = j-1;

        while (i >= 0 && j < nums.length) {
            if (Math.abs(nums[i]) > Math.abs(nums[j])) {
                result[resultIndex] = nums[j] * nums[j];
                j++;
            } else if (Math.abs(nums[i]) == Math.abs(nums[j])) {
                result[resultIndex] = nums[i] * nums[i];
                i--;
            } else {
                result[resultIndex] = nums[i] * nums[i];
                i--;
            }
            resultIndex++;
        }

        while (i >= 0) {
            result[resultIndex] = nums[i] * nums[i];
            i--;
            resultIndex++;
        }

        while (j < nums.length) {
            result[resultIndex] = nums[j] * nums[j];
            j++;
            resultIndex++;
        }

        return result;
    }


    public static int[] doublePointerSqueezeFromSides(int[] nums) {
        int i = 0;
        int j = nums.length-1;

        int[] result = new int[nums.length];
        int resultIndex = result.length-1;
        while (i <= j) {
            if (Math.abs(nums[i]) <= Math.abs(nums[j])) {
                result[resultIndex] = nums[j] * nums[j];
                j--;
            } else {
                result[resultIndex] = nums[i] * nums[i];
                i++;
            }
            resultIndex--;
        }
        return result;
    }
}
