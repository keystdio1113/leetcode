package first.twoPointers;

public class Day02_209_MinimumSizeSubArraySum {
    /**
     * Felt like Dynamic programming LOL....
     * Say we define T[i] as the minimum size of the subarray of first.array [A1, A2.... Ai]
     * T[0] = 0
     * T[i+1] = min(T[i+1]
     * But why we could use sliding window, so weird?
     * Sliding window only works because it is all positive numbers!
     *      Counter sample with negative numbers found: (minSubArrayLenSlidingWindow(7, new int[]{5,1,1,-3,2,5,1}));
     *      Cannot really articulate why this will fail, but it seems like we really need the assumption of "More elements must means bigger sum"?
     */

    public static void main(String[] args) {
        System.out.println(minSubArrayLenSlidingWindow(7, new int[]{5,1,1,-3,2,5,1}));
        System.out.println(minSubArrayLenSlidingWindow(7, new int[]{2,3,1,2,4,3}));
        System.out.println(minSubArrayLenSlidingWindow(4, new int[]{1,4,4}));
        System.out.println(minSubArrayLenSlidingWindow(11, new int[]{1,1,1,1,1,1,1,1}));
        System.out.println(minSubArrayLenSlidingWindow(11, new int[]{1,2,3,4,5}));
//        System.out.println(minSubArrayLenBruteForce(7, new int[]{2,3,1,2,4,3}));
//        System.out.println(minSubArrayLenBruteForce(4, new int[]{1,4,4}));
//        System.out.println(minSubArrayLenBruteForce(11, new int[]{1,1,1,1,1,1,1,1}));
//        System.out.println(minSubArrayLenBruteForce(11, new int[]{1,2,3,4,5}));
    }

    public static int minSubArrayLenSlidingWindow(int target, int[] nums) {
        boolean found = false;
        int i = 0;
        int sum = 0;
        int ans = Integer.MAX_VALUE;
        for (int j = 0; j < nums.length; j++) {
            sum += nums[j];
            while (sum >= target) { // this is cool...
                found = true;
                int size = j - i + 1;
                ans = Math.min(size, ans);
                sum -= nums[i];
                i++;
            }
        }
        return found ? ans : 0;
    }

    /**
     * Missed interesting edge case where a single element 1
     */
    public static int minSubArrayLenBruteForce(int target, int[] nums) {
        boolean found = false;
        int min = Integer.MAX_VALUE;

        for (int i = 0; i < nums.length; i++) {
            int sum = 0;
            for (int j = 0; j < nums.length-i; j++) {
                sum += nums[i+j];
                if (sum >= target) {
                    found = true;
                    min = Math.min(min, j+1);
                }
            }
        }

        if (found) {
            return min;
        } else {
            return 0;
        }
    }
}
