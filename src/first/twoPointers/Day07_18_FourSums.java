package first.twoPointers;

import java.util.*;

/**
 *
 *
 */
public class Day07_18_FourSums {
    public static void main(String[] args) {
        System.out.println(solution(new int[]{1,0,-1,0,-2,2}, 0));
        System.out.println(solution(new int[]{2,2,2,2,2}, 8));
        System.out.println(solution(new int[]{1000000000,1000000000,1000000000,1000000000}, -294967296)); // overflow, really LOL?
    }

    // Note this is different from 3 sum as we are getting a target and not 0 anymore.
    public static List<List<Integer>> solution(int[] nums, int target) {
        Arrays.sort(nums);
        List<List<Integer>> results = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0 && nums[i] > target) return results; // Very tricky branch cutting logic here, need to be positive numbers

            if (i >= 1 && nums[i] == nums[i-1]) continue; // deduplication
            for (int j = i+1; j < nums.length; j++) {
                if (j-1 > i && nums[j-1] == nums[j]) continue; // smart way doing deduplication
                if (nums[i]>0 && nums[j]>0 && nums[i]+nums[j]>target) return results; // Very tricky branch cutting logic here, need to be positive numbers

                int left = j+1;
                int right = nums.length-1;
                while (right > left) {
                    int fourSum = nums[i] + nums[j] + nums[left] + nums[right];
                    if (fourSum > target) {
                        right--;
                    } else if (fourSum < target) {
                        left++;
                    } else {
                        List<Integer> currRes = List.of(nums[i], nums[j], nums[left], nums[right]);
                        results.add(currRes);
                        while (right > left && nums[right] == nums[right-1]) right--;
                        while (right > left && nums[left] == nums[left+1]) left++;
                        right--;
                        left++;
                    }
                }
            }
        }
        return results;
    }
}
