package first.linkedList;

import model.ListNode;

/**
 * This is not that straightforward LOL... Spent a lot of time:
 *  1. Debating should I use double direction pointer?
 *  2. Debating dummy header. but seem too much work
 *  3. Updating size is pretty hard to keep track of since I am calling methods within methods... Did some extra size increments when calling big methods that calls small methods
 */
public class Day03_707_DesignLinkedList {
    public static void main(String[] args) {
        MyLinkedList obj = new MyLinkedList();
        obj.addAtHead(0);
        obj.addAtIndex(1, 4);
        obj.addAtTail(8);
        obj.addAtHead(5);
        obj.addAtIndex(4,3);
        obj.addAtTail(0);
        obj.addAtTail(5);
        obj.addAtIndex(6,3);
        obj.deleteAtIndex(7);
        obj.deleteAtIndex(5);
        obj.addAtTail(4);
    }

    static class MyLinkedList {
        ListNode head;
        ListNode tail;
        int size;
        public MyLinkedList() {
            this.size = 0;

        }

        public int get(int index) {
            if (index >= this.size) {
                return -1;
            } else {
                ListNode curr = head;
                for (int i = 0; i < index; i++) {
                    curr = curr.next;
                }
                return curr.val;
            }
        }

        public void addAtHead(int val) {
            if (size == 0) {
                head = new ListNode(val);
                tail = head;
            } else {
                ListNode newNode = new ListNode(val);
                newNode.next = head;
                head = newNode;
            }

            this.size++;
        }

        public void addAtTail(int val) {
            if (size == 0) {
                head = new ListNode(val);
                tail = head;
            } else {
                ListNode newNode = new ListNode(val);
                tail.next = newNode;
                tail = newNode;
            }

            this.size++;
        }

        public void addAtIndex(int index, int val) {
            if (index > size) {
                return;
            } else {
                if (index == 0) {
                    addAtHead(val);
                } else if (index == size) {
                    addAtTail(val);
                } else {
                    // here index has to be [1, size-1]
                    ListNode prev = head;
                    for (int i = 0; i < index-1; i++) {
                        prev = prev.next;
                    }
                    ListNode curr = prev.next;
                    ListNode newNode = new ListNode(val);
                    newNode.next = curr;
                    prev.next = newNode;
                    this.size++;
                }
            }
        }

        public void deleteAtIndex(int index) {
            if (index >= size) {
                return;
            } else {
                if (size == 1) {
                    head = null;
                    tail = null;
                } else {
                    // at least 2 elements here.
                    if (index == 0) {
                        head = head.next;
                    } else {
                        ListNode curr = head;
                        for (int i = 0; i < index-1; i++) {
                            curr = curr.next;
                        }
                        curr.next = curr.next.next;

                        if (index == size-1) {
                            tail = curr;
                        }
                    }
                }
            }

            this.size--;
        }

        public String toString() {
            return ListNode.toString(head);
        }
    }
}
