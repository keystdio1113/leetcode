package first.linkedList;

import model.ListNode;

import java.util.HashSet;
import java.util.Set;

public class Day04_160_intersectionOfList {
    public static void main(String[] args) {
        ListNode c = new ListNode(8, new ListNode(4, new ListNode(5)));
        ListNode a = new ListNode(4, new ListNode(1, c));
        ListNode b = new ListNode(5, new ListNode(5, new ListNode(6, new ListNode(1, c))));

        System.out.println(getIntersectionNodeOofOneSpace(a, b));
//        System.out.println(getIntersectionNodeOofNSpace(a, b));
    }

    public static ListNode getIntersectionNodeOofOneSpace(ListNode headA, ListNode headB) {
        int lenA = findLength(headA);
        int lenB = findLength(headB);

        ListNode currShort;
        ListNode currLong;
        if (lenA >= lenB) {
            currLong = headA;
            currShort = headB;
        } else {
            currLong = headB;
            currShort = headA;
        }
        int diff = Math.abs(lenA - lenB);
        for (int i = 0; i < diff; i++) {
            currLong = currLong.next;
        }
        while (currLong != null) {
            if (currLong == currShort) {
                return currLong;
            }
            currLong = currLong.next;
            currShort = currShort.next;
        }
        return null;
    }

    private static int findLength(ListNode head) {
        ListNode curr = head;
        int len = 0;
        while (curr != null) {
            len++;
            curr = curr.next;
        }
        return len;
    }

    /**
     * This is very straightforward, just use a set...
     */
    public static ListNode getIntersectionNodeOofNSpace(ListNode headA, ListNode headB) {
        Set<ListNode> visited = new HashSet<>();
        ListNode curr = headA;
        while (curr != null) {
            visited.add(curr);
            curr = curr.next;
        }

        curr = headB;
        while (curr != null) {
            if (visited.contains(curr)) {
                return curr;
            }
            curr = curr.next;
        }
        return null;
    }
}
