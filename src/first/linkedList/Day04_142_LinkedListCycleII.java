package first.linkedList;

import model.ListNode;

import java.util.HashSet;
import java.util.Set;

public class Day04_142_LinkedListCycleII {
    public static void main(String[] args) {
        ListNode head = new ListNode(3);
        ListNode n2 = new ListNode(2);
        ListNode n0 = new ListNode(0);
        ListNode n4 = new ListNode(4);
        head.next = n2; n2.next = n0; n0.next = n4; n4.next = n2;

//        System.out.println(detectCycleOOfNSpace(head).val);
        System.out.println(detectCycleOOfOneSpace(head).val);
    }

    // Forgot about first run they start together.
    // I wrote it wrong initially, one pointer 2x faster than the other only proves existance of cycle and doesn't say where.
    public static ListNode detectCycleOOfOneSpace(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode slow = head;
        ListNode fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (fast == slow) {
                // both pointers intersect here, loop
                ListNode l1 = head;
                ListNode l2 = fast;
                while (l1 != l2) {
                    l1 = l1.next;
                    l2 = l2.next;
                }
                return l1;
            }
        }
        return null;
    }

    public static ListNode detectCycleOOfNSpace(ListNode head) {
        Set<ListNode> visited = new HashSet<>();
        ListNode curr = head;
        while (curr != null) {
            if (visited.contains(curr)) {
                return curr;
            } else {
                visited.add(curr);
                curr = curr.next;
            }
        }
        return null;
    }
}
