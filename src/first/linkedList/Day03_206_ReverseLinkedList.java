package first.linkedList;

import model.ListNode;

/**
 * Pretty easy, just need to make sure that all sizes are taken care of and make sure there is something to hold
 * the remaining elements
 */
public class Day03_206_ReverseLinkedList {
    public static void main(String[] args) {
        ListNode.printList(reverseList(ListNode.createLinkedList(new int[]{1,2,3,4,5})));
        ListNode.printList(reverseListRecursive(ListNode.createLinkedList(new int[]{1,2,3,4,5}), null));
    }

    public static ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) { // 0 or 1 elements
            return head;
        } else { // at least 2 elements here
            ListNode prev = null;
            ListNode curr = head;
            ListNode next = curr.next;

            while (next != null) {
                ListNode remained = next.next;

                curr.next = prev;
                next.next = curr;

                prev = curr;
                curr = next;
                next = remained;
            }
            return curr;
        }
    }

    public static ListNode reverseListRecursive(ListNode curr, ListNode prev) {
        if (curr == null) {
            return prev;
        } else {
            ListNode remained = curr.next;
            curr.next = prev;
            return reverseListRecursive(remained, curr);
        }
    }
}
