package first.linkedList;

import model.ListNode;

/**
 * Pretty straight forward, just use dummy pointer, and carefully play with the next pointers.
 * Somehow having a while loop within a while loop is not so clean
 */
public class Day03_203_RemoveElements {
    public static void main(String[] args) {
//        ListNode.printList(removeElements(ListNode.createLinkedList(new int[]{1,2,6,3,4,5,6}), 6));
//        ListNode.printList(removeElements(ListNode.createLinkedList(new int[]{7,7,7,7,7}), 7));

        ListNode.printList(removeElementsOneLoop(ListNode.createLinkedList(new int[]{1,2,6,3,4,5,6}), 6));
        ListNode.printList(removeElementsOneLoop(ListNode.createLinkedList(new int[]{7,7,7,7,7}), 7));
    }

    public static ListNode removeElements(ListNode head, int val) {
        ListNode dummyHead = new ListNode();
        dummyHead.next = head;

        ListNode prev = dummyHead;
        ListNode curr = head;

        while (curr != null) {
            while (curr != null && val == curr.val) {
                prev.next = curr.next;
                curr = curr.next;
            }
            if (curr != null) {
                prev = curr;
                curr = curr.next;
            }
        }

        return dummyHead.next;
    }

    public static ListNode removeElementsOneLoop(ListNode head, int val) {
        ListNode dummyHead = new ListNode();
        dummyHead.next = head;

        ListNode prev = dummyHead;
        ListNode curr = head;
        while (curr != null) {
            if (curr.val == val) {
                prev.next = curr.next;
                curr = curr.next;
            } else {
                prev = prev.next;
                curr = curr.next;
            }
        }
        return dummyHead.next;
    }

    // This is harder than I thought
    // do the head first, and then remove the rest
    public static ListNode removeElementsWithoutUsingDummy(ListNode head, int val) {
        while (head != null && head.val == val) {
            head = head.next;
        }

        ListNode curr = head;
        while (curr != null && curr.next != null) {
            if (curr.next.val == val) {
                curr.next = curr.next.next;
            }
            curr = curr.next;
        }

        return head;
    }
}
