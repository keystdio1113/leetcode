package first.linkedList;

import model.ListNode;

public class Day04_24_ReversePairs {
    public static void main(String[] args) {
        ListNode.printList(swapPairs(ListNode.createLinkedList(new int[]{1,2,3,4,5,6})));
        ListNode.printList(swapPairs(ListNode.createLinkedList(new int[]{1,2,3,4,5,6,7})));
    }

    // Swapping pairs has a lot of mess when dealing with pointers. Need to know the correct ordering of pointers manuevers
    // Also need to take care of NPE when dealing with 2 pointers at once.
    public static ListNode swapPairs(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;

        if (head == null || head.next == null) {
            return head; // don't need to do anything if there are only 1 or 2 elements
        }

        // at least 2 elements here.
        ListNode prev = dummy;
        ListNode curr1 = head;
        ListNode curr2 = curr1.next;

        while (curr1 != null && curr2 != null) {
            ListNode remained = curr2.next;

            curr1.next = remained;
            curr2.next = curr1;
            prev.next = curr2;

            prev = curr1;
            curr1 = prev.next;
            if (curr1 != null) {
                curr2 = curr1.next; // Stupid NPE....
            }
        }

        return dummy.next;
    }
}
