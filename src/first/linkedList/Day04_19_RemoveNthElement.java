package first.linkedList;

import model.ListNode;

/**
 * A lot of edge case handling, I think the trick is to take care of edge conditions before preceeding
 * And at each stage, assume a certain conditions has been met.
 */
public class Day04_19_RemoveNthElement {
    public static void main(String[] args) {
        ListNode.printList(removeNthFromEndMine(ListNode.createLinkedList(new int[]{}), 2));
        ListNode.printList(removeNthFromEndMine(ListNode.createLinkedList(new int[]{1,2}), 2));
        ListNode.printList(removeNthFromEndMine(ListNode.createLinkedList(new int[]{1,2,3,4,5}), 2));
    }

    public static ListNode removeNthFromEndMine(ListNode head, int n) {
        ListNode dummyHead = new ListNode();
        dummyHead.next = head;

        ListNode slow = dummyHead;
        ListNode fast = dummyHead;
        int i = 0;
        // move fast to be N step ahead of slow
        //  {0,1,2,3,4,5}, 2
        // slow = -1
        // fast = 1
        while (fast != null && i < n) {
            fast = fast.next;
            i++;
        }
        if (fast == null) {
            //  {0}), 2
            return head; // this list is too short for us to handle.
        }

        // move slow to 1 element before the ones to be removed.
        //  {0,1,2,3}, 2
        // slow = -1,0,1
        // fast = 1,2,3
        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }
        ListNode prev = slow;
        ListNode toBeRemoved = prev.next;
        ListNode remained = toBeRemoved.next;

        prev.next = remained;

        return dummyHead.next;
    }
}
