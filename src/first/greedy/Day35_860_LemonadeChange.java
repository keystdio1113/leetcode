package first.greedy;

public class Day35_860_LemonadeChange {
//    static int fiveDollarBill = 0;
//    static int tenDollarBill = 0;
//    static int twentyDollarBill = 0;

    public static void main(String[] args) {
        System.out.println(lemonadeChange(new int[]{5,5,5,20,5,10,5,20,20,5}));
        System.out.println(lemonadeChange(new int[]{5,5,10,10,5,20,5,10,5,5}));
        System.out.println(lemonadeChange(new int[]{5,5,5,10,5,5,10,20,20,20}));
        System.out.println(lemonadeChange(new int[]{5,5,5,5,10,5,10,10,10,20}));
        System.out.println(lemonadeChange(new int[]{5,5,5,10,20}));
        System.out.println(lemonadeChange(new int[]{5,5,10,10,20}));
    }

    public static boolean lemonadeChange(int[] bills) {
        return greedy(bills);
//        return backtrack(bills, 0, 0, 0);
    }

    public static boolean greedy(int[] bills) {
        // prioritize NOT using 5s as they are the most versertile
        int five = 0;
        int ten = 0;
        for (int i = 0; i < bills.length; i++) {
            if (bills[i] == 5) {
                five++;
            } else if (bills[i] == 10) {
                if (five >= 1) {
                    five--;
                    ten++;
                } else {
                    return false;
                }
            } else {
                if (five >= 1 && ten >= 1) {
                    five--;
                    ten--;
                } else if (five >= 3) {
                    five -= 3;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean backtrack(int[] bills, int startI, int five, int ten) {
        boolean good = true;
        for (int i = startI; i < bills.length; i++) {
            if (bills[i] == 5) {
                five++;
            } else if (bills[i] == 10) {
                ten++;
                // attempt to make change for 5, if there isn't any 5 dollar left, we are done
                if (five == 0) {
                    return false;
                } else {
                    five--;
                }
            } else {  // bills[i] == 20
                // we could break 20 dollars bills in two ways:
                //   3x $5
                //   1x $5 and 1x $10
                boolean breakWith3Fives = five >= 3;
                boolean canBreak20 = false;
                if (breakWith3Fives) {
                    canBreak20 |= backtrack(bills, i+1, five-3, ten);
                }

                boolean breakWith1TenAnd1Fives = five >= 1 && ten >= 1;
                if (breakWith1TenAnd1Fives) {
                    canBreak20 |= backtrack(bills, i+1, five-1, ten-1);
                }
                if (!canBreak20) {
                    return false;
                }
            }
        }
        return good;
    }
}
