package first.greedy;

public class Day34_134_GasStation {
    public static void main(String[] args) {
//        System.out.println(canCompleteCircuit(new int[]{1,2,3,4,5}, new int[]{3,4,5,1,2}));
        System.out.println(canCompleteCircuit(new int[]{2,3,4}, new int[]{3,4,3}));
    }

    public static int canCompleteCircuit(int[] gas, int[] cost) {
        return greedy(gas, cost);
//        return bruteForce(gas, cost);
    }


    public static int greedy(int[] gas, int[] cost) {
        // we will keep track of a cummulative sum of the net gain with an important observation. For this sample:
        // gas = [1, 2, 3, 4, 5]
        // cost= [3, 4, 5, 1, 2]
        // net = [-2, -2, -2, 3, 3]
        // cumSum = [-2, -4, -6, -3, 0]
        // If the cummulative sum is less than zero for an index, we know that index, and any index before it, cannot be the starting point.
        // Prove by contradiction maybe? this is weird... can't do it lol
        int curSum = 0;
        int totalSum = 0;
        int start = 0;
        for (int i = 0; i < gas.length; i++) {
            int net = gas[i] - cost[i];
            curSum += net;
            totalSum += net;
            if (curSum < 0) {
                start = i+1;
                curSum = 0;
            }
        }
        if (totalSum < 0) {
            return -1;
        } else {
            return start;
        }
    }

    // so I must stop at every station?
    public static int bruteForce(int[] gas, int[] cost) {
        int result = -1;
        for (int i = 0; i < gas.length; i++) {
            // we can not start from this point as there is not enough gas to put us to next station
            if (gas[i] < cost[i]) continue;

            int tryI = (i+1) % cost.length;;
            int remaining = gas[i] - cost[i];
            while (remaining > 0 && tryI != i) {
                remaining += (gas[tryI] - cost[tryI]);
                tryI = (tryI+1) % cost.length;
            }

            if (remaining >= 0 && tryI == i) {
                result = i;
                break;
            }
        }

        return result;
    }


    // DP seems impossible to do because we also need to keep track of the remaining gas at each stop.
    public static int dp(int[] gas, int[] cost) {
        for (int i = 0; i < gas.length; i++) {
            // we can not start from this point as there is not enough gas to put us to next station
            if (gas[i] < cost[i]) continue;

            // flattens the circular for better consumption:
            //  [1,2,3,4,5], start from index 3, this will become -> [4,5,1,2,3,4] with an extra 4 paddeed
            int[] gasF = new int[gas.length+1];
            int[] costF = new int[gas.length+1];
            for (int c = 0; c < gas.length; c++) {
                int index = (c+i) % gas.length;
                gasF[c] = gas[index];
                costF[c] = cost[index];
            }

            gasF[gasF.length-1] = gas[i];
            costF[gasF.length-1] = cost[i];

            // define the following:
            //  first.dp[i] = true if we could reach the index i
            //  initially:
            //    first.dp[0] = true, all others are false
            //  recurrently:
            //    first.dp[i] =
            boolean[] dp = new boolean[gasF.length];
            dp[0] = true;
        }

        return 0;
    }
}
