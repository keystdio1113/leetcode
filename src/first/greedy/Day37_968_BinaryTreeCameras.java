package first.greedy;

import model.TreeNode;

public class Day37_968_BinaryTreeCameras {
    private static int result = 0;

    public static void main(String[] args) {
        TreeNode root = new TreeNode(
                0,
                new TreeNode(
                        0,
                        new TreeNode(0), new TreeNode(0)),
                null);

        root = new TreeNode(0);
        System.out.println(minCameraCover(root));
    }

    public static int minCameraCover(TreeNode root) {
        return greedy(root);
    }

    // The first.greedy approach assumes that we always want to start putting cameras from the leaf node
    // since a camera covers two layers of nodes, starting from leaf node, we want to prioritize put a camera on its parents
    // node if leaf node is not covered.
    // We are doing a bottom up approach from leaf node, so we are doing post order tranversal here.
    //   0: this node is not covered
    //   1: this node has a camera
    //   2: this node is covered

    public static int greedy(TreeNode root) {
        result = 0;

        // dummy root also works here
        TreeNode dummyRoot = new TreeNode(0, root, null);
        traversal(dummyRoot);
        return result;
        // if not, we need to take care of special case here on the root case.
//        int rootStatus = traversal(root);
//        if (rootStatus == 0) {
//            return result + 1;
//        } else {
//            return result;
//        }
    }

    private static int traversal(TreeNode root) {
        if (root == null) return 2;
        int left = traversal(root.left);
        int right = traversal(root.right);

        if (left == 0 || right == 0) {
            // either children is not covered, parents must need a camera
            result++;
            return 1;
        }

        if (left == 1 || right == 1) {
            return 2; // one of the child is camera node, I am covered
        }

        if (left == 2 && right == 2) {
            return 0; // I am not covered as none of my children has a camera (from above branch)
        }

        throw new IllegalStateException("This should not happened");
    }



    // This is actually a vertex cover problem. (NOT QUITE... but closed)
    // We could traverse down each node with recursion, and perform two branches of decisions.
    // Each root:
    //   1. the root itself is part of the minimum vertex cover, this case, the child are free
    //   2. the root is not part of the minimum VC, then the child has to be taken
    // This is actually a vertex cover problem.
    // We could traverse down each node with recursion, and perform two branches of decisions.
    // Each root:
    //   1. the root itself is part of the minimum vertex cover, this case, the child are free
    //   2. the root is not part of the minimum VC, then the child has to be taken
    public static int solveAsVertexCover(TreeNode root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null) return 1;
        int includeRoot = recursion(root, true);
        int skipRoot = recursion(root, false);
        return Math.min(includeRoot, skipRoot);
    }

    private static int recursion(TreeNode root, boolean included) {
        if (root == null) {
            return 0;
        } else {
            if (included) {
                return 1 + recursion(root.left, false) + recursion(root.right, false);
            } else {
                return recursion(root.left, true) + recursion(root.right, true);
            }
        }
    }
}
