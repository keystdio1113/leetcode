package first.greedy;

public class Day32_55_JumpGame {
    public static void main(String[] args) {
        System.out.println(canJump(new int[]{1,2,3}));
        System.out.println(canJump(new int[]{2,3,1,1,4}));
        System.out.println(canJump(new int[]{3,2,1,0,4}));
    }
    public static boolean canJump(int[] nums) {
//        return first.dp(nums);
        return greedyByExtendingRange(nums);
    }

    public static boolean greedyByExtendingRange(int[] nums) {
        // this first.greedy approach is done by extending the range of how far you can jump.
        // [2,3,1,1,4], for this one:
        //   1. we got 2, so we know we could cover index 0-2, first range
        //   2. within first range, we loop through the elements within and attempt to extend the right boundary of the coverage
        //   3. We see 3 can take us to index 4, which is the end.

        if (nums.length == 1) return true;

        int l = 0;
        int r = l+nums[l];

        while (l <= r && r < nums.length-1) {
            int maxRangeFromL = l + nums[l];
            r = Math.max(r, maxRangeFromL);
            l++;
        }
        return r >= nums.length-1;
    }

    public static boolean dp(int[] nums) {
        // Define a boolean first.array first.dp
        // first.dp[i] stands for I could reach position i
        // trivially we have:
        //    first.dp[0] = true
        // Recurrently we have:
        //    first.dp[i] = first.dp[j] && nums[j] >= i-j  for 0 <= j <= i-1
        // Then final results will be the last element
        //    first.dp[first.dp.length-1]
        boolean[] dp = new boolean[nums.length];
        dp[0] = true;
        for (int i = 1; i < dp.length; i++) {
            for (int j = 0; j <= i-1; j++) {
                int distance = i-j;
                dp[i] = dp[i] || (dp[j] && nums[j] >= distance);
            }
        }
        return dp[dp.length-1];
    }
}
