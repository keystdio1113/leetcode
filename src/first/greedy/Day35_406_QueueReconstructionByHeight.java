package first.greedy;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Day35_406_QueueReconstructionByHeight {
    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(reconstructQueue(new int[][]{
                new int[]{7, 0},
                new int[]{4, 4},
                new int[]{7, 1},
                new int[]{5, 0},
                new int[]{6, 1},
                new int[]{5, 2}
        })));
    }
    public static int[][] reconstructQueue(int[][] people) {
        return greedy(people);
    }

    public static int[][] greedy(int[][] people) {
        // lets first sort by height descending order.
        //  [[7,0],[4,4],[7,1],[5,0],[6,1],[5,2]] becomes:
        //  [[7,0],[7,1],[6,1],[5,0],[5,2],[4,4]]
        //  We will loop through the sorted first.array, and if we see a person that has some requirements that k people must be
        //  not shorter than them, we will promote them forward so that they are k people in front of them.
        //  Note that we could do this because once we sorted the first.array, we can be sure that people in the front are always taller
        //  so there is no harm in bringing people to the front.
        //
        //  Hence in the above shorted first.array.
        //    1. we keep [7,0] in place since he has no requirements in terms of k
        //    2. we keep [7,1] in place since there is already k=1 people that are not shorter than him in front.
        //    3. we need to move [6,1] after the [7,0] to satisfy k=1, then first.array becomes
        //         [[7,0],[6,1],[7,1],[5,0],[5,2],[4,4]]
        //    4. we need to move [5,0] to the foremost of the list as there can be no one taller than him.
        //         [[5,0],[7,0],[6,1],[7,1],[5,2],[4,4]]
        //    5. we need to move [5,2] to after [7,0]
        //         [[5,0],[7,0],[5,2],[6,1],[7,1],[4,4]]
        //    6. we need to move [4,4] to after [6,1]
        //         [[5,0],[7,0],[5,2],[6,1],[4,4],[7,1]]

        Arrays.sort(people, (a, b) -> {
            if (a[0] == b[0]) return a[1] - b[1];
            else return b[0] - a[0];
        });

        List<int[]> resultQ = new LinkedList<>();
        for (int i = 0; i < people.length; i++) {
            int position = people[i][1];
            resultQ.add(position, people[i]);
        }

        return resultQ.toArray(new int[people.length][]);
        }
}
