package first.greedy;

import java.util.Arrays;

public class Day35_452_MinimumNumberOfArrowsBurstBallons {
    public static void main(String[] args) {
        System.out.println(findMinArrowShots(new int[][]{
                new int[]{9,12},
                new int[]{1,10},
                new int[]{4,11},
                new int[]{8,12},
                new int[]{3,9},
                new int[]{6,9},
                new int[]{6,7},
        }));

        System.out.println(findMinArrowShots(new int[][]{
                new int[]{3,9},
                new int[]{7,12},
                new int[]{3,8},
                new int[]{6,8},
                new int[]{9,10},
                new int[]{2,9},
                new int[]{0,9},
                new int[]{3,9},
                new int[]{0,6},
                new int[]{2,8}
        }));

        System.out.println(findMinArrowShots(new int[][]{
                new int[]{-2147483646,-2147483645},
                new int[]{2147483646,2147483647}
        }));

        System.out.println(findMinArrowShots(new int[][]{
                new int[]{10,16},
                new int[]{2,8},
                new int[]{1,6},
                new int[]{7,12}
        }));
    }

    public static int findMinArrowShots(int[][] points) {
        return greedy(points);
    }

    public static int greedy(int[][] points) {
        Arrays.sort(points, (a, b) -> Integer.compare(a[0], b[0]));
        int stacksCount = 0;
        int i = 0;
        while (i < points.length){
            int j = i+1;
            int stackLeft = points[i][0]; // this is actually not necessary.. it is guarenteed to be true since first.array is sorted, but doesnt hurt?
            int stackRight = points[i][1];
            while (j < points.length) {
                if (points[j][0] >= stackLeft && points[j][0] <= stackRight) {
                    stackLeft = Math.max(stackLeft, points[j][0]);
                    stackRight = Math.min(stackRight, points[j][1]);
                    j++;
                } else {
                    break;
                }
            }
            stacksCount++;
            i = j;
        }
        // careful about the last one? Should be taken care of.
        return stacksCount;
    }
}
