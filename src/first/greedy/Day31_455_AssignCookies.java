package first.greedy;

import java.util.Arrays;

public class Day31_455_AssignCookies {
    public static void main(String[] args) {
        System.out.println(findContentChildren(new int[]{1, 2, 3}, new int[]{1, 1}));
        System.out.println(findContentChildren(new int[]{1, 2}, new int[]{1, 2, 3}));
    }

    public static int findContentChildren(int[] g, int[] s) {
        return find(g, s);
    }

    public static int find(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);

        int childI = 0;
        int fedChildCount = 0;
        for (int i = 0; i < s.length && childI < g.length; i++) {
            int currCookieSize = s[i];
            if (s[i] >= g[childI]) {
                childI++;
                fedChildCount++;
            } else {
                // do nothing here, as we need bigger cookie to feed this kid.
            }
        }
        return fedChildCount;
    }
}
