package first.greedy;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day34_1005_MaxSumAfterKNegations {
    public static void main(String[] args) {
//        System.out.println(largestSumAfterKNegations(new int[]{-8,3,-5,-3,-5,-2}, 6));
        System.out.println(largestSumAfterKNegations(new int[]{4,2,3}, 1));
    }

    public static int largestSumAfterKNegations(int[] nums, int k) {
        return betterGreedy(nums, k);
    }

    public static int betterGreedy(int[] nums, int k) {
        // 0. Sort the first.array by the ABSOLUTE value by descending order
        // 1. Negate k negative numbers
        // 2. If there are remaining k:
        //    a. remaining k is even -> do nothing
        //    b. remaining k is odd -> negate the element with the smallest absolute numbers
        // 3. calculate sum

        List<Integer> sorted = Arrays.stream(nums)
                .boxed()
                .sorted((a, b) -> Math.abs(b) - Math.abs(a))
                .collect(Collectors.toList());
        for (int i = 0; i < sorted.size(); i++) {
            if (k > 0 && sorted.get(i) < 0) {
                k--;
                sorted.set(i, -1*sorted.get(i));
            }
        }

        if (k > 0 && k % 2 == 1) {
            int lastIndex = sorted.size()-1;
            sorted.set(lastIndex, -sorted.get(lastIndex));
        }

        return sorted.stream().mapToInt(Integer::intValue).sum();
    }

    public static int initialGreedy(int[] nums, int k) {
        // Negate all the k-smallest negative numbers
        //   1. k is not large enough for every negative number -> give up, we tried our best
        //   2. there is some remaining k.
        //       if there is a 0, we can waste the remaining negations in it -> return as is
        //       if there is no 0,
        //          if remaining k is even, we can return the current sum -> we could negate back and forth for nothing
        //          if remaining k is odd, we will have to either
        //               negate the smallest positive (abs value of smallest positive smaller than largest negative)
        //               double negate the largest negative (abs value of smallest positive smaller than largest negative)
        Arrays.sort(nums);
        int remainingK = k;
        int sum = 0;
        boolean containsZero = false;
        boolean containsNegative = false;
        boolean containsPositive = false;

        int smallestPositive = -1;
        int largestNegative = Integer.MIN_VALUE;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) containsZero = true;
            if (nums[i] > 0) containsPositive = true;
            if (nums[i] < 0) containsNegative = true;

            if (nums[i] > 0 && smallestPositive == -1) smallestPositive = nums[i];
            if (nums[i] < 0 && nums[i] > largestNegative) largestNegative = nums[i];

            if (nums[i] < 0 && remainingK > 0) {
                nums[i] = -nums[i];
                remainingK--;
            }

            sum += nums[i];
        }

        if (remainingK == 0) {
            return sum;
        } else {
            if (containsZero) {
                return sum;
            } else {
                if (remainingK % 2 == 0) {
                    return sum;
                }
            }
        }

        // these are the weird cases where we have some remaining K value to deal with
        // So we need to negate 1 more value:
        //   if the original first.array is all positive, we negate the smallest positive (Effectively subtract 2*SP)
        //   if there are some negatives and positives, we need to find the largest negative (LN) and smallest positive (SP)
        //      if abs(LN) < abs(SP), we negate the LN once more (Effectively subtract 2*abs(LN)
        //      else, we negate SP, (Effectively subtract 2*SP)
        if (containsPositive && !containsNegative) {
            return sum - 2*smallestPositive;
        } else if (containsPositive && containsNegative) {
            if (Math.abs(largestNegative) < Math.abs(smallestPositive)) {
                return sum - 2*Math.abs(largestNegative);
            } else {
                return sum - 2*smallestPositive;
            }
        } else if (!containsPositive && containsNegative) {
            return sum - 2*Math.abs(largestNegative);
        } else {
            return 0;
        }
    }
}
