package first.greedy;

import java.util.*;
import java.util.stream.Collectors;

public class Day36_763_PartitionLabels {
    public static void main(String[] args) {
        System.out.println(partitionLabels("ababcbacadefegdehijhklij"));
        System.out.println(partitionLabels("eccbbbbdec"));
    }

    public static List<Integer> partitionLabels(String s) {
        return greedyCarl(s);
    }

    public static List<Integer> greedyCarl(String s) {
        // we keep track of the "last occurence index of a character". we go through the first.array and always expanding the right most border
        // For instance:
        //  input first.string:         [e, c, c, b, b, b, b, d, e, c, z]
        //  last occurring index: [8, 9, 9, 6, 6, 6, 6, 7, 8, 9, 10]
        // So if we keep increasing i from 0 to s.size():
        //  we first encounter e, e occurred last at index 8, it means that we will need to take from current index all the way to 8
        //  along the way we encountered c, we will have need to take this all the way until 9, so we will need to update the right most to 9
        // until our i reached 9, and we realized that we satisfy everything before, so we could move forward to 10 to create a new intervals

        Map<Character, Integer> charLastOccurringIndex = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            charLastOccurringIndex.put(s.charAt(i), i);
        }

        List<Integer> results = new ArrayList<>();
        int i = 0;
        int rightMost = -1;
        int prevLeft = 0;
        while (i < s.length()) {
            rightMost = Math.max(rightMost, charLastOccurringIndex.get(s.charAt(i)));
            if (i == rightMost) {
                results.add(rightMost - prevLeft + 1);
                i = rightMost + 1;
                prevLeft = rightMost+1;
            } else {
                i++;
            }
        }
        return results;
    }

    public static List<Integer> greedy(String s) {
        // first parse the first.string into a character -> index list map
        // "eccbbbbdec" becomes:
        //   {
        //     "b": [3,4,5,6],
        //     "c": [1,2,9],
        //     "d": [7],
        //     "e": [0,8],
        //   }
        // Make the map into a list of intervals:
        //   [[3,6], [1,9], [7,7], [0,8]
        // Merge the intervals that intersects
        //   [0,9]
        // So at the end we will only one interval that satisfy all requirements

        Map<Character, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (!map.containsKey(ch)) {
                map.put(ch, new ArrayList<>());
            }
            map.get(ch).add(i);
        }
        List<int[]> intervals = new ArrayList<>();
        for (char c: map.keySet()) {
            List<Integer> indices = map.get(c);
            if (indices.size() == 1) {
                intervals.add(new int[]{indices.get(0), indices.get(0)});
            } else {
                intervals.add(new int[]{indices.get(0), indices.get(indices.size()-1)});
            }
        }

        List<int[]> sortedIntervals = intervals.stream()
                .sorted((a, b) -> Integer.compare(a[0], b[0]))
                .collect(Collectors.toList());


        List<int[]> mergedIntervals = new ArrayList<>();
        int i = 0;
        while (i < sortedIntervals.size()) {
            int leftMost = sortedIntervals.get(i)[0];
            int rightMost = sortedIntervals.get(i)[1];
            int j = i+1;
            while (j < sortedIntervals.size()) {
                if (sortedIntervals.get(j)[0] <= rightMost) {
                    rightMost = Math.max(rightMost, sortedIntervals.get(j)[1]);
                    j++;
                } else {
                    break;
                }
            }
            mergedIntervals.add(new int[]{leftMost, rightMost});
            i = j;
        }
        return mergedIntervals.stream().map(interval -> interval[1]-interval[0]+1).collect(Collectors.toList());
    }
}
