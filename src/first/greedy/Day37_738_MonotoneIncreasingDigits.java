package first.greedy;

import java.util.Arrays;

public class Day37_738_MonotoneIncreasingDigits {
    public static void main(String[] args) {
        System.out.println(monotoneIncreasingDigits(100));
        System.out.println(monotoneIncreasingDigits(10));
        System.out.println(monotoneIncreasingDigits(1234));
        System.out.println(monotoneIncreasingDigits(332));
    }

    public static int monotoneIncreasingDigits(int n) {
        return greedy(n);
    }

    public static int greedy(int n) {
        String nStr = n+"";
        int[] str = new int[nStr.length()];
        for (int i = 0; i < nStr.length(); i++) {
            str[i] = Integer.parseInt(String.valueOf(nStr.charAt(i)));
        }


        int flagUpToThisPointFromRightMostEverythingNeedsToBe9 = str.length;
        for (int i = str.length-1; i >= 1; i--) {
            int currDigit = str[i];
            int previousDigit = str[i-1];
            if (currDigit < previousDigit) {
                flagUpToThisPointFromRightMostEverythingNeedsToBe9--;
                str[i-1]--;
            }
        }

        for (int i = str.length-1; i >= flagUpToThisPointFromRightMostEverythingNeedsToBe9; i--) {
            str[i] = 9;
        }

        String joined = "";
        for (int i: str) {
            joined += i;
        }

        return Integer.parseInt(joined);
    }

    public static int bruteforce(int n) {
        for (int i = n; i >= 0; i--) {
            boolean isValid = true;
            int num = i;
            int prev = num % 10;
            num = num / 10;
            while (num > 0) {
                int next = num % 10;
                if (prev < next) {
                    isValid = false;
                    break;
                }
                num = num / 10;
                prev = next;
            }
            if (isValid) {
                return i;
            }
        }

        throw new IllegalStateException("Should have something above");
    }
}
