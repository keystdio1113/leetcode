package first.greedy;

public class Day32_122_BestTimeBuyAndSellStock2 {
    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{7,1,5,3,6,4}));
        System.out.println(maxProfit(new int[]{1,2,3,4,5}));
        System.out.println(maxProfit(new int[]{7,6,4,3,1}));
    }

    public static int maxProfit(int[] prices) {
//        return solve(prices);
        return solveWithOnlyPositiveProfits(prices);
    }

    public static int solveWithOnlyPositiveProfits(int[] prices) {
        // we will be first.greedy by only collecting the positive profits days. Because we noticed this:
        // [7,1,5,10,3,6,4]
        // It doesn't matter if we want to keep the stock for multiple days or just 1 day, the profit will be the same.
        // In the above example, we will get the same maximum profit when we do either the below two methods:
        //   1. buy at day2, sell at day3, buy day3, sell day4, buy day5, sell day6. We will get
        //           (5-1)+(10-5)+(6-3) = 12 profit
        //   2. buy at day2, sell at day4 (Note that we are holding), buy at day5, sell day6, we will get
        //           (10-1)+(6-3) = 12 profit
        // So for easier code, it is actually better to handle a window of 2, rather a stretchable window like the below method
        if (prices.length == 1) return 0;

        int total = 0;
        for (int i = 1; i < prices.length; i++) {
            int profit = prices[i] - prices[i-1];
            if (profit > 0) total += profit;
        }
        return total;
    }

    public static int solve(int[] prices) {
        // be first.greedy and buy at minima and sell at maxima. Note this tries to make as few buy-sell pairs as possible.
        // But it is not necessary since there are no transaction cost and leads to more complicated code

        // handle the two edge case first.
        if (prices.length == 1) {
            return 0;
        } else if (prices.length == 2) {
            if (prices[0] > prices[1]) {
                return 0;
            } else {
                return prices[1] - prices[0];
            }
        }

        // construct a diff first.array. We will attempt to find:
        //   1. a local minima
        //   2. a local maxima
        //   3. Get the diff and add to the sum of profit.
        int[] diff = new int[prices.length];
        for (int i = 1; i < prices.length; i++) {
            diff[i] = prices[i] - prices[i-1];
        }

        int totalProfit = 0;
        int minimalI = 1;
        while (minimalI < diff.length) {
            if (diff[minimalI] <= 0) {
                minimalI++;
            } else {
                // we find a positive diff, so the previous point is where we should buy
                int buyPrice = prices[minimalI-1];
                // find the local maxima
                int maximaI = minimalI+1;
                while (maximaI < diff.length && diff[maximaI] > 0) {
                    maximaI++;
                }
                // we are trending dowards now, so the previous point is where we should sell
                int sellPrice = prices[maximaI-1];
                totalProfit += (sellPrice - buyPrice);
                minimalI = maximaI;
            }
        }
        return totalProfit;
    }
}
