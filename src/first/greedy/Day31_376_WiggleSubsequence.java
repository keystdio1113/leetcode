package first.greedy;

public class Day31_376_WiggleSubsequence {
    public static void main(String[] args) {
        System.out.println(wiggleMaxLength(new int[]{0, 0}));
        System.out.println(wiggleMaxLength(new int[]{1,17,5,10,13,15,10,5,16,8}));
    }

    public static int wiggleMaxLength(int[] nums) {
        return greedy(nums);
    }

    public static int dp(int[] nums) {
        // Using 0-index here.
        // Defining a first.dp table: first.dp[i], first.dp[i] means the length of the longest wiggle subsequence found in n[0..i] that is ending at index i
        // So we have:
        //   first.dp[0] = 0 // we might need this...
        //   first.dp[1] = 1
        // Recurringly we have:
        //   first.dp[i] = max {
        //               first.dp[i-1] -> we do not take n[i] as the next subsequence
        //               first.dp[j]+1 for every 0 <= j <= i-1 if n[i]-n[j] is alternating with the previous bump
        //           }
        //   do this for 0 <= i < len(n)
        // We need a backtrack table as well to determine what is the previous element index for the optimal subsequence.
        int[] dp = new int[nums.length];
        int[] backtrack = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            int backtrackIndex = i-1; // assume this unless being modified.
            int max;
            if (i == 0) {
                max = 1;
            } else {
                max = dp[i-1]; // we don't take num[i] and just use its previous value
            }

            for (int j = 0; j <= i-1; j++) {
                int diff = nums[i] - nums[j];
                if (diff == 0) continue;
                if (j == 0) {
                    if (1+dp[j] > max) {
                        max = 1+dp[j];
                        backtrackIndex = j;
                    }
                } else {
                    int jPreviousDiff = nums[j] - nums[backtrack[j]];
                    if (diff * jPreviousDiff < 0) {
                        if (1+dp[j] > max) {
                            max = 1+dp[j];
                            backtrackIndex = j;
                        }
                    }
                }
            }

            dp[i] = max;
            backtrack[i] = backtrackIndex;
        }
        return dp[nums.length-1];
    }

    public static int dpWithUpAndDown(int[] nums) {
        // we define up[i] as "The longest length of wiggle subsequence and n[i]-n[i-1] > 0
        // we define down[i] as "The longest length of wiggle subsequence and n[i]-n[i-1] < 0
        int[] up = new int[nums.length];
        int[] down = new int[nums.length];

        up[0] = 1;
        down[0] = 1;

        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                int diff = nums[i] - nums[j];
                if (diff == 0) {
                    up[i] = up[i-1]; // we can just keep "potentially" overwriting the i-th index with max, and at the end, it is an accumulative max.
                    down[i] = down[i-1];
                } else if (diff > 0){
                    up[i] = Math.max(up[i], 1+down[j]);
                } else {
                    down[i] = Math.max(down[i], 1+up[j]);
                }
            }
        }
        return Math.max(up[nums.length-1], down[nums.length-1]);
    }

    public static int dpWithUpAndDownWithoutLookingBack(int[] nums) {
        // we define up[i] as "The longest length of wiggle subsequence and n[i]-n[i-1] > 0
        // we define down[i] as "The longest length of wiggle subsequence and n[i]-n[i-1] < 0
        int[] up = new int[nums.length];
        int[] down = new int[nums.length];

        up[0] = 1;
        down[0] = 1;
        for (int i = 1; i < nums.length; i++) {
            int diff = nums[i] - nums[i-1];
            if (diff > 0) {
                up[i] = down[i-1] + 1;
                down[i] = down[i-1];
            } else if (diff < 0) {
                down[i] = up[i-1] + 1;
                up[i] = up[i-1];
            } else {
                up[i] = up[i-1];
                down[i] = down[i-1];
            }
        }
        return Math.max(up[nums.length-1], down[nums.length-1]);
    }

    public static int greedy(int[] nums) {
        if (nums.length == 1) return 1;
        if (nums.length == 2) {
            if (nums[0] != nums[1]) return 2;
            else return 1;
        }

        int prevDiff = nums[1] - nums[0];
        int longestLength = prevDiff == 0 ? 1 : 2;
        for (int i = 2; i < nums.length; i++) {
            int currDiff = nums[i] - nums[i-1];
            if (
                    (prevDiff <= 0 && currDiff > 0) ||
                    (prevDiff >= 0 && currDiff < 0)
            ) {
                longestLength++;
                prevDiff = currDiff;
            }
        }
        return longestLength;
    }
}
