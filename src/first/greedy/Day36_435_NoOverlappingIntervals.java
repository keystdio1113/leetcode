package first.greedy;

import java.util.Arrays;

public class Day36_435_NoOverlappingIntervals {
    public static void main(String[] args) {
        System.out.println(eraseOverlapIntervals(new int[][]{
                new int[]{1,2},
                new int[]{2,3},
                new int[]{3,4},
                new int[]{1,3}
        }));
    }

    public static int eraseOverlapIntervals(int[][] intervals) {
        return greedyFromLeft(intervals);
//        return greedyFromRight(intervals);
    }

    // We are still counting how many intervals stack there is.
    // But the key is to get return int.length - stackCount; Brilliant!
    public static int greedyFromRight(int[][] intervals) {
        // lets do something fun here, sort by the right edges and we will do backwards.
        Arrays.sort(intervals, (a, b) -> Integer.compare(a[1], b[1]));
        int count = 0;
        int i = intervals.length-1;
        while (i >= 0) {
            int left = intervals[i][0];
            int j = i-1;
            while (j >= 0) {
                if (intervals[j][1] > left) {
                    left = Math.max(left, intervals[j][0]);
                    j--;
                } else {
                    break;
                }
            }
            count++;
            i = j;
        }
        return intervals.length - count;
    }

    public static int greedyFromLeft(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> Integer.compare(a[0], b[0]));
        int i = 0;
        int count = 0;
        while (i < intervals.length) {
            int right = intervals[i][1];
            int j = i + 1;
            while (j < intervals.length) {
                if (intervals[j][0] < right) {
                    right = Math.min(intervals[j][1], right);
                    j++;
                } else {
                    break;
                }
            }
            i = j;
            count++;
        }
        return intervals.length - count;
    }
}
