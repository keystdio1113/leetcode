package first.greedy;

public class Day31_53_MaximumSubArray {
    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
    }

    public static int maxSubArray(int[] nums) {
        return greedy(nums);
    }

    public static int dp(int[] nums) {
        // define first.dp[i] as the maximum subarray sum using the elements from nums[0..i] ending at nums[i]
        // first.dp[0] = nums[0]
        // recurring steps for first.dp[i] = max(num[i], first.dp[i-1]+num[i]), the two cases
        //    1. if we decide to start a new subarray with only num[i]
        //    2. if we decide to attempt to attach num[i] to the previous subarray
        // return the maximum value inside first.dp as we are not bound to use every input element
        int[] dp = new int[nums.length];
        dp[0] = nums[0];

        int max = dp[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.max(nums[i], dp[i-1]+nums[i]);
            max = Math.max(max, dp[i]);
        }

        return max;
    }

    public static int greedy(int[] nums) {
        // first.greedy approach. We keep track of a maximum "localConsequentSum", we will move start a new localConsequentSum
        // every time it became negative.
        int max = nums[0];

        int localConsequentSum = 0;
        for (int i = 0; i < nums.length; i++) {
            localConsequentSum += nums[i];
            max = Math.max(localConsequentSum, max);
            if (localConsequentSum < 0) {
                localConsequentSum = 0;
            }
        }
        return max;
    }
}
