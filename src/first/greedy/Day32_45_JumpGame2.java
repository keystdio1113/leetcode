package first.greedy;

public class Day32_45_JumpGame2 {
    public static void main(String[] args) {
        System.out.println(jump(new int[]{2,1}));
        System.out.println(jump(new int[]{1,2,3}));
        System.out.println(jump(new int[]{2,3,1,1,4}));
        System.out.println(jump(new int[]{2,3,0,1,4}));
    }

    public static int jump(int[] nums) {
//        return first.dp(nums);
        return greedyExtendingRightBoundary(nums);
    }

    public static int greedyExtendingRightBoundary(int[] nums) {
        // We could do this because we know that it is guarentee a solution.
        if (nums.length == 1) return 0;
        if (nums.length == 2) return 1;

        int nextFurthestAway = -1;
        int curFurthestAway = 0;
        int step = 0;
        for (int l = 0; l < nums.length; l++) {
            nextFurthestAway = Math.max(nextFurthestAway, l + nums[l]);
            if (l == curFurthestAway) {
                step++;
                curFurthestAway = nextFurthestAway;
                if (nextFurthestAway >= nums.length-1) break;
            }
        }
        return step;
    }


    public static int dp(int[] nums) {
        // Define 1-d first.array first.dp as follows
        // first.dp[i] stands for the minimum jump to arrive on index i
        // Initially,
        //   first.dp[0] = 0, everything else is plus infinity.
        // Recurrently,
        //    first.dp[i] = minimum of (first.dp[j]+1) for all j within [0,i-1] that is within jumping distance of i
        // results will be in first.dp[n-1]
        int[] dp = new int[nums.length];
        for (int i = 0; i < dp.length; i++) {
            dp[i] = Integer.MAX_VALUE;
        }
        dp[0] = 0;
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j <= i-1; j++) {
                int distance = i - j;
                if (nums[j] >= distance) {
                    dp[i] = Math.min(dp[i], dp[j]+1);
                }
            }
        }
        return dp[nums.length-1];
    }
}
