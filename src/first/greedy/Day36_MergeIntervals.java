package first.greedy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day36_MergeIntervals {
    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(merge(new int[][]{
                new int[]{2,3},
                new int[]{4,5},
                new int[]{6,7},
                new int[]{8,9},
                new int[]{1,10}
        })));

        System.out.println(Arrays.deepToString(merge(new int[][]{
                new int[]{1,3},
                new int[]{2,6},
                new int[]{8,10},
                new int[]{15,18},
        })));
    }

    public static int[][] merge(int[][] intervals) {
        return sortAndMerge(intervals);
    }

    public static int[][] sortAndMerge(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> Integer.compare(a[0], b[0]));
        List<int[]> results = new ArrayList<>();

        int i = 0;
        while (i < intervals.length) {
            int j = i+1;
            int currentRightMost = intervals[i][1];
            while (j < intervals.length) {
                if (intervals[j][0] <= currentRightMost) {
                    currentRightMost = Math.max(currentRightMost, intervals[j][1]);
                    j++;
                } else {
                    break;
                }
            }
            results.add(new int[]{intervals[i][0], currentRightMost});
            i = j;
        }

        int[][] res = new int[results.size()][2];
        for (int k = 0; k < results.size(); k++) {
            res[k] = results.get(k);
        }

        return res;
    }
}
