package first.array;

import first.utils.PrintUtils;

/**
 * Got it almost correct but there are some edge cases I got wrong.
 * Key thing is getting invariant correct and every loop, rules need to be the same.
 * Carl actually don't need to care about directions, I got tricked by direction %4 and that makes my solution harder,
 * it is not needed at all LOL.
 */
public class Day2_59_SpiralMatrix2 {
    public static void main(String[] args) {
//        PrintUtils.print2DMatrixLike(generateMatrix(3));
//        PrintUtils.print2DMatrixLike(generateMatrix(4));

        PrintUtils.print2DMatrixLike(generateMatrixCarl(3));
        PrintUtils.print2DMatrixLike(generateMatrixCarl(4));

    }

    public static int[][] generateMatrixCarl(int n) {
        int[][] result = new int[n][n];

        int currLoop = 0;

        int count = 1;

        while (currLoop < n/2) {
            int row = currLoop;
            int col = currLoop;
            int size = n-1 - 2*currLoop;

            for (int i = 0; i < size; i++) { // R
                result[row][col] = count;
                col++;
                count++;
            }

            for (int i = 0; i < size; i++) { // D
                result[row][col] = count;
                row++;
                count++;
            }

            for (int i = 0; i < size; i++) { // L
                result[row][col] = count;
                col--;
                count++;
            }

            for (int i = 0; i < size; i++) { // U
                result[row][col] = count;
                row--;
                count++;
            }

            currLoop++;
        }

        if (n % 2 == 1) {
            result[n/2][n/2] = n*n;
        }

        return result;
    }

    /**
     * Did this MSFT interview before and I falied miserably
     *
     * We will create a loop that stops at 1 -> n^2 so that we don't need to handle odd/even size
     * We will maintain:
     *  1. A step size indicating how far we should go in one direction, decrement by 2 everytime we finished a revolution
     *  2. A flag that goes up everytime we force a turn, use MOD to indicate which direction we should go
     *  3. Current coordinates (x,y) to be filled, update the values after filling in value according to directions
     *
     * 01, 02, 03, 04
     * 12, 13, 14, 05
     * 11, 16, 15, 06
     * 10, 09, 08, 07
     *
     * start with size=3
     *  fill 3 elements (1-3) going R, turn,
     *  fill 3 elements (4-6) going D, turn,
     *  fill 3 elements (7-9) going L, turn,
     *  fill 3 elements (10-12) going U, turn, decrement size by 2,
     * size=1
     *  fill 1 elements (13) going R, turn
     *  fill 1 elements (14) going D, turn
     *  fill 1 elements (15) going L, turn
     *  fill 1 elements (16) going U, turn
     *
     * 01, 02, 03
     * 08, 09, 04
     * 07, 06, 05
     *
     * d = turn % 4, which direction
     * x = turn / 4, starting left top corner
     * s = n-1 - 2x
     *
     * d = 0, x = 1, [(0, 0), (0, 1)]
     * d = 1, x = 1, [(0, 2), (1, 2)]
     * d = 2, x = 1, [(2, 2), (2, 1)]
     * d = 3, x = 1, [(2, 0), (1, 0)]
     * d = 4, x = 2, [(1, 1)]
     */
    public static int[][] generateMatrix(int n) {
        int[][] result = new int[n][n];
        int i = 1;
        int turn = 0;

        int row = 0;
        int col = 0;

        // Assumption made: Really think the math will work out whether n is even or odd....
        //   Fixed by carl:
        //      1. This should be controlled by how many loops we are allowed to do.
        //      2. Also need to fix the beginning of row and col
        //      3. Middle point need to be hand drawn.
        //      4. Every turning point, the row and col needs to be reset
        int currLoop = Integer.MIN_VALUE; // prime the loop
        while (currLoop < n/2) {
            int d = turn % 4;
            currLoop = turn / 4;
            int s = (n-1) - 2*currLoop;
            if (d == 0) { // R
                row = currLoop;
                col = currLoop;
                for (int j = 0; j < s; j++) {
                    result[row][col] = i;
                    i++;
                    col++;
                }
            } else if (d == 1) { // D
                for (int j = 0; j < s; j++) {
                    result[row][col] = i;
                    i++;
                    row++;
                }
            } else if (d == 2) { // L
                for (int j = 0; j < s; j++) {
                    result[row][col] = i;
                    i++;
                    col--;
                }
            } else { // U
                for (int j = 0; j < s; j++) {
                    result[row][col] = i;
                    i++;
                    row--;
                }
            }
            turn++;
        }

        if (n % 2 != 0) {
            result[n/2][n/2] = n*n;
        }

        return result;
    }
}
