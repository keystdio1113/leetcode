package first.monotonicStack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Day61_496_NextGreaterElement {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(nextGreaterElement(new int[]{4, 1, 2}, new int[]{1, 3, 4, 2})));
    }

    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        return monotonicStack(nums1, nums2);
    }

    public static int[] monotonicStack(int[] nums1, int[] nums2) {
        int[] res = new int[nums1.length];
        Map<Integer, Integer> numToIndexInNums1 = new HashMap<>();
        for (int i = 0; i < nums1.length; i++) {
            res[i] = -1; // default value
            numToIndexInNums1.put(nums1[i], i);
        }

        // this monotonic stack will contain elements that are the smallest in the top of stack, monotonically increase as going down the stack
        Stack<Integer> mono = new Stack<>();
        mono.push(nums2[0]);
        for (int i = 1; i < nums2.length; i++) {
            if (nums2[i] <= mono.peek()) {
                mono.push(nums2[i]);
            } else {
                while (!mono.isEmpty() && nums2[i] > mono.peek()) {
                    int num = mono.pop();
                    if (numToIndexInNums1.containsKey(num)) {
                        res[numToIndexInNums1.get(num)] = nums2[i];
                    }
                }
                mono.push(nums2[i]);
            }
        }

        return res;
    }

    public static int[] bruteforce(int[] nums1, int[] nums2) {
        if (nums1.length == 0) return new int[]{};
        int[] res = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            int j = 0;
            while (j < nums2.length) {
                if (nums2[j] == nums1[i]) {
                    break;
                }
                j++;
            }

            boolean found = false;
            while (j < nums2.length) {
                if (nums2[j] > nums1[i]) {
                    found = true;
                    break;
                }
                j++;
            }

            if (found) {
                res[i] = nums2[j];
            } else {
                res[i] = -1;
            }
        }
        return res;
    }
}
