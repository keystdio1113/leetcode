package first.monotonicStack;

import java.util.Arrays;
import java.util.Stack;

public class Day61_739_DailyTemperature {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(dailyTemperatures(new int[]{73, 74, 75, 71, 69, 72, 76, 73})));
    }

    public static int[] dailyTemperatures(int[] temperatures) {
//        return doublePointer(temperatures);
        return monotonicStackSolution(temperatures);
    }

    public static int[] doublePointer(int[] temperatures) {
        int[] res = new int[temperatures.length];
        for (int i = 0; i < temperatures.length; i++) {

            int j = i+1;
            int daysToWait = 1;
            boolean foundWarmerDay = false;
            while (j < temperatures.length) {
                if (temperatures[j] > temperatures[i]) {
                    foundWarmerDay = true;
                    break;
                } else {
                    j++;
                    daysToWait++;
                }
            }
            if (foundWarmerDay) res[i] = daysToWait;
        }

        return res;
    }

    // we will maintain a invariant here:
    //   1. the stack will always be monotonic incrementing from top of stack to bottom of stack
    //   2. index in the stack are the ones we did not find an value that is larger than it yet as we are traversing
    public static int[] monotonicStackSolution(int[] temperatures) {
        // TODO: length of 1?
        int[] res = new int[temperatures.length];
        Stack<Integer> mono = new Stack<>();
        mono.push(0);
        for (int i = 1; i < temperatures.length; i++) {
            int currT = temperatures[i];

            if (currT <= temperatures[mono.peek()]) {
                mono.push(i);
            } else {
                while (!mono.isEmpty() && currT > temperatures[mono.peek()]) {
                    int currTopI = mono.pop();
                    res[currTopI] = i - currTopI;
                }
                mono.push(i);
            }
        }

        return res;
    }
}
