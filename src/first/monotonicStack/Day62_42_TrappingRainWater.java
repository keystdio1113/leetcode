package first.monotonicStack;

public class Day62_42_TrappingRainWater {
    public static void main(String[] args) {
        System.out.println(trap(new int[]{0,1,0,2,1,0,1,3,2,1,2,1}));
        System.out.println(trap(new int[]{4,2,0,3,2,5}));
    }

    public static int trap(int[] height) {
        return doublePointerEnhanced(height);
    }

    // for each i, the amount of rain water it can store is based on the left shortest bar that is taller than it and the right shortest bar that is taller than it.
    public static int doublePointer(int[] height) {
        int water = 0;
        // the first and last i doesn't hold water
        for (int i = 1; i < height.length-1; i++) {
            int lBar = height[i];
            int rBar = height[i];

            for (int l = 0; l < i; l++) {
                if (height[l] > lBar) lBar = height[l];
            }

            for (int r = i+1; r < height.length; r++) {
                if (height[r] > rBar) rBar = height[r];
            }

            water += (Math.min(lBar, rBar) - height[i]);
        }
        return water;
    }

    // let's preemptively calculate the every element's tallest bar on the left and tallest bar on the right.
    // Note that we could actually calculate it pretty easily
    public static int doublePointerEnhanced(int[] height) {
        int[] tallestLeft = new int[height.length];
        int[] tallestRight = new int[height.length];

        tallestLeft[0] = height[0];
        for (int i = 1; i < tallestLeft.length; i++) {
            tallestLeft[i] = Math.max(tallestLeft[i-1], height[i]);
        }

        tallestRight[tallestRight.length-1] = height[tallestRight.length-1];
        for (int i = tallestRight.length-2; i >= 0; i--) {
            tallestRight[i] = Math.max(tallestRight[i+1], height[i]);
        }

        int amount = 0;
        for (int i = 0; i < height.length; i++) {
            amount += (Math.min(tallestLeft[i], tallestRight[i]) - height[i]);
        }

        return amount;
    }
}
