package first.monotonicStack;

import java.util.Stack;

public class Day63_84_LargestRectangleInHistogram {
    public static int largestRectangleArea(int[] heights) {
        return bruteforce(heights);
    }

    public static int bruteforce(int[] heights) {
        int max = -1;
        for (int i = 0; i < heights.length; i++) {
            // we will find the first bar that is smaller than the current on its left side and its right side.
            int leftSmallerBarIndex = -1;
            int rightSmallerBarIndex = -1;

            for (int j = i; j >= 0; j--) {
                if (heights[j] < heights[i]) {
                    leftSmallerBarIndex = j;
                }
            }

            for (int j = i; j < heights.length; j++) {
                if (heights[j] < heights[i]) {
                    rightSmallerBarIndex = j;
                }
            }

            max = Math.max(max, heights[i] * rightSmallerBarIndex-leftSmallerBarIndex);
        }

        return max;
    }


    // we will maintain a monotonic stack where:
    // elements in the stack will be monotonic decreasing from top of stack to bottom of stack.
    // So we will be storing a progressively step up of the historgram in the stack until we saw something smaller,
    // then we empty the stack to calculate the area
//    public static int first.monotonicStack(int[] heights) {
//        Stack<Integer> mono = new Stack<>();
//
//        mono.push(heights[0]);
//        int max = heights[0];
//
//        for (int i = 1; i < heights.length; i++) {
//            max = Math.max(max, heights[i]);
//
//            if (heights[i] >= mono.peek()) {
//                mono.push(heights[i]);
//            } else {
//                max = Math.max(max, calculateMax(mono));
//                mono.push(heights[i]);
//            }
//        }
//        int remainingMax = calculateMax(mono);
//        max = Math.max(max, remainingMax);
//        return max;
//    }

//    private static int calculateMax(Stack<Integer> mono) {
//        int max = 0;
//
//        List<Integer>
//        while (!mono.isEmpty()) {
//            int curr = mono.pop();
//            count++;
//            max = Math.max(max, count*curr);
//        }
//
//        return max;
//    }
}
