package first.monotonicStack;

import java.util.Arrays;
import java.util.Stack;

public class Day62_503_NextGreaterElements2 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(nextGreaterElements(new int[]{1, 2, 1})));
    }

    public static int[] nextGreaterElements(int[] nums) {
        return monotonicStack(nums);

    }

    public static int[] monotonicStack(int[] nums) {
        int originalSize = nums.length;

        int[] res = new int[originalSize];
        for (int i = 0; i < res.length; i++) {
            res[i] = -1;
        }

        // stack will contain elements's index who we have not yet found a value that is larger than it yet.
        // It will be monotonic increasing from top of stack to bottom of stack (Top contains smallest element)
        Stack<Integer> mono = new Stack<>();
        mono.push(0);

        // For an first.array [0,1,2] -> since it is a loop, we will loop through 1,2,3,1(4%3),2(5%3), so 2*len-1 = 5 times
        for (int i = 1; i < 2*originalSize-1; i++) {
            int adjustedI = i % nums.length;
            if (nums[adjustedI] <= nums[mono.peek()]) {
                mono.push(adjustedI);
            } else {
                while (!mono.isEmpty() && nums[adjustedI] > nums[mono.peek()]) {
                    int indexWhoseValueIsSmallerThanCurr = mono.pop();
                    res[indexWhoseValueIsSmallerThanCurr] = nums[adjustedI];
                }
                mono.push(adjustedI);
            }
        }

        return res;
    }
}
