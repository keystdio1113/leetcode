package second.array;

public class _209_minimumSizeSubarraySum {
    public static void main(String[] args) {
        System.out.println(secondRun_slidingWindow(7, new int[]{2,3,1,2,4,3}));
        System.out.println(secondRun_slidingWindow(4, new int[]{1,4,4}));
        System.out.println(secondRun_slidingWindow(11, new int[]{1,1,1,1,1,1,1,1}));
    }

    public static int secondRun_slidingWindow(int target, int[] nums) {
        int i = 0;
        int min = Integer.MAX_VALUE;
        int sum = 0;
        boolean found = false;
        for (int j = 0; j < nums.length; j++) {
            sum += nums[j];
            while (sum >= target) {
                int ans = j-i+1;
                found = true;
                sum -= nums[i];
                i++;
                min = Math.min(ans, min);
            }
        }

        if (found) {
            return min;
        } else {
            return 0;
        }
    }

    public static int slightlyBetterBruteForce(int target, int[] nums) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < nums.length; i++) {
            int sum = 0;
            for (int j = i; j<nums.length; j++) {
                sum += nums[j];
                if (sum >= target) {
                    min = Math.min(min, j-i+1);
                }
            }
        }

        if (min == Integer.MAX_VALUE) {
            return 0;
        } else {
            return min;
        }
    }

    public static int bruteForce(int target, int[] nums) {
        boolean found = false;
        int shortest = Integer.MAX_VALUE;
        for (int length = 1; length <= nums.length; length++) {
            for (int i = 0; i+length <= nums.length; i++) {
                int runningSum = 0;
                for (int j = 0; j < length; j++) {
                    runningSum += nums[i+j];
                }
                if (runningSum >= target) {
                    return length;
                }
            }
        }

        return 0;
    }
}
