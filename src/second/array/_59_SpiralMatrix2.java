package second.array;

import first.utils.PrintUtils;

public class _59_SpiralMatrix2 {
    public static void main(String[] args) {
        PrintUtils.print2DMatrixLike(generateMatrix(3));
        PrintUtils.print2DMatrixLike(generateMatrix(1));
        PrintUtils.print2DMatrixLike(generateMatrix(4));
    }

    public static int[][] generateMatrix(int n) {
        int[][] res = new int[n][n];

        int curr = 1;
        int i = 0;
        /**
         * (0,0) -> (0,1)
         * (0,2) -> (1,2)
         * (2,2) -> (2,1)
         * (2,0) -> (1,0)
         *
         * (1,1)
         */
        while (i <= n/2) {
            int currRow = i;
            int currCol = i;

            int forwardMove = n - 2*i - 1;
            if (forwardMove <= 0) {
                // do nothing
            } else {
                for (int j = 0; j < forwardMove; j++) {
                    res[currRow][currCol] = curr;
                    currCol++;
                    curr++;
                }

                for (int j = 0; j < forwardMove; j++) {
                    res[currRow][currCol] = curr;
                    currRow++;
                    curr++;
                }

                for (int j = 0; j < forwardMove; j++) {
                    res[currRow][currCol] = curr;
                    currCol--;
                    curr++;
                }

                for (int j = 0; j < forwardMove; j++) {
                    res[currRow][currCol] = curr;
                    currRow--;
                    curr++;
                }
            }

            i++;
        }

        if (n % 2 == 1) {
            res[n/2][n/2] = curr;
        }

        return res;
    }
}
