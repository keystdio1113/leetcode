package second.array;

import java.util.Arrays;

public class _27_removeElement {
    public static void main(String[] args) {
        int[] nums = new int[]{3, 2, 2, 3};
        System.out.println(twoPointer(nums, 3));
        System.out.println(Arrays.toString(nums));


        nums = new int[]{0,1,2,2,3,0,4,2};
        System.out.println(twoPointer(nums, 2));
        System.out.println(Arrays.toString(nums));

        nums = new int[]{1};
        System.out.println(twoPointer(nums, 1));
        System.out.println(Arrays.toString(nums));
    }

    public static int twoPointer(int[] nums, int val) {
        if (nums.length == 0) {
            return 0;
        } else if (nums.length == 1) {
            if (nums[0] == val) {
                return 0;
            } else {
                return 1;
            }
        }
        int k = 0;
        int i = 0; // traversal pointer
        int rightMost = 0; // we will maintain nums[0, rightMost] be a valid array without @val

        while (i < nums.length) {
            if (nums[i] == val) {
                // don't need to anything
            } else {
                nums[rightMost] = nums[i];
                k++;
                rightMost++;
            }
            i++;
        }
        return k;
    }


    public static int removeElement(int[] nums, int val) {
        if (nums.length == 0) {
            return 0;
        } else if (nums.length == 1) {
            if (nums[0] == val) {
                return 0;
            } else {
                return 1;
            }
        }

        // at least 2 elements here
        // regular traversal pointer that goes to the end of the array
        int i = 0; // we will keep an invariant such that nums[0, i] is the array without any @val elements

        while (i < nums.length) {
            if (nums[i] != val) {
                // do nothing
            } else {
                for (int j = i+1; j < nums.length; j++) {
                    if (nums[j] != val) {
                        int temp = nums[i];
                        nums[i] = nums[j];
                        nums[j] = temp;
                        break;
                    }
                }
            }
            i++;
        }

        int count = 0;
        for (int num: nums) {
            if (num != val) {
                count ++;
            } else {
                break;
            }
        }

        return count;
    }
}
