package second.linkedList;

import model.ListNode;

public class _707_DesignLinkedList {
    public static void main(String[] args) {
        MyLinkedList l = new MyLinkedList();
        l.addAtHead(1);
        l.addAtTail(3);
        l.addAtIndex(1, 2);
        l.get(1);
        l.deleteAtIndex(1);
        l.get(1);
    }

    static class MyLinkedList {
        ListNode dummyHead;

        public MyLinkedList() {
            this.dummyHead = new ListNode(-1);

        }

        public int get(int index) {
            ListNode curr = dummyHead.next;
            for (int i = 0; i < index && curr != null; i++) {
                curr = curr.next;
            }
            if (curr == null) {
                return -1;
            } else {
                return curr.val;
            }

        }

        public void addAtHead(int val) {
            ListNode remain = dummyHead.next;
            ListNode newNode = new ListNode(val);
            newNode.next = remain;
            dummyHead.next = newNode;
        }

        public void addAtTail(int val) {
            ListNode curr = dummyHead;
            while (curr.next != null) {
                curr = curr.next;
            }
            curr.next = new ListNode(val);
        }

        public void addAtIndex(int index, int val) {
            ListNode curr = dummyHead;
            for (int i = 0; i < index && curr != null; i++) {
                curr = curr.next;
            }
            if (curr == null) {
                return;
            } else {
                ListNode remain = curr.next;
                ListNode newNode = new ListNode(val);
                newNode.next = remain;
                curr.next = newNode;
            }
        }

        public void deleteAtIndex(int index) {
            ListNode curr = dummyHead;
            for (int i = 0; i < index; i++) {
                curr = curr.next;
            }
            if (curr == null || curr.next == null) {
                return;
            } else {
                ListNode remain = curr.next.next;
                curr.next = remain;
            }
        }
    }
}
