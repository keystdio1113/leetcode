package second.linkedList;

import model.ListNode;

import java.util.HashSet;
import java.util.Set;

public class _160_IntersectionOfTwoLinkedLists {
    public static void main(String[] args) {
        ListNode headA = ListNode.createLinkedList(new int[]{4,1,8,4,5});
        ListNode headB = ListNode.createLinkedList(new int[]{5,6,1,8,4,5});
        ListNode.printList(secondRunGetIntersectionNodeO1Space(headA, headB));
    }


    public static ListNode secondRunGetIntersectionNodeO1Space(ListNode headA, ListNode headB) {
        int sizeA = 0;
        int sizeB = 0;

        ListNode currA = headA;
        ListNode currB = headB;

        while (currA.next != null) {
            sizeA++;
            currA = currA.next;
        }

        while (currB.next != null) {
            sizeB++;
            currB = currB.next;
        }

        if (currA != currB) {
            return null; // no intersection
        }

        int diffSize = Math.abs(sizeA - sizeB);
        ListNode fast = null;
        ListNode slow = null;
        if (sizeA >= sizeB) {
            fast = headA;
            slow = headB;
        } else {
            fast = headB;
            slow = headA;
        }
        for (int i = 0; i < diffSize; i++) {
            fast = fast.next;
        }

        while (fast != null && slow != null) {
            if (fast == slow) {
                return fast;
            } else {
                fast = fast.next;
                slow = slow.next;
            }
        }

        throw new IllegalStateException("This should not happened");
    }

    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        Set<ListNode> visited = new HashSet<>();
        ListNode curr = headA;
        while (curr != null) {
            visited.add(curr);
            curr = curr.next;
        }

        curr = headB;
        while (curr != null) {
            if (visited.contains(curr)) {
                return curr;
            }
            curr = curr.next;
        }
        return null;
    }
}
