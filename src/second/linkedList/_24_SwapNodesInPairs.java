package second.linkedList;

import model.ListNode;

public class _24_SwapNodesInPairs {
    public static void main(String[] args) {

    }

    public static ListNode swapPairs(ListNode head) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode prev = dummy;

        ListNode a = prev.next;
        ListNode b = a != null ? a.next : null;
        ListNode remaining = b != null ? b.next : null;

        while (a != null && b != null) {
            prev.next = b;
            b.next = a;
            a.next = remaining;

            prev = a;
            a = prev.next;
            b = a != null ? a.next : null;
            remaining = b != null ? b.next : null;
        }

        return dummy.next;
    }
}
