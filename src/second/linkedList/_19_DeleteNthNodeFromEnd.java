package second.linkedList;

import model.ListNode;

import java.util.Stack;

public class _19_DeleteNthNodeFromEnd {
    public static void main(String[] args) {

    }

    public ListNode twoPointer(ListNode head, int n) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;

        ListNode slow = dummy;
        ListNode fast = dummy;
        for (int i = 0; i <= n; i++) {
            fast = fast.next;
        }
        while (fast != null) {
            slow = slow.next;
            fast = fast.next;
        }
        slow.next = slow.next.next;
        return dummy.next;
    }

    public ListNode removeNthFromEnd2Scans(ListNode head, int n) {
        int size = 0;
        ListNode curr = head;
        while (curr != null) {
            size++;
            curr = curr.next;
        }

        int indexToRemove = size - n;
        ListNode dummy = new ListNode(-1);
        dummy.next = head;

        ListNode prev = dummy;
        for (int i = 0; i < indexToRemove; i++) {
            prev = prev.next;
        }
        ListNode toRemove = prev.next;
        ListNode remaining = toRemove.next;
        prev.next = remaining;

        return dummy.next;
    }

    public ListNode removeNthFromEnd1Scan(ListNode head, int n) {
        Stack<ListNode> stack = new Stack<>();
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode curr = dummy;
        while (curr != null) {
            stack.push(curr);
            curr = curr.next;
        }

        ListNode toRemove = null;
        for (int i = 0; i < n; i++) {
            toRemove = stack.pop();
        }
        ListNode prev = stack.pop();
        ListNode remaining = toRemove.next;
        prev.next = remaining;

        return dummy.next;
    }
}
