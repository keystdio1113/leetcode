package second.linkedList;

import model.ListNode;

public class _206_reverseLinklist {
    public static void main(String[] args) {

    }

    private static ListNode reverseList(ListNode head) {
        if (head == null) {
            return null;
        } else {
            ListNode prev = null;
            ListNode curr = head;

            while (curr != null) {
                ListNode next = curr.next;
                curr.next = prev;
                prev = curr;
                curr = next;
            }

            return prev;
        }
    }

    private static ListNode reverseRecursion(ListNode prev, ListNode curr) {
        if (curr == null) {
            return prev;
        } else {
            ListNode next = curr.next;
            curr.next = prev;
            return reverseRecursion(curr, next);
        }
    }
}
