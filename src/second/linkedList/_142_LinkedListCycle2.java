package second.linkedList;

import model.ListNode;

public class _142_LinkedListCycle2 {
    public static void main(String[] args) {

    }

    public static ListNode detectCycle(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;

        while (slow != null && fast != null) {
            slow = slow.next;
            if (fast.next != null) {
                fast = fast.next.next;
            } else {
                return null;
            }

            if (slow == fast) {
                ListNode curr1 = slow;
                ListNode curr2 = head;
                while (curr1 != curr2) {
                    curr1 = curr1.next;
                    curr2 = curr2.next;
                }
                return curr1;
            }
        }
        return null;
    }
}
