package second.binarySearch;

public class _704_Binary_Search {
    public static void main(String[] args) {
        System.out.println(search(new int[]{-1,0,3,5,9,12}, 9));
        System.out.println(search(new int[]{-1,0,3,5,9,12}, -2));
    }

    public static int search(int[] nums, int target) {
        return searchLeftClosedRightOpened(nums, target);
        //return searchLeftClosedRightClosed(nums, target);
    }

    private static int searchLeftClosedRightClosed(int[] nums, int target) {
        int low = 0;
        int high = nums.length - 1;

        while (low <= high) {
            int mid = low + ((high-low) / 2);
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                high = mid-1;
            } else {
                low = mid+1;
            }
        }

        return -1;
    }

    private static int searchLeftClosedRightOpened(int[] nums, int target) {
        int low = 0;
        int high = nums.length;

        while (low < high) {
            int mid = low + ((high-low)/2); // prevent overflow
            if (nums[mid] == target) {
                return mid;
            } else if(nums[mid] > target) {
                high = mid;
            } else {
                low = mid+1;
            }
        }

        return -1;
    }

}
