package second.twoPointers;

import java.util.Arrays;

public class _977_SquaresOfSortedArray {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(squeezeFromSides((new int[]{-4,-1,0,3,10}))));
        System.out.println(Arrays.toString(squeezeFromSides((new int[]{-7,-3,2,3,11}))));
        System.out.println(Arrays.toString(squeezeFromSides((new int[]{-7,-3,2,3}))));
        System.out.println(Arrays.toString(squeezeFromSides((new int[]{2,3,4}))));
        System.out.println(Arrays.toString(squeezeFromSides((new int[]{2,3}))));
        System.out.println(Arrays.toString(squeezeFromSides((new int[]{2}))));
    }

    public static int[] sortedSquares(int[] nums) {
        int[] res = new int[nums.length];
        if (nums.length == 1) {
            res[0] = nums[0] * nums[0];
            return res;
        }

        // we want [0, i] to be all the non-positives, [j, .length-1] to be all the positives
        int i = 0; int j = 0;
        while (i < nums.length) {
            if (nums[i] <= 0) {
                i++;
            } else {
                break;
            }
        }
        i--;
        j = i+1;

        int curr = 0;

        while (i >= 0 && j < nums.length) {
            int left = nums[i]*nums[i];
            int right = nums[j]*nums[j];
            if (left <= right) {
                res[curr] = left;
                i--;
            } else {
                res[curr] = right;
                j++;
            }
            curr++;
        }


        // finish the edges of both sides
        while (j < nums.length) {
            res[curr] = nums[j] * nums[j];
            j++;
            curr++;
        }

        while (i >= 0) {
            res[curr] = nums[i] * nums[i];
            i--;
            curr++;
        }
        return res;
    }

    public static int[] squeezeFromSides(int[] nums) {
        int[] res = new int[nums.length];

        int i = 0;
        int j = nums.length-1;
        int curr = nums.length-1;
        while (i <= j) {
            if (Math.abs(nums[i]) >= Math.abs(nums[j])) {
                res[curr] = nums[i] * nums[i];
                i++;
            } else {
                res[curr] = nums[j] * nums[j];
                j--;
            }
            curr--;
        }
        return res;
    }

}
