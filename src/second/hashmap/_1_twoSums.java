package second.hashmap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class _1_twoSums {
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    res[0] = i;
                    res[1] = j;
                }
            }
        }

        return res;
    }

    public int[] twoSumHashSetSecondRun(int[] nums, int target) {
        HashMap<Integer, Integer> toFindToIndexMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int currNum = nums[i];
            int toFind = target - currNum;
            if (toFindToIndexMap.containsKey(toFind)) {
                return new int[]{i, toFindToIndexMap.get(toFind)};
            } else {
                toFindToIndexMap.put(currNum, i);
            }
        }

        throw new IllegalStateException();
    }
}
