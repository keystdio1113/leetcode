package second.hashmap;

import java.util.HashMap;
import java.util.Map;

public class _242_validAnagram {


    public static void main(String[] args) {

    }

    public static boolean isAnagram(String s, String t) {
        Map<String, Integer> charCount1 = new HashMap<>();
        Map<String, Integer> charCount2 = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            String currChat = s.substring(i, i+1);
            if (charCount1.containsKey(currChat)) {
                charCount1.put(currChat, 0);
            }
            charCount1.put(currChat, charCount1.get(currChat)+1);
        }

        for (int i = 0; i < t.length(); i++) {
            String currChat = t.substring(i, i+1);
            if (charCount2.containsKey(currChat)) {
                charCount2.put(currChat, 0);
            }
            charCount2.put(currChat, charCount2.get(currChat)+1);
        }

        if (charCount1.size() != charCount2.size()) {
            return false;
        }

        for (String key1: charCount1.keySet()) {
            if (!charCount2.containsKey(key1)) {
                return false;
            }
            if (charCount2.get(key1).intValue() != charCount1.get(key1).intValue()) {
                return false;
            }
        }

        return true;
    }
}
