package second.hashmap;

public class _383_randomNote {
    public boolean canConstruct(String ransomNote, String magazine) {
        int [] supply = new int[26];
        for (int i = 0; i < magazine.length(); i++) {
            int currChar = magazine.charAt(i);
            supply[currChar-'a'] += 1;
        }

        for (int i = 0; i < ransomNote.length(); i++) {
            int currChar = ransomNote.charAt(i);
            supply[currChar-'a'] -= 1;
        }

        for (int i = 0; i < supply.length; i++) {
            if (supply[i] < 0) {
                return false;
            }
        }

        return true;

    }
}
