package second.hashmap;

import java.util.HashSet;
import java.util.Set;

public class _202_HappyNumber {

    public static void main(String[] args) {
        System.out.println(isHappy(7));
    }
    public static boolean isHappy(int n) {
        Set<Integer> seenNumber = new HashSet<>();


        while (true) {
            String nStr = n + "";
            int sum = 0;
            for (int i = 0; i < nStr.length(); i++) {
                int currDigit = Integer.parseInt(nStr.charAt(i) + "");
                sum += currDigit * currDigit;
            }
            if (sum == 1) {
                return true;
            }

            if (seenNumber.contains(sum)) {
                return false;
            } else {
                seenNumber.add(sum);
            }
            n = sum;
        }
    }
}
