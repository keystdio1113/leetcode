package second.hashmap;

import java.util.HashSet;
import java.util.Set;

public class _349_IntersectionsOfTwoArrays {
    public static int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> res = new HashSet<>();
        Set<Integer> set1 = new HashSet<>();

        for (int n: nums1) {
            set1.add(n);
        }

        for (int n2: nums2) {
            if (set1.contains(n2)) {
                res.add(n2);
            }
        }

        int[] result = new int[res.size()];
        int i = 0;
        for (int nR: res) {
            result[i] = nR;
            i++;
        }

        return result;

    }

}
