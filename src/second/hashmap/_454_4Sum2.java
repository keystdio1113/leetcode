package second.hashmap;

import java.util.HashMap;
import java.util.Map;

public class _454_4Sum2 {
    public int fourSumCount(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        int count = 0;
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                for (int k = 0; k < nums3.length; k++) {
                    for (int l = 0; l < nums4.length; l++) {
                        if (nums1[i] + nums2[j] + nums3[k] + nums4[l] == 0) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    public int fourSumONCube(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        Map<Integer, Integer> threeSumToCount = new HashMap<>();
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                for (int k = 0; k < nums3.length; k++) {
                    int threeSum = nums1[i] + nums2[j] + nums3[k];
                    if (!threeSumToCount.containsKey(threeSum)) {
                        threeSumToCount.put(threeSum, 0);
                    }
                    int newSum = threeSumToCount.get(threeSum)+1;
                    threeSumToCount.put(threeSum, newSum);
                }
            }
        }

        int res = 0;
        for (int i = 0; i < nums4.length; i++) {
            int toFind = 0 - nums4[i];
            if (threeSumToCount.containsKey(toFind)) {
                res += threeSumToCount.get(toFind);
            }
        }

        return res;
    }

    public int fourSumONSquare(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        Map<Integer, Integer> twoSum1 = new HashMap<>();
        Map<Integer, Integer> twoSum2 = new HashMap<>();
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                int twoSum = nums1[i] + nums2[j];
                if (!twoSum1.containsKey(twoSum)) {
                    twoSum1.put(twoSum, 0);
                }
                int newSum = twoSum1.get(twoSum)+1;
                twoSum1.put(twoSum, newSum);
            }
        }

        for (int i = 0; i < nums3.length; i++) {
            for (int j = 0; j < nums4.length; j++) {
                int twoSum = nums3[i] + nums4[j];
                if (!twoSum2.containsKey(twoSum)) {
                    twoSum2.put(twoSum, 0);
                }
                int newSum = twoSum2.get(twoSum)+1;
                twoSum2.put(twoSum, newSum);
            }
        }

        int res = 0;
        for (int twoSumKey1: twoSum1.keySet()) {
            int toFind = 0 - twoSumKey1;
            if (twoSum2.containsKey(toFind)) {
                res += twoSum1.get(twoSumKey1) * twoSum2.get(toFind);
            }
        }

//        for (int twoSumKey2: twoSum2.keySet()) {
//            int toFind = 0 - twoSumKey2;
//            if (twoSum1.containsKey(toFind)) {
//                res += twoSum1.get(toFind) * twoSum2.get(twoSumKey2);
//            }
//        }
        return res;
    }
}
