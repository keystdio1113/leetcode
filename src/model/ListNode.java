package model;
public class ListNode {
    public int val;
    public ListNode next;
    public ListNode() {}
    public ListNode(int val) { this.val = val; }
    public ListNode(int val, ListNode next) { this.val = val; this.next = next; }

    public String toString() {
        return String.format("[%s,%s]", val, next);
    }

    public static void addToEnd(ListNode head, int val) {
        if (head == null) {
            throw new IllegalArgumentException("Head cannot be null");
        }

        ListNode curr = head;
        if (curr.next == null) {
            curr.next = new ListNode(val);
        } else {
            while (curr.next != null) {
                curr = curr.next;
            }
            curr.next = new ListNode(val);
        }
    }

    public static ListNode createLinkedList(int[] input) {
        if (input.length == 0) {
            return null;
        } else {
            ListNode head = new ListNode(input[0]);
            ListNode curr = head;
            for (int i = 1; i < input.length; i++) {
                curr.next = new ListNode(input[i]);
                curr = curr.next;
            }
            return head;
        }
    }

    public static String toString(ListNode head) {
        if (head == null) {
            return "[]";
        } else {
            StringBuilder res = new StringBuilder("[" + head.val);
            ListNode curr = head.next;
            // TODO: This will be very sad if there is a loop in the list, this will be an infinite loop
            while (curr != null) {
                res.append(", ").append(curr.val);
                curr = curr.next;
            }
            res.append("]");
            return res.toString();
        }
    }

    public static void printList(ListNode head) {
        System.out.println(toString(head));
    }
}
