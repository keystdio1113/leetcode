package model;

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this(val);
        this.left = left;
        this.right = right;
    }

    public String toString() {
        return String.format("[%s, %s, %s]",
                val,
                this.left == null ? "NULL" : "*",
                this.right == null ? "NULL" : "*"
        );
    }
}
