import java.util.HashMap;
import java.util.Map;

public class Q2466 {
    public static void main(String[] args) {
        System.out.println(countGoodStrings(200, 200, 10, 1));
    }


    public static int countGoodStrings(int low, int high, int zero, int one) {
        // String option1 = stringAdd("0", zero);
        // String option2 = stringAdd("1", one);

        // return (helper("", low, high, option1, option2)) % (1000000000+7);

        int mod = 1000000000+7;
        Map<Integer, Integer> lengthToCount = new HashMap<>();
        return smart(0, low, high, zero, one, lengthToCount) % mod;
    }

    private static int smart(int length, int low, int high, int zero, int one, Map<Integer, Integer> lengthToCount) {
        if (lengthToCount.containsKey(length)) {
            return lengthToCount.get(length);
        } else {
            if (length > high) {
                return 0;
            } else {
                int extra = length < low ? 0 : 1;
                int result = extra + smart(length+zero, low, high, zero, one, lengthToCount) + smart(length+one, low, high, zero, one, lengthToCount);
                result = result % 1000000007;

                lengthToCount.put(length, result);
                return result;
            }
        }
    }

    // private int helper(String curr, int low, int high, String option1, String option2) {
    //     if (curr.length() > high) {
    //         return 0;
    //     } else if (curr.length() < low) {
    //         return helper(curr+option1, low, high, option1, option2) + helper(curr+option2, low, high, option1, option2);
    //     } else {
    //         return 1 + helper(curr+option1, low, high, option1, option2) + helper(curr+option2, low, high, option1, option2);
    //     }
    // }


    // private String stringAdd(String s, int length) {
    //     String result = "";
    //     for (int i = 0; i < length; i++) {
    //         result = result + s;
    //     }
    //     return result;
    // }
}
