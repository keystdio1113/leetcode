class Q1140 {
    public int stoneGameII(int[] piles) {
        return dfs(piles, 1, 0, true);
    }

    private int dfs(int[] piles, int M, int takenPiles, boolean aliceTurn) {
        if (takenPiles == piles.length) {
            return 0;
        } else {
            int result = aliceTurn ? -1 : Integer.MAX_VALUE;
            int score = 0;
            for (int X = 1; X <= 2*M && X <= piles.length-takenPiles; X++) {
                score += piles[takenPiles + X - 1];
                if (aliceTurn) {
                    result = Math.max(result, score + dfs(piles, Math.max(M, X), takenPiles+X, !aliceTurn));
                } else {
                    result = Math.min(result, dfs(piles, Math.max(M, X), takenPiles+X, !aliceTurn));
                }
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Q1140 q1140 = new Q1140();
        System.out.println(q1140.stoneGameII(new int[] {2,7,9,4,4}));
    }
}