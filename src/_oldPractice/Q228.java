import java.util.ArrayList;
import java.util.List;

public class Q228 {
    public static void main(String[] args) {
        System.out.println(summaryRanges(new int[] {0,1,2,4,5,7}));
    }


    public static List<String> summaryRanges(int[] nums) {
        List<String> res = new ArrayList<>();
        if (nums.length == 0) {
            return res;
        } else if (nums.length == 1) {
            res.add(nums[0] + "");
            return res;
        } else {
            for (int i = 0; i < nums.length; i++) {
                int start = nums[i];
                while (i+1 < nums.length && nums[i+1]-nums[i] == 1) {
                    i++;
                }

                if (start != nums[i]) {
                    res.add(String.format("%s->%s", start, nums[i]));
                } else {
                    res.add(start + "");
                }
            }

            return res;
        }
    }
}
