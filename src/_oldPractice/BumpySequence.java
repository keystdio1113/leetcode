package _oldPractice;

import java.util.Arrays;
import java.util.Stack;

public class BumpySequence {
    public static void main(String[] args) {
        int[] testEdge0 = new int[]{};
        int[] testEdge1 = new int[]{1};
        int[] testEdge2 = new int[]{1, 1, 1, 1, 1, 1};
        int[] testEdge3 = new int[]{1, 2};

        int[] test1 = new int[]{ 5, 5, -1, 2, 2, 3, 4 }; // should be 3
        int[] test2 = new int[]{ 2, 4, 7, 3, 10, 5, 5 }; // I got 5
        int[] test3 = new int[]{ 2, 4, -1, 9, 0, 5, -2 }; // I got 7
        int[] test4 = new int[]{ 1, 2, 0, 7, 3, 5, 4 }; // I got 7
        int[] test5 = new int[]{ 3, 3, 4, 4 }; // I got 2
        int[] test6 = new int[]{ 3, 3, 4, 4, 4, 3 }; // I got 3

        int[] test7 = new int[]{2, 4, -1, 9, 0, 5, -2};
        int[] test8 = new int[]{2, 4, 7, 3, 10, 5, 5};
        int[] test9 = new int[]{1, 2, 0, 7, 3, 5, 4};
        int[] test10 = new int[]{1, 2, 0, 7, 3, 5};
        int[] test11 = new int[]{1, 2, 0, 0, 3, 5, 3, 6, 6, 0};
        int[] test12 = new int[]{2, 4, 4, 4, 4, -1, 9, 0, 5, -2};
        int[] test13 = new int[]{2, 4, 7, 3, 10, 5, -5};
        int[] test17 = new int[]{5, 5, -1, 2, 2, 3, 4};
        int[] test18 = new int[]{-1, 5, -1, 5, 5, 5, -1, 1, -1, 1, -1, 1, -1, 1};
        int[] test19 = new int[]{5, 5, -1, 2, 2, 3, 4, 1};

//        Utils.assertTrue(solution(testEdge0) == 0);
        Utils.assertTrue(solution(testEdge1) == 1);
        Utils.assertTrue(solution(testEdge2) == 1);
        Utils.assertTrue(solution(testEdge3) == 2);

        Utils.assertTrue(solution(test1) == 3);
        Utils.assertTrue(solution(test2) == 5);
        Utils.assertTrue(solution(test3) == 7);
        Utils.assertTrue(solution(test4) == 7);
        Utils.assertTrue(solution(test5) == 2);
        Utils.assertTrue(solution(test6) == 3);

        Utils.assertTrue(solution(test7) == 7);
        Utils.assertTrue(solution(test8) == 5);
        Utils.assertTrue(solution(test9) == 7);
        Utils.assertTrue(solution(test10) == 6);
        Utils.assertTrue(solution(test11) == 7);
        Utils.assertTrue(solution(test12) == 7);
        Utils.assertTrue(solution(test13) == 5);
        Utils.assertTrue(solution(test17) == 3);
        Utils.assertTrue(solution(test18) == 12);
        Utils.assertTrue(solution(test19) == 4);

    }

    public static int solution(int[] input) {
        int[] T = new int[input.length];
        int[] S = new int[input.length];

        // init values
        for (int i = 0; i < input.length; i++) {
            T[i] = 1;
            S[i] = -1;
        }

        // recurrence
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j <= i-1; j++) {
                boolean canBePlacedIntoSequence = true;
//                System.out.printf("Comparing index %s and %s, T[i] = %s, T[j] = %s, ", i, j, T[i], T[j]);
                int jPreviousIndex = S[j];
                if (jPreviousIndex == -1) {
//                    System.out.print("No previous index, ");
                    canBePlacedIntoSequence = input[i] != input[j]; // we don't allow equal in the bumpiness
                } else {
                    if (input[i] == input[j]) {
                        canBePlacedIntoSequence = false;
                    } else {
                        boolean jIsPeak = (input[j]-input[jPreviousIndex]) > 0;
                        boolean iIsLargerThanJ = (input[i]-input[j]) > 0;
                        if (jIsPeak) {
//                            System.out.print("j index is peak, ");
                            canBePlacedIntoSequence = !iIsLargerThanJ;
                        } else {
//                            System.out.print("j index is valley, ");
                            canBePlacedIntoSequence = iIsLargerThanJ;
                        }
                    }
                }
//                System.out.printf("canBePlacedIntoSequence = %s\n", canBePlacedIntoSequence);

                if (canBePlacedIntoSequence) {
                    if (1+T[j] > T[i]) {
                        T[i] = 1+T[j];
                        S[i] = j;
                        int a = 1;
                    } else {
                        T[i] = T[i];
                    }
                }
            }
        }

        int max = 0;
        int maxIndex = -1;
        for (int i = 0; i < input.length; i++) {
            if (max < T[i]) {
                max = T[i];
                maxIndex = i;
            }
        }

        // find max value
        System.out.printf("Array %s has the LBS: ", Arrays.toString(input));
        backtrack(input, S, maxIndex);
        System.out.printf("with a length of %s\n", max);

        return max;
    }

    public static void backtrack(int[] input, int[] S, int from) {
        Stack<Integer> result = new Stack<>();
        while (S[from] != -1) {
            result.push(input[from]);
            from = S[from];
        }
        result.push(input[from]);

        while (!result.isEmpty()) {
            System.out.printf("%s, ", result.pop());
        }
    }
}
