package _oldPractice;

import java.util.Map;
import java.util.TreeMap;

public class Q1493 {
    public static void main(String[] args) {
        longestSubarray(new int[] {1,0,1,0,1,0});
    }

    public static int longestSubarray(int[] nums) {
        int i = 0;
        int zeroCount = 0;

        Map<Integer, Integer> oneSequence = new TreeMap<>();
        int j = 0;
        while (i < nums.length) {
            if (nums[i] == 0) {
                zeroCount++;
            } else {
                j = i;
                while (j < nums.length && nums[j] == 1) {
                    j++;
                }
                oneSequence.put(i, j-1);
                i = j;
            }
            i++;
        }

        System.out.printf("OneSequence: %s\n", oneSequence);

        if (zeroCount == 0) {
            return nums.length - 1;
        } else if (zeroCount == nums.length) {
            return 0;
        } else {
            return findLongest(oneSequence);
        }
    }

    private static int findLongest(Map<Integer, Integer> oneSequence) {
        int longest = -1;

        int prevJ = -1;
        int prevSequenceLen = -1;
        boolean init = false;

        for (int i: oneSequence.keySet()) {
            int j = oneSequence.get(i);
            int currSequenceLen = j-i+1;

            if (!init) {
                prevJ = j;
                prevSequenceLen = currSequenceLen;
                longest = currSequenceLen;
                init = true;
            } else {
                int zeroCountInBetween = i - prevJ - 1;
                if (zeroCountInBetween == 1) {
                    longest = Math.max(longest, prevSequenceLen + currSequenceLen);
                } else {
                    longest = Math.max(longest, currSequenceLen);
                }
                prevJ = j;
                prevSequenceLen = currSequenceLen;
            }
        }
        return longest;
    }
}
