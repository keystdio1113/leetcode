import java.util.*;

public class Q54 {
    public static void main(String[] args) {
        final int[] data = new int[] { 5, 4, 2, 1, 3 };
        Arrays.sort(data);


        spiralOrder(new int[][]{
                new int[] {1, 2, 3},
                new int[] {4, 5, 6},
                new int[] {7, 8, 9}
        });
    }


    public static List<Integer> spiralOrder(int[][] matrix) {
        Set<String> visited = new HashSet<>();
        List<Integer> result = new ArrayList<>();

        Map<Integer, Integer> flagCount = new HashMap<>(Map.of(
                0, 0,
                1, 0,
                2, 0,
                3, 0
        ));

        int m = matrix.length;
        int n = matrix[0].length;

        int flag = 0;
        String loc = "";
        int i = 0;
        int j = 0;
        do {
            int currAction = flag % 4;
            if (currAction == 0) { // right
                while (j < n - flagCount.get(currAction)) {
                    result.add(matrix[i][j]);
                    visited.add(i + ":" + j);
                    j++;
                }
                flagCount.put(currAction, flagCount.get(currAction) + 1);
                loc = i + ":" + j;
                i++;
            } else if (currAction == 1) { // down
                while (i < m - flagCount.get(currAction)) {
                    result.add(matrix[i][j]);
                    visited.add(i + ":" + j);
                    i++;
                }
                flagCount.put(currAction, flagCount.get(currAction) + 1);
                loc = i + ":" + j;
                j--;
            } else if (currAction == 2) { // left
                while (j >= 0 + flagCount.get(currAction)) {
                    result.add(matrix[i][j]);
                    visited.add(i + ":" + j);
                    j--;
                }
                flagCount.put(currAction, flagCount.get(currAction) + 1);
                loc = i + ":" + j;
                i++;
            } else { // up
                while (i >= 0 + flagCount.get(currAction)) {
                    result.add(matrix[i][j]);
                    visited.add(i + ":" + j);
                    i--;
                }
                flagCount.put(currAction, flagCount.get(currAction) + 1);
                loc = i + ":" + j;
                j++;
            }
            flag++;
        } while (!visited.contains(loc));

        return result;
    }
}
