import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Q1799 {
    private static Map<Integer, Integer> cache = new HashMap<>();

    public static void main(String[] args) {
        System.out.println(maxScore(new int[] {1,2,3,4,5,6}));
//        System.out.println(maxScore(new int[] {773274,313112,131789,222437,918065,49745,321270,74163,900218,80160,325440,961730}));
    }

    public static int maxScore(int[] nums) {
        return helper(nums, 0, 1, 0);
    }

    public static int helper(int[] nums, int mask, int i, int currScore) {
        if (cache.containsKey(mask)) {
            return cache.get(mask);
        }

        if (mask == ((1<<nums.length) - 1)) {
            return currScore;
        }

        int max = -1;
        for (int a = 0; a < nums.length; a++) {
            for (int b = a + 1; b < nums.length; b++) {
                boolean iBeenUsed = ((1 << a) & mask) > 0;
                boolean jBeenUsed = ((1 << b) & mask) > 0;
                if (iBeenUsed || jBeenUsed) {
                    continue;
                }
                int num1 = nums[a];
                int num2 = nums[b];
                int score = i * gcd(num1, num2);
                int newMask = mask | (1 << a) | (1 << b);
                max = Math.max(max, helper(nums, newMask, i+1, currScore+score));
            }
        }
        cache.put(mask, max);
        return max;
    }

    public static int gcd(int a, int b) {
        if (a == 0 && b != 0) {
            return b;
        } else if (a != 0 && b == 0) {
            return a;
        } else {
            int quotient = a / b;
            int remaindar = a % b;
            return gcd(b, remaindar);
        }
    }
}
