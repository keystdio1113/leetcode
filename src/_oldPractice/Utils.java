package _oldPractice;

public class Utils {
    public static void assertTrue(boolean b) {
        if (!b) {
            throw new IllegalArgumentException("Assertion doesn't hold");
        }
    }
}
