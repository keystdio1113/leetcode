package _oldPractice;

import java.util.Stack;

public class Q227 {

    public static void main(String[] args) {
//        System.out.println(calculate("3+2*2"));
        System.out.println(calculate("1"));
    }

    public static int calculate(String s) {
        Stack<Integer> stack = new Stack<>();

        String currNumber = "";
        String pendingOp = "";

        for (int i = 0; i < s.length(); i++) {
            char currChar = s.charAt(i);
            if (currChar == ' ') {
                // do nothing.....
            } else if (Character.isDigit(currChar)) {
                currNumber += s.charAt(i);
            } else { // an op
                int num = Integer.parseInt(currNumber);
                currNumber = "";
                if (pendingOp.length() != 0) {
                    int v1 = stack.pop();
                    if (pendingOp.equals("*")) {
                        stack.push(v1*num);
                    } else {
                        stack.push(v1/num);
                    }
                    pendingOp = "";
                } else {
                    if (currChar == '+') {
                        stack.push(num);
                    } else if (currChar == '-') {
                        stack.push(-1 * num);
                    } else {
                        stack.push(num);
                        pendingOp = currChar + "";
                    }
                }

            }
        }

        int result = 0;
        if (currNumber.length() != 0 && pendingOp.length() != 0) {
            int num = Integer.parseInt(currNumber);
            int v1 = stack.pop();
            if (pendingOp.equals("*")) {
                result += (v1*num);
            } else {
                result += (v1/num);
            }
        }

        if (currNumber.length() != 0 && pendingOp.length() == 0) {
            int num = Integer.parseInt(currNumber);
            result += num;
        }

        while (!stack.isEmpty()) {
            result += stack.pop();
        }
        return result;
    }

}