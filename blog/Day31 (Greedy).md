# Day31_455_AssignCookies
Not too much new stuff here, pretty straightforward.

# Day31_WiggleSubsequence
Relearned a lot of DP stuff in class. Some important take aways:
1. ups and downs first.array really clever!
2. we could actually keep overriding the same elements in the DP first.array to keep things simple

This can also be done with first.greedy approach. And the first.greedy approach, the less than equal sign of the prev Diff is really hard to get wrong...

# Day31_53_MaximumSubArray
Did it the DP way, but the first.greedy algorithm just makes DP a bit too overcomplicated?