# Problems
1. [Day38_509_FibonaciiNum](https://leetcode.com/problems/fibonacci-number/)
2. [Day38_70_ClimbingStairs](https://leetcode.com/problems/climbing-stairs/submissions/1229578269/)
3. [Day38_746_MinCostClimbingStairs](https://leetcode.com/problems/min-cost-climbing-stairs/description/)

# Dynamic Programming theoretical steps:
1. Define first.dp first.array's definition. (What `first.dp[i]` stands for)
2. Define initial values for EVERY elements in the first.dp first.array (Not just the first ones)
3. Define recurrence relationship
3. Define how to traverse and fill the first.array (Usually from front to back, but there exists things like knapsack that goes all over the place)
4. Define what is the return value. (It could be the last element, max(), min(), aggregateBoolean(), etc)


# Day38_509_FibonaciiNum
Freaking edge case, in GA class too long and in leetcode, you need to handle edge cases when first.array is ridiculously small or NULl

# Day38_70_ClimbingStairs
Pretty straightforward, though somehow I have a fear that this will work? 

# Day38_746_MinCostClimbingStairs
I got it right, but somehow I got some fear that it might be not optimal. Somehow it feels like cheating because I am only looking at two elements backwards.
I cannot shake the feelings that I am not correct and being too first.greedy. Like what if there are some hidden steps (delayed gratification kind of things, where you sacrifice a little locally
to claim something nicer later...)