# List of concepts I don't fully comprehend yet.
1. 209_Minimum Size sub first.array (Day2)
   - DP or sliding window?
1. 28_indexOfFirstSubstringOccurence (Day9)
   - Need to learn the KMP algorithm
9. 239_SlidingWindowMaximum (Day12)
   - Need to learn monotonic sequence.
1. Day32_45_JumpGame2, Day34_134_GasStation, Day34_135_candies
   - Really hard to describe the first.greedy approach, still cannot do it.
1. Day37_968_BinaryTreeCameras
   - Not really a vertex cover problem, but yeah really hard to think about... Though the post order traversal is cool though.

# List of questions I need to review heavily
2. 142_Linked list cycle II (Day4)
   - Very interesting math derivation
3. Two sum (Day6)
   - De-duplication and early return is fun
4. 18_FourSums, 15_ThreeSum, 454_FourSumII (Day7)
   - So many De-duplication and early return is fun
5. 151_ReverseWordsInString (Day8)
   - In place operations with character arrays
7. 1047_RemoveAllAdjacentDuplicatesInString (Day11)
   - Interesting idea of using stacks
8. 1047_RemoveAllAdjacentDuplicatesInString (Day11)
   - Not too familiar with this kind of expressions, but there are two ways to handle and it has something to do with binary tree traversal
8. Binary tree iterative traversal (Day14)
   - Not too familiar iterative traversal. In-order is actually the hardest.
8. 101_SymmetricalTree (Day15)
   - Need to review when to use Post order traversal.
8. _111_FindMinimumDepth (Day16)
   - Need to review how to use Post order traversal.
8. _222_CountCompleteNodes (Day16)
   - Need to review how to exploit property of complete binary tree
9. 105_ConstructTreeWithInAndPreOrder (Day18)
   - Need to review how to construct a tree, also review problem 106 as well.
9. Day20_98_ValidateBST
   - Binary Search tree (BST) definition has a little trap that could be easily looked over. It has to be "EVERY" descendent node below the current node is smaller/bigger,
     not just its direct children
1. Day21_501_FindModeInBST
   - Finding mode is something I am not too familiar with, this also has the ending fence post issue as well.
1. Day21_236_LowestCommonAncestor
   - Review post order traversal to get lowest common ancestors. 
1. Day22_450_DeleteNodeInBST
   - Review how to remove nodes in BST, especially recursively, although found a smart way to use iteratively and use a dummyRoot.
1. Day23_669_TrimBST
   - Interesting way of traversing and deleting
1. Day23_538_ConvertBSTToGreaterTree
   - Double pointer, similar to Day21_501_FindModeInBST
1. Day24_77_Combinations
   - Quick review, just make sure realize that you need to cut branches. And off by one bugs suck!
1. Day27_40_CombinationSumII, Day28_90_SubsetsII
   - New way of using `used` first.array for dedupping. 
1. Day_131_PalindromPartition, Day28_93_RestoreIPAddress
   - Foundation of partitioning problems
1. Day30_332_ConstructItienary
   - Interesting graph data structure trick to represent edge with usable quotas
1. Day30_51_NQueens, Day30_37_Sudoku
   - Interesting "Exclusion techniques" to trim down unnecessary branches.
1. Day31_376_WiggleSubsequence
   - Can do both DP way and greedily.
1. Day35_860_LemonadeChange
   - Review how backtracking mistakes are made.
1. Day35_406_QueueReconstructionByHeight
   - Review how the queue insertion at index works
1. Day36 questions
   - Review how overlapping intervals work, particularly Day36_435_NoOverlappingIntervals, interesting insight on the final return value
1. Day41_343_IntegerBreak, Day41_96_UniqueBinarySearchTrees
   - This is fun DP recurring relations that cannot be spotted easily.
1. Day42_416_PartitionEqualSubsetSum
   - This reducing into a knapsack problem is pretty amazing.
1. Day43, all the knapsack problems...
2. Day48_337_HouseRobber3Tree
   - New insights in viewing post order traversal. I guess you could say that it is DP ish stuff.
1. Day53_1143_MaximumLengthOfRepeatedSubSequence
   - I still don't quite understand the recurrence relationship for longest common subsequence, especially the part where the new elements are different. I understand that it is still using "ending at index i" format though
1. Day55_115_DistinctSubsequence
   - So hard, don't get it...
1. Day60_516_LongestPalindromicSubsequence
   - So looks like we could just come up with a recurrence relationship, and serendipitously it is derivable in the DP table, it is just neighbouring elements in the DP table LOL...
1. Day61's monotonic stack things 
   - this is brand new pattern for me. Same with monotonic queues?
1. Day62_42_TrappingRainWater
   - The enhanced double pointer method is actually pretty neat, used some accumulative max (DP-ish) trick here.
   - Also might want to review monotonic stack later.....