# Problems
10 questions on level-order-traversal:
1. [_102_LevelOrderTraversal](https://leetcode.com/problems/binary-tree-level-order-traversal/description/)
2. [_107_LevelOrderTraversalII](https://leetcode.com/problems/binary-tree-level-order-traversal-ii/description/)
3. [_199_RightSideView](https://leetcode.com/problems/binary-tree-right-side-view/description/)
3. [_637_AverageOfLevelsInBinaryTree](https://leetcode.com/problems/average-of-levels-in-binary-tree/description/)
3. [_429_NAryTreeLevelOrderTraversal](https://leetcode.com/problems/n-ary-tree-level-order-traversal/description/)
3. [_515_findLargestValueInEachRow](https://leetcode.com/problems/find-largest-value-in-each-tree-row/description/)
3. [_116_PopulatingNextRightPointersInEachNode](https://leetcode.com/problems/populating-next-right-pointers-in-each-node/description/)
3. [_117_PopulatingNextRightPointersInEachNodeII](https://leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/description/)
3. [_104_MaximumDepth](https://leetcode.com/problems/maximum-depth-of-binary-tree/description/)
3. [_111_FindMinimumDepth](https://leetcode.com/problems/minimum-depth-of-binary-tree/description/)

Regular questions:
1. [_226_InvertTree](https://leetcode.com/problems/invert-binary-tree/description/)
2. [_101_SymmetricalTree](https://leetcode.com/problems/symmetric-tree/description/)

# Take aways
Binary tree problem key points:
1. Be sure about the traversal orderings
2. If we need info that requires the child nodes to finishes first, we will use post order traversal as it does LEFT-RIGHT-NODE or some other combination. The key is to handle what's beneath first, then do me. (Bottom-up approach?)

# Symmetrical tree
Symmetrical tree is a good thing to learn from. Post order traversal is used to:
1. Collect some information about the `inside`  (In this case, is the elements "left's right" and "right's left" symmetric)
2. Collect some information about the `outside` (In this case, is the elements "left's left" and "right's right" symmetric)
3. Confirm if the current two value is the same.

We are collecting information from below first and then based on the current node situation, returns back up.

# Level wise traversal
The key of maintaining the queue during the BFS is to:
1. Drain EVERY single nodes in the queue when popping elements.
2. Make sure to get the initial size count of the queue and not rely on `q.size()` as that value will change during BFS explore

