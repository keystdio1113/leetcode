# Problems
1. [BinaryTree-PreOrder](https://leetcode.com/problems/binary-tree-preorder-traversal/submissions/1207450602/)
1. [BinaryTree-InOrder](https://leetcode.com/problems/binary-tree-inorder-traversal/)
1. [BinaryTree-PostOrder](https://leetcode.com/problems/binary-tree-postorder-traversal/submissions/1207450024/)

# Take away
Iterative traversal of binary tree is not easy. Pre-order and Post-order is kind of the reverse of each other. InOrder
is harder because the traversal order is different from getting the results 

# Iterative traversal

For preorder and postorder, it is important to realize that they are almost similar with one another. Preorder traversal outputs value in
`VAL-LEFT-RIGHT` order while Post order traversal outputs value in `LEFT-RIGHT-VAL` order. We could actually first make preorder traveral outputs
`VAL-RIGHT-LEFT` order, and then reverse the result list to get post order traversal. Pretty smart

For in order traversal, we will first have to go all the way down the left tree, get the value and take the right node, and go all the way left again.