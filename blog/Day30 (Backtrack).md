# Problems
1. [Day30_332_ConstructItienary](https://leetcode.com/problems/reconstruct-itinerary/submissions/1222498981/?source=submission-noac)
2. [Day30_51_NQueens](https://leetcode.com/problems/n-queens/editorial/)
3. [Day30_37_Sudoku](https://leetcode.com/problems/sudoku-solver/)

# Day30_332_ConstructItienary
Learned:
1. How to make an graph adjaceny list with quota
2. How to return boolean values rather than throwing an exception if the return value doesn't really matter

# Day30_51_NQueens
Perhaps should not create a chessboard class as it gets complicated. But this is good enough I am happy.

# Day30_37_Sudoku
Backtracking idea is clear, but a clever trick with finite size arrays to cut branches premptively.