# Problems
1. [Day35_860_LemonadeChange](https://leetcode.com/problems/lemonade-change/submissions/1226969987/)
2. [Day35_406_QueueReconstructionByHeight](https://leetcode.com/problems/queue-reconstruction-by-height/)
3. [Day35_452_MinimumNumberOfArrowsBurstBallons](https://leetcode.com/problems/minimum-number-of-arrows-to-burst-balloons/submissions/1227156548/)

# Day35_860_LemonadeChange
Solved it with backtracking first, timeout for sure. And spent a lot of time debugging backtrack. Had to deal with:
1. StartI
2. Cannot use sharable counter across multiple backtrace branch. They will mess with each other. 

```java
    public static boolean backtrack(int[] bills, int startI) {
        boolean good = true;
        for (int i = startI; i < bills.length; i++) {
            if (bills[i] == 5) {
                fiveDollarBill++;
            } else if (bills[i] == 10) {
                tenDollarBill++;
                // attempt to make change for 5, if there isn't any 5 dollar left, we are done
                if (fiveDollarBill == 0) {
                    return false;
                } else {
                    fiveDollarBill--;
                }
            } else {  // bills[i] == 20
                // we could break 20 dollars bills in two ways:
                //   3x $5
                //   1x $5 and 1x $10
                boolean breakWith3Fives = fiveDollarBill >= 3;
                boolean canBreak20 = false;
                if (breakWith3Fives) {
//                    System.out.println("recursing with 3x $5");
                    fiveDollarBill -= 3;
                    canBreak20 |= backtrack(bills, i+1);
                    fiveDollarBill += 3;
                }

                boolean breakWith1TenAnd1Fives = fiveDollarBill >= 1 && tenDollarBill >= 1;
                if (breakWith1TenAnd1Fives) {
//                    System.out.println("recursing with 1x $5 and 1x $10");
                    fiveDollarBill -= 1;
                    tenDollarBill -= 1;
                    canBreak20 |= backtrack(bills, i+1);
                    fiveDollarBill += 1;
                    tenDollarBill += 1;
                }
                if (!canBreak20) {
                    return false;
                }
            }
        }
        return good;
```

This is not good, as we could not share the fiveDollarBill static variables across two different branches.

Can't believe first.greedy works here, I think this is true to prioritizing using the less flexible 10-5 combo first and then go to the 3x5 combo. 


# Day35_406_QueueReconstructionByHeight
Whoa, the first.greedy strategy just serendipitously matches up with how queue.insert at index works. I guess I am not that familiar with list insert manipulations.

# Day35_452_MinimumNumberOfArrowsBurstBallons
Weird cases:
1. Got overflow during the comparator in sort, had to use `Integer.compare(a,b)`
2. Need to update the stack border once we are eating more ballons