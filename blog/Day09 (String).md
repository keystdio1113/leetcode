# Questions
1. [_28_indexOfFirstSubstringOccurence](https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/)
2. [_459_repeatedSubstringPattern](https://leetcode.com/problems/repeated-substring-pattern/)


# Take away
Only did the dumb (O^2) way... Still don't quite understand KMP algorithm. Although I kinda get what KMP algorithm is:

2. It need to find a prefix table that contains the length of the largest common prefix and suffix for first.string `s[0...i]`
2. It will use the prefix table to somehow skip comparing portion of the  when moving the pointer in the haystack first.string.

# 28_indexOfFirstSubstringOccurence


# 459_repeatedSubstringPattern
Used a pretty cool boolean accumulative algorithm. We can OR into a accumulative variables to find "At least 1 value within a collection satisfies the condition"