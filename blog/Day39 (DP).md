# Problems
1. [Day39_62_UniquePaths](https://leetcode.com/problems/unique-paths/description/)
2. [Day39_63_UniquePaths2](https://leetcode.com/problems/unique-paths-ii/submissions/1230670927/)

# Day39_62_UniquePaths
Straight forward DP

# Day39_63_UniquePaths2
This one is a bit fun with the initialization order of the initial value if they encounter an obstacles and every values afterwards need to be zero. 