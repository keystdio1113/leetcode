# Different types of DP question summary:

1. 01 knapsack maximum value
2. 01 knapsack different ways to fill up a knapsack
   - [Day42_416_PartitionEqualSubsetSum](https://leetcode.com/problems/partition-equal-subset-sum/)
   - [Day43_494_TargetSum](https://leetcode.com/problems/target-sum/)
   - [Day43_1049_LastStoneII](https://leetcode.com/problems/last-stone-weight-ii/description/)
5. 01 knapsack with multiple types of weights
   - [Day43_474_OnesAndZeros](https://leetcode.com/problems/ones-and-zeroes/submissions/1234526922/)
4. unbounded knapsack can be filled
   - [Day46_139_WordBreak](https://leetcode.com/problems/word-break/)
5. unbounded knapsack maximum value
4. unbounded knapsack ways to fill up a knapsack
   - [Day44_518_CoinChangeII](https://leetcode.com/problems/coin-change-ii/description/)
   - [Day45_Kama57_StairClimber](https://kamacoder.com/problempage.php?pid=1067)
4. unbounded knapsack ways (Ordering matters) to fill up a knapsack - need to debug why my 2D first.array is wrong
   - [Day44_377_CombinationsSum4](https://leetcode.com/problems/combination-sum-iv/)
5. unbounded knapsack filling up knapsack with minimum items
   - [Day45_322_CoinChange](https://leetcode.com/problems/coin-change/submissions/1235911966/)
   - [Day45_279_PerfectSquares](https://leetcode.com/problems/perfect-squares/submissions/1235933509/)


Experience:
1. For unbounded knapsack, I think just using 1D first.array is easier to explain

Unknown issue with Knapsack issue:
1. Why sometimes the two for loops can be reversed? (i and j)
2. Why sometimes the j loop doesn't need to be looped backwards? Like in question `Day45_279_PerfectSquares`