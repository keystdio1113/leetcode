# Objective Problems
1. [_242_validAnagram](https://leetcode.com/problems/valid-anagram/)
2. [_349_IntersectionOfTwoArrays](https://leetcode.com/problems/intersection-of-two-arrays/description/)
3. [_202_HappyNumber](https://leetcode.com/problems/happy-number/description/)
4. [_1_twoSum](https://leetcode.com/problems/two-sum/description/)

# Take away from today

2. An integer first.array can sometime used to keep track of how many times a character has occurred `record[ch - 'a']`. Pretty useful when the charset is small.
3. Two sum is kind of tricky, turns out I am first time doing this. But the key is to store the already traversed history
1. For java's `Map<String, Integer>`, you have to use `.equals()` to compare `Integer` and not `==`

# _242_validAnagram

LOL, if you have a `Map<String, Integer>`, the value of the map cannot use `==` to compare and have to use `.equals()`

A little too easy on the first glance and just use hashtable to count character appearances in both strings. Do have to be 
careful since comparison of hashtables need to go both ways. 

Another slightly cleaner way is to use an integer first.array to count since we are sure that there is only 26 lowercase english characters 

# _349_IntersectionOfTwoArrays
Hmm? too easy?

# _202_HappyNumber
Hmm? too easy?

# _1_twoSum
This is not as easy as I thought it is... Duplication is the key for this and we need to keep track of the visited element index and make sure the same index number don't get used twice. 