# Problems
1. [_104_MaximumDepth](https://leetcode.com/problems/maximum-depth-of-binary-tree/)
1. [_559_NAryTreeMaxDepth](https://leetcode.com/problems/maximum-depth-of-n-ary-tree/)
2. [_111_FindMinimumDepth](https://leetcode.com/problems/minimum-depth-of-binary-tree/submissions/1209419340/)
1. [_222_CountCompleteNodes](https://leetcode.com/problems/count-complete-tree-nodes/submissions/1209411570/)

# Take away
There are differences between a "height" and "depth" of a binary tree. 
```bash
Height   Depth     Tree
   3        1       1
   2        2     2   3
   1        3   4  5 6  7
```

Note:
1. We will use Post-order traversal to get the height, because we want the parent node to process the info from the child node
2. We will use Pre-order traversal if we want the depth as the parent info is passed down to the child


# 222_CountCompleteNodes
The complete binary tree has a property that could be used here. It is very easy to calculate the node number of a FULL binary tree, and we don't 
need to traverse through every nodes. We will just traverse at most the height of to determine if it is a full binary tree since it is given that it is a complete 
tree. That is better in some cases. But worst case, we still need to go through every nodes. 

Learned some interesting bit wise operations with raising 2 power as well using left shift.