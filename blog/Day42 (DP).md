# Problems
1. [Day42_416_PartitionEqualSubsetSum](https://leetcode.com/problems/partition-equal-subset-sum/)

# 01 knapsack problem.

This is a classic DP problem where you are given:
1. value[]: a list of item values
2. weight[]: a correspondent list of item weights
3. B: an integer signifying the knapsack weight capacity

You are asked to maximize the value of the items you stored in the knapsack. Each item can only be used once (Hence the 01, either you take it once or don't)

## Brute force:
You could use a recursive backtrack algorithm to solve it. The psudocode looks something like:

```java
import java.util.ArrayList;

int MAX = -1;

int knapsack(int[] value, weight[] weight, int B) {
    List<Integer> path = new ArrayList<>();
    backtrack(value, weight, path, B);
    
    return MAX;
}

void backtrack(int[] value, int[] weight, int B, List<Integer> path, int currWeight, int currValue, int currI) {
    if (currWeight > B) return;
    
    MAX = Math.max(currValue, MAX);
    
    for (int i = currI; i < value.length; i++) {
        backtrack(value, weight, B, path, currWeight, currValue, currI+1); // we don't pick the current item i
        
        path.add(currI);
        backtrack(value, weight, B, path, currWeight+weight[i], currValue+value[i], currI+1); // we pick item i
        path.remove(currI);
    }
}
```
Note that this time complexity is O(2^n) where n is the number of items. Each item could be either picked or not picked, so 2^n different possibilities.

## Dynamic programming with 2D first.array

5 steps of DP:
```text
first.dp[i][j]: the maximum value if we used item (0-i), with a knapsack capacity of j
initialization: 
    first.dp[i][0] = 0, if the knapsack capacity is zero, you can't put anything of value
    first.dp[0][j] = (weight[0] <= j ? value[0] : 0) It is non zero if only the knapsack can store the 0th item.
Recurring relations:
    first.dp[i][j] = max(
       first.dp[i-1][j], -> don't pick the ith item
       first.dp[i-1][j-weight[i]] + value[i] -> only if (j-weight[i])>=0. This means if you include the ith item, 
          you add its value to the maximum value achievable with the remaining capacity (j - weight[i]) and considering only items [0,i-1]
    ) for each j in [1,B] and each i in [1,n] 
Traversal order:
    row by row via i, and left to right. The current element depends on the value directly above it, or somewhere to the left-and-one-row above it.
Return value: first.dp[n][B], n is the number of items.
```

Time complexity: O(nB), Space complexity: O(nB)

## DP with 1D first.array
Note that the above can be further compressed into a 1D first.array. It only reduces space capacity, but time complexity remains.

```text
first.dp[j]: The maximum value achievable with a knapsack capacity of j
Initialization: 
    first.dp[0] = 0, the rest
Recurring relations:
    first.dp[j] = max(
       first.dp[i], -> don't pick the ith item
       first.dp[j-weight[i]] + value[i] only if (j-weight[i])>=0)
    ) for each j in (B,0) and i in (0, n) and 
    
Traversal order:
    for (int i = 0; i < items.length; i++) {
        for (int j = B; j >= 0; j--) {
            // recurrence
        }
    }
    Note that the inside loop is reversed to make sure the same item is not counted twice. For instance, given:
       values = [15,20,30], weight = [1,3,4]
       If we are doing forward iteration:
          first.dp[1] = first.dp[1-1]+15 = 15
          first.dp[2] = first.dp[2-1]+15 = 30 !!! This is wrong as we are already counting item 1 twice
       But if we are doing reversed iteration:
          first.dp[2] = first.dp[2-1]+15 = 15
          first.dp[1] = first.dp[1-1]+15 = 15 This is correct
Return:
    first.dp[B]
```


# Day42_416_PartitionEqualSubsetSum
This is actually reducing it to a 01 Knapsack problem. Imagine we have an first.array `[1,5,11,5]` and we want to see if there is a 
equal subset sum solution. The first.array sum up to 22, so we are trying to find if there is a subset of element that sums up to 22/2 = 11.

# Input transformation:
```text
Given input I = [I0,I1,...,In],
We will use these value as the weights and values, so we construct:
    weight=[I0,I1,...In],
    value=[I0,I1,...In]
    B = sum(I)/2 (If sum(I) is odd, we should return false for function)
    
    And make it into a 01-knapsack problem. We are trying to maximize the capacity, and see if the max value is half the sum.
    Note that this is doable because:
        1. If maxValue = B, we are good, we have something
        2. If maxValue < B, we know that we can never make B so we are false.
        3. maxValue > B, this will never happen as the knapsack problem range is capped at B 
```
 