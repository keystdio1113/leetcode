# Objective Problems
1. [_203_RemoveElements](https://leetcode.com/problems/remove-linked-list-elements/description/)
1. [_206_ReverseLinkedList](https://leetcode.com/problems/reverse-linked-list/description/)
1. [_707_DesignLinkedList](https://leetcode.com/problems/design-linked-list/description/)

# One take away from today
Calm down and think carefully if I need a while loop of just an if statements when dealing with LinkedList pointers. Also make sure there is only single place to update variables for better book keeping (LC707).

# Remove elements
I know that I need to use dummyHeaders but however I realized I actually do not know how to do things without dummyHeader, but I can still work it out.
Also I initially wrote some more complicated code than I hope and there are cleaner version:
1. For non dummyHead version, I just need to do the head first, and then do the rest
2. when traversing, since I am moving through the entire list anyway, I don't need an entire loop to delete consequent elements.


# Reversed LinkList
I just know how to do it iteratively. But recursively, I guess it is very weird in the return statement, but I could stare at it I think I could
get it out

# Design LinkedList
Needed to make sure there is only 1 place where `this.size` is incremented