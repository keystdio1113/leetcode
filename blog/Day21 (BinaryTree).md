# Problems
1. [Day21_530_MinimumAbsolutionDifferenceInBST](https://leetcode.com/problems/minimum-absolute-difference-in-bst/submissions/1213634791/)
2. [Day21_501_FindModeInBST](https://leetcode.com/problems/find-mode-in-binary-search-tree/)
3. [Day21_236_LowestCommonAncestor](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/)

# Day21_530_MinimumAbsolutionDifferenceInBST
Review of yesterday's in order traversal of BST to get sorted streams

# Day21_501_FindModeInBST
Learned how to find a mode in Binary tree. It is a two traversal process:
1. Find the `maxFrequency` in 1 full traversal of the tree
2. Find the nodes that appear `maxFrequency` times in another traversal

In the 2nd step, Remember that there is a fence post problem here. Since we are adding valid count in the else branch where we moved to the next non-repeating element (which could be the end of tree), 
it is likely that we skip a valid count and reached the end of tree. Hence we need to remember to add it back at the end. 

There is a single traversal way, and that is exploiting the fact the inorder traversal will always get the nodes in sorted order. And modes would be next to each other.

# Day21_236_LowestCommonAncestor
I knew a way to do by finding path and process the tree through the paths. But I did not know how to use 
post order, looks fancy.