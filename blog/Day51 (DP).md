# Problems
1. [Day51_309_BestTimeToBuyAndSellStockWithCoolingPeriod](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/submissions/1241067233/)
2. [Day51_714_BestTimeToBuyAndSellStockWithTransactionFee](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/submissions/1241048322/)

# Take aways
Don't be shy on encoding more states in the DP first.array. But first you need to understand what those states mean. Honestly, I feel like knowing how to bruteforce your way out is pretty enough already.