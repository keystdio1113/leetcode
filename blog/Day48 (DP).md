# Problems
1. [Day48_198_HouseRobber1](https://leetcode.com/problems/house-robber/description/)
2. [Day46_213_HouseRObber2](https://leetcode.com/problems/house-robber-ii/description/)
2. [Day48_337_HouseRobber3Tree](https://leetcode.com/problems/house-robber-iii/submissions/1238578997/)

# Day48_337_HouseRobber3Tree

This is a brand new concept of having binary tree doing DP. We are using post-order traversal and have the bottom child to return a status
to the above parents to calculate necessary information.  
