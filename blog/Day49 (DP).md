# Problems
1. [Day49_121_BestTimeToBuyAndSellStock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description/)
2. [Day49_122_BestTimeToBuyAndSellStock2MultiBuy](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/submissions/1239202059/)

# Takeaways
Learn that we could potentially embed an extra piece of information in the DP first.array to save another inner for loop to trim down time complexity

This is very similar to yesterday's binary tree for robber. Where we return children's node information to parent's node