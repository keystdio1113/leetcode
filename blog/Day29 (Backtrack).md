# Problems
1. [Day29_491_NonDecreasingSubSequence](https://leetcode.com/problems/non-decreasing-subsequences/submissions/1221332891/)
2. [Day29_46_Permutations](https://leetcode.com/problems/permutations/)
3. [Day29_47_PermutationsII](https://leetcode.com/problems/permutations-ii/submissions/1221524883/)

# Take away

1. Really practiced on the sub sequence on how to do dedupping, know the subtle differences between dedupping in branches and dedupping prior to dedupping.
1. Using `remaining` elements at each block really makes reasoning each recursion step easier

# Note: recursion tree graph and notes are in the `.drawio` files in this repo.