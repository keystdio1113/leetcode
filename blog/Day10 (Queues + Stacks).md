
# Questions
1. [_225_ImplementStacksWithTwoQueues](https://leetcode.com/problems/implement-stack-using-queues/)
2. [_232_ImplementQueuesWithTwoStacks](https://leetcode.com/problems/implement-queue-using-stacks/)


# Take away
Pay attention to the ordering of the queues and stacks elements. Using single queues to simulate stack is cool. I guess this doesn't work when using stacks to simulate queues becasue 
you cannot put stuff into the bottom of the stack and only the top. 

# 225_ImplementStacksWithTwoQueues
Using single queues to simulate stack is cool. I guess this doesn't work when using stacks to simulate queues becasue
you cannot put stuff into the bottom of the stack and only the top.

# 232_ImplementQueuesWithTwoStacks
