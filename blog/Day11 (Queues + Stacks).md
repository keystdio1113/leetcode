# Problems
1. [20_ValidParenthesis](https://leetcode.com/problems/valid-parentheses/submissions/1204781979/)
2. [_1047_RemoveAllAdjacentDuplicatesInString](https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/)
3. [_150_ReversePolishExpression](https://leetcode.com/problems/evaluate-reverse-polish-notation/)

# Take away
Learn interesting ideas of using stack today. And the reverse polish expression has interesting connections
with binary trees.

# _20_ValidParenthesis
I know this is a classic. But I still Missed an edge case where the stack is empty and a closing characters is encountered

# _1047_RemoveAllAdjacentDuplicatesInString
So when seeing "Continuous doing X from point i", we could consider stack because the point i will always be available 
on top of the stacks for reference.

Also can't believe that I forgot I could prepend strings to create a reverse first.string from a stack

# 150_ReversePolishExpression

Whoa this is actually has something to do with a post order traversal of a binary tree. Natural
expression is the in-order traversal. 

Learned about that you can actually traverse the expression from beginning to end or the other direction. Both have pitfalls though.
1. From forward to end, the order of operands matter for the minus and divide operations
2. From end to forward, have to peek the stack two layers deep to see if we have two numbers to operate on.

I think the forward to end is easier to consume. 