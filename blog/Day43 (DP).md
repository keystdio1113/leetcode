# Problems
1. [Day43_1049_LastStoneII](https://leetcode.com/problems/last-stone-weight-ii/description/)
2. [Day43_494_TargetSum](https://leetcode.com/problems/target-sum/)
3. [Day43_474_OnesAndZeros](https://leetcode.com/problems/ones-and-zeroes/submissions/1234526922/)


Cannot believe that you could make these two problems into knapsack problems. But i think they follow some interesting pattern.
They have some sort of bi-partitioning pattern going on. Split into two disjoint but collectively exhaust subset and you could make it into knapsack

Also 01 knapsack can have two variations:
1. Find the maximum values
2. Find the number of ways to completely fill a knapsack
3. Knapsack with multiple types of weight. 

Be very careful about initial values here. Tripped me multiple time.