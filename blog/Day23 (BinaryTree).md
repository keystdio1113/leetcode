# Problems
1. [Day23_669_TrimBST](https://leetcode.com/problems/trim-a-binary-search-tree)
2. [Day23_108_ConvertSortedArrayToBST](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/description/)
3. [Day23_538_ConvertBSTToGreaterTree](https://leetcode.com/problems/convert-bst-to-greater-tree/description/)

# Day23_669_TrimBST
Solved it using a dumb way, traversing whole tree to discover every key to be removed, and used DeleteOneNode method from yesterday to delete them one by one.
But this actually could be done by using recursion, the key is to figure out that even though the node is out of bounds, its children could be within bound.

# Day23_108_ConvertSortedArrayToBST
Pretty simple, made some slight hiccups when building trees. Reviewed a bit on the interval definitions. Though I completely bypassed using `plus and divide 2` method that
could result in overflowing.

# Day23_538_ConvertBSTToGreaterTree
Solved it by summing everything first, and distribute it to every nodes inorder-ly

This is actually could be solved using two pointers like in calculating running sum in first.array, or finding mode in a sortedArray.
This question has similar solving techniques as in Day21_501_FindModeInBST