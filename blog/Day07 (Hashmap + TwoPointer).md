# Problems

2. [_454_FourSumII](https://leetcode.com/problems/4sum-ii/description/)
3. [_15_ThreeSum](https://leetcode.com/problems/3sum/description/)
1. [_18_FourSums](https://leetcode.com/problems/4sum/description/)
3. [_383_RansomNote](https://leetcode.com/problems/ransom-note/)

# Take away from today
These k-sums problem have devil-ish details in de-duplication and branch cutting

# ThreeSums/FourSums
These problems are devilish. But couple takeaways worthwhile to remember:
1. Sorting arrays make double pointer method possible as we could reliable shorten the window
2. De-duping solutions is pretty tricky here but require careful examination

# FourSumII
This is a simpler question than the other one K-sum problems because:
1. It only asked for unqiue count
2. It is working on different arrays so we don't have four pointers on the same arrays, makes life easier

But still this is fun as we are able to break one single 4-level-nested-for-loops into two 2-level-nested-for-loops   

# Ransom note
Review from yesterday, make use of character arrays.