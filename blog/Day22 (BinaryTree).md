# Problems
1. [Day22_235_LowestCommonAncestorBST](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/submissions/1214840590/)
1. [Day22_701_InsertIntoBST](https://leetcode.com/problems/insert-into-a-binary-search-tree/submissions/1214850921/)
2. [Day22_450_DeleteNodeInBST](https://leetcode.com/problems/delete-node-in-a-bst/submissions/1215097888/)

# Day22_235_LowestCommonAncestorBST
This is very similar to finding the largest smaller key in BST. It is trying your best to push down the pointer until you cannot anymore.
Learned how to do it recursively as well, though I am more comfortable with iterative

# Day22_701_InsertIntoBST
Did it independently, since we don't we need to care about balanced tree, it is actually pretty easy.

# Day22_450_DeleteNodeInBST
This is very interesting... I need to review how to remove BST node.

There are 5 scenario when encountering a node to delete:
1. Node to delete doesn't exist... easy, do nothing
2. Node to delete is a leaf node, just cut it
3. Node to delete has NULL left, non-NULL right, have parent's point to its non-NULL right
4. Node to delete has NULL right, non-NULL left, have parent's point to its non-NULL left
5. Node to delete has both left and right child, need some elaborations

For scenario 5, we could either the promote its left child or right child to be it.

If we are promoting the left child, we will
1. find the immediate predecessor (IP) of the node -> the right most node of the left subtree
2. Assign the right subtree of the node to be the right subtree of the IP
3. Return the left subtree to the node's parent.

If we are promoting the right child, we will
1. find the immediate successor (IS) of the node -> the left most node of the right subtree
2. Assign the left subtree of the node to be the left subtree of the IS
3. Return the right subtree to the node's parent.

Hard to get right when doing iteratively is to maintain a `previous` to denote the parent of the node we are trying to delete, like deleting first.linkedList node. 
It will break when we are deleting the root node ***(Use dummyRoot to handle this)*** and we need to identify parent is coming from the left or coming from the right.

But surprisingly recursive takes care of this as the call stack implicitly maintain this previous relationship and just a return at each stack should suffice.

Also I brilliantly invented dummyRoot to handle binary trees' previous value for value deletion. 