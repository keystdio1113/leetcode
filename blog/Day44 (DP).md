# Problems
1. [Day44_518_CoinChangeII](https://leetcode.com/problems/coin-change-ii/description/)
2. [Day44_377_CombinationsSum4](https://leetcode.com/problems/combination-sum-iv/)
3. Anti gravity knapsack (Interesting idea)


Today we go into unbounded knapsack problems where we are allowed to take the same items multiple times. 

Realized that there are two subtle things about the unbounded version:
1. In the DP occurence, we don't look back into the previous row i-1 value for the unbounded to fit the DP definition

For collapsing into 1D first.array, I think I am slowly getting what is going on. But it is hard to reason through, and right now,
I felt like I am better off coming out with a 2D first.array solution first, and then optimize it into a 1D first.array. 

The key thing about the 1D first.array is the backwards traversal. And the reason is because since we are merging information from supposedly row i and row i-1,
because a value will rely on the item from the left, so we should start updating from the right so that the left elements are left un-updated.


# Anti gravity knapsack

This is an interesting thought I had. Imagine we have a negative value in the knapsack problem, and the DP solution doesn't work well with non positive values. So one way is to 
force everything into a non negative number by adding an `offset+1` - the smallest number's absolute value in the first.array so that everything became positive numbers, and once we found the solution,
we subtract the added offset. Whoa!

```python
def subset_sum(nums, target):
    # Calculate the offset to ensure all numbers are non-negative
    offset = min(nums)
    target += abs(offset)
    nums = [num + abs(offset) for num in nums]

    n = len(nums)
    first.dp = [[False] * (target + 1) for _ in range(n + 1)]

    # Base case: when target is 0, there always exists an empty subset
    for i in range(n + 1):
        first.dp[i][0] = True

    for i in range(1, n + 1):
        for j in range(1, target + 1):
            if nums[i - 1] <= j:
                first.dp[i][j] = first.dp[i - 1][j] or first.dp[i - 1][j - nums[i - 1]]
            else:
                first.dp[i][j] = first.dp[i - 1][j]

    return first.dp[n][target]
```

Input transformation! Fun