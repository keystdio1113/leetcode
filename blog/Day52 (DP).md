# Problems
1. [Day52_300_LongestIncreasingSubSequence](https://leetcode.com/problems/longest-increasing-subsequence/submissions/1242173135/)
2. [Day52_674_LongestIncreasingSubArray](https://leetcode.com/problems/longest-continuous-increasing-subsequence/submissions/1242174944/)
3. [Day52_718_MaximumLengthOfRepeatedSubArray](https://leetcode.com/problems/maximum-length-of-repeated-subarray/submissions/1242215905/)

# Take away
Reviewed a bit what I learn in GATech. Return value is the maximum value across all the arrays. 