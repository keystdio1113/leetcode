# Problems
1. [Day60_647_PanlindromicSubstrings](https://leetcode.com/problems/palindromic-substrings/submissions/1250181173/)
2. [Day60_516_LongestPalindromicSubsequence](https://leetcode.com/problems/longest-palindromic-subsequence/)

Pretty interesting DP traversal that allows expanding from middle to both boundary, kinda like an invariant question Day02_977_SquaresOfSortedArray