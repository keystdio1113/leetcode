# Problems
1. [Day32_122_BestTimeBuyAndSellStock2](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/description/)
1. [Day32_55_JumpGame](https://leetcode.com/problems/jump-game/)
1. [Day32_45_JumpGame2](https://leetcode.com/problems/jump-game-ii/submissions/1225168819/)

# Day32_122_BestTimeBuyAndSellStock2
This is just looking for the ups and downs point. One neat trick I learned is that we could actually buy and sell the next day and it will be 
equivalent to buy-holdForACoupleDays-sell if we decide we should hold multiple days. Lead to easier code to write rather than maintaining two pointers

# Day32_55_JumpGame
Greedy actually has a faster runtime than DP. Though the idea is keep expanding the right most regions until it cannot be reach.

# Day32_45_JumpGame2
This one is hard. DP was doable too, but the first.greedy approach is to exploit the fact that it is guarenteed to be reachable. And we will:

```text
For input first.array [2,3,1,1,4]

(0-index below)
We know that the first jump will always land us up to index 2. So everything after index 2 must be on the second jump.

[X,X,X,X,X]
 1,1,1
       2,2    
``` 