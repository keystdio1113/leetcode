# Problems
1. [_654_MaximumBinaryTree](https://leetcode.com/problems/maximum-binary-tree/submissions/1212811229/)
2. [_617_MergeBinaryTree](https://leetcode.com/problems/merge-two-binary-trees/description/)
3. [Day20_700_SearchInBinaryTree](https://leetcode.com/problems/search-in-a-binary-search-tree/submissions/1212833809/)
4. [Day20_98_ValidateBST](https://leetcode.com/problems/validate-binary-search-tree/submissions/1212858786/)

# Take away
Binary Search tree (BST) definition has a little trap that could be easily looked over. It has to be "EVERY" descendent node below the current node is smaller/bigger, 
not just its direct children

# 654_MaximumBinaryTree
This was so much easier after doing constructing tree from various order

# 617_MergeBinaryTree
Pretty easy, learned how to do it modifying original tree or make new tree.

# Day20_700_SearchInBinaryTree
Basic 

# Day20_98_ValidateBST
Fell into trap of BST validations....