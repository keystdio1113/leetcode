# Objective Problems
1. [_704_BinarySearchBasic](https://leetcode.com/problems/binary-search/description/)
2. [_27_InPlaceRemoveElements](https://leetcode.com/problems/remove-element/description/)

# Optional Problems
1. LC34 (Application for binary search)
2. LC35 （Seem to deal with situation when target is not unique)

# Carl's reference:
1. https://docs.qq.com/doc/DUG9UR2ZUc3BjRUdY

# One thing to remember from today
Invariants are pretty important. Could be used in handling double pointers and binary search.

# Binary Search
This is a basic algorithm but ridiculously hard to get it right. When to use:
1. Array is sorted
2. Array elements is unique (If the target element not unique, there is no guarantee which one will be return, but one will be returned) 

There are 2 main key points to get binary search correct:

**1. Search interval invariant maintained while shrinking**

This means the while loop or recursive of the divide must keep on reducing the search space until not valid. This could be handled by using Carl's method of carefully defining the intervals.
There are two ways of defining search intervals and make sure the interval definition is an invariant. 

There are two types of interval mechanism, left-closed-right-closed (LCRC), and left-closed-right-opened (LCRO).


For LCRC, loop termination is `while(l <= r)` since`[1,1]` is the smallest valid interval. At each search steps, we will create 3 intervals:
`[0, m-1], m, [m+1, r]`. Note the `m-1` and `m+1` ensures that the search space steadily decreases in size.  

For LCRO, loop termination is `while(l < r)` since `[1,2)` is the smallest valid interval. At each search steps, we will create 3 intervals:
`[0, m), m, [m+1, r)`. Note the `m` and `m+1` ensures that the search space steadily decreases in size.
 
**2. Calculating the midpoint needs to avoid overflowing**

This could be done with `m = l + (r-l)/2` rather than `m = (l+r) / 2`

**3. divide by 2 with potential 1-off doesn't matter**
As long as it is dividing by 2, we get the effect of shrinking subproblems by half. And we define the interval correctly then we are good. The `mid` 
doesn't matter whether it is floored or not.

# Double pointers

Today I dipped into double pointer programmings. Takeaways

**Have to understand what each pointer stands for before using double pointers**

In LC27, the fast pointer is trying to collect the valid elements in the first.array, the slow pointer indicates the edge of the
arrays that is all valid elements. So the slow pointer location will sometime get elements from the fast pointer and updates itself.

This kinda problems are tricky in:
1. Fence post style error. I made mistakes in the final return value as the slow is already moving ahead. But if we keep the invariant throughout the loops, it should be pretty clear what the end value is.
