# Problems
1. [_513_BottomLeftTreeValue](https://leetcode.com/problems/find-bottom-left-tree-value/)
2. [_112_PathSum](https://leetcode.com/problems/path-sum/submissions/1211008896/)
3. [_113_PathSumII](https://leetcode.com/problems/path-sum-ii/submissions/1211019616/)
4. [_105_ConstructTreeWithInAndPreOrder](https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/submissions/1211077310/)
5. [_106_ConstructTreeWithInAndPostOrder](https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/)

# Take away
1. Sometimes it doesn't matter if we are using pre/in/post order traversal, usually as long as we are not modification to the tree, it doesn't matter.
2. Constructing a tree from arrays need specific guidelines. Take a look at 105 or 106 comments to see how it is done.

# _513_BottomLeftTreeValue
Should have realized that it doesn't matter pre/in/post, because the left most child of the next level is always the first to encounter when
we are traversing downwards.

# _112_PathSum and _113_PathSumII
+1 and -1 (backtracking) everytime going down the recursive level if there is a shared object with temporary information

# 105_ConstructTreeWithInAndPreOrder and 106_ConstructTreeWithInAndPostOrder
Must understand the general mind set first and then take care of the closing or opening bracket of the traversal. 