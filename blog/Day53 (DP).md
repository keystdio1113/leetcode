# Problems
1. [Day53_1143_MaximumLengthOfRepeatedSubSequence](https://leetcode.com/problems/longest-common-subsequence/)
2. [Day53_1035_UncrossedLine](https://leetcode.com/problems/uncrossed-lines/submissions/1245124229/)
3. [Day53_MaximumSubArray](https://leetcode.com/problems/maximum-subarray/description/)

Takeaway:
1. I still don't quite understand the recurrence relationship for longest common subsequence, especially the part where the new elements are different. I understand that it is still using "ending at index i" format though

