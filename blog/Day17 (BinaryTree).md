# Problems
1. [_110_BalancedBinaryTree](https://leetcode.com/problems/balanced-binary-tree/)
2. [_257_binaryTreePaths](https://leetcode.com/problems/binary-tree-paths/)
3. [_404_SumOfLeftLeaves](https://leetcode.com/problems/sum-of-left-leaves/submissions/1210160347/)

# Takeaways
1. We are starting to get into backtracking, that we need to do one in and one out type of recursions
2. Returning non valid number is also a way of keeping track of information (_110 to return unbalanced tree)