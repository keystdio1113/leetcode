# Problems
1. [Day28_93_RestoreIPAddress](https://leetcode.com/problems/restore-ip-addresses/)
2. [Day28_78_Subsets](https://leetcode.com/problems/subsets/submissions/1220628187/)
3. [Day28_90_SubsetsII](https://leetcode.com/problems/subsets-ii/)

# Take away
1. Really draw the tree graph since I am not too familiar with it.
2. `i` in the for loop and `startIndex` are so easy to get mixed up.

# Note: recursion tree graph and notes are in the `.drawio` files in this repo.