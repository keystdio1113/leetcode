# Objective Problems
1. [_24_ReversePairs](https://leetcode.com/problems/swap-nodes-in-pairs/description/)
1. [_19_RemoveNthElement](https://leetcode.com/problems/remove-nth-node-from-end-of-list/description/)
1. [_160_intersectionOfList](https://leetcode.com/problems/intersection-of-two-linked-lists/description/)
2. [_142_LinkedListCycleII](https://leetcode.com/problems/linked-list-cycle-ii/description/)

# One take away
Whoa, finding a cycle in a LinkedList AND finding the entry point of the cycle, amazing day!

# Linked list cycle II

Really like Carl using relative speed to prove that the fast pointer will always catch up with the slow pointer. This is kind of like relative speed in physics.
If there is a loop, fast pointer and slow pointer will eventually forever cycle in the loop with fast pointer faster than slow pointer. Since `fast` is exactly
one unit faster than `slow`, `fast` will always approach `slow` in a relative speed of 1, and it is guarentee to meet up since the distance to it is discrete. However
if `fast` is 2 or more unit faster than `slow`, depending on the loop size and time difference both pointers enter the loop, `fast` might never catch up with `slow`.

But anyway, finding the entrance of the loop is quite a bit of math derivation. And let me see if I could derive it again LOL...

We denote couple things:
1. Distance from Linked List head to entrance of loop is x
2. Distance from entrance of loop to point where `slow` and `fast` intersect is y
3. Distance from the intersection point to entrance of loop to be z

We have relationship when `fast` and slow intersects
1. The distance `fast` go through = x + n*(y+z) + y, n denotes how many times `fast` go through in the loop
2. The distance `slow` go through = x + m*(y+z) + y, m denotes how many times `slow` go through in the loop
3. We also know that `fast` walks twice as fast as `slow`, n >= 1, n > m

Hence we have:
`x + n*(y+z) + y = 2[x + m*(y+z) + y]`

With some math since we are solving for x

```
x + n(y+z) + y = 2x + 2m(y+z) + 2y
-x = (2m-n)(y+z) + y 
x = (n-2m)(y+z) - y
x = (n-2m-1)(y+z) + z (this minus 1 trick here is a key)
```

Note that we could prove that m = 0 by the following. Imagine we have the following diagram:

```
  |-----------o---------|--------o------------|
  A           K         B        I            C
```

The diagram above "straighten the cycle in a linked list". AB will be one cycle traversal, BC will be the consequent cycle traversal.

Imagine when `slow` enter the cycle at point B, `fast` will be at point K for sure. We know that:
1. `fast` will always intersect `slow` at point I since their relative approach speed is 1
2. If we denote distance KB = k, and the cycle length as d, we know that when `fast` reach point C, `slow` would have travel distance (k+d)/2 since it is half the speed of `fast`. This distance is definitely less than d since k < d.

These two insights means that `slow` did not finish a full cycle by the time `fast` finish a cycle after `slow` enter the loop, hence in the above equation m = 0.

Continue with equation, we have the following important relation between x and z.
`x = (n-1)(y+z) + z`

**[Important trick]** this equation means that if we start an index1 at head of list, and start an index2 at the intersection point,
move them concurrently with the same speed of 1. They will meet at the entrance of the cycle! Note that we have n>=1, so we don't care about how many
times index2 will go through the cycle, it will still meet index1 at the entrance of the cycle.


# Reverse pairs

Pretty familiar with this. Have to say the pointer manuevers was a bit rough. Also forget to take care of NPE when there is odd number of elements and the 
last pair of elements is not complete

# Removed Nth last element

This is a very classic double pointer solutions. N-th last means double pointers exactly N step away from each other. Once we know this, just need to slow down
and check if the pointers are at the index we want.

# Intersection Of List

This is kinda fun. Able to figure out to use double pointers to save space as long as we could advance them together.