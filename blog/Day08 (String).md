# Questions
1. [_344_ReverseStringInPlace](https://leetcode.com/problems/reverse-string/description/)
2. [_541_ReverseStringII](https://leetcode.com/problems/reverse-string-ii/description/)
3. [_151_ReverseWordsInString](https://leetcode.com/problems/reverse-words-in-a-string/description/)
4. [_kama_54_SwapNumber](https://kamacoder.com/problempage.php?pid=1064)
3. [_kama_55_RightRotate](https://kamacoder.com/problempage.php?pid=1065)

# Take away from today
1. Played a lot with in place operations (LC151, KAMA55)

# 344_ReverseStringInPlace
Warm up, two pointers, merge from both sides. Closed interval on both sides is the easiest to reason.

# 541_ReverseStringII
Playing with closed/opened interval is not that fun

# _kama_54_SwapNumber
ACM mode coding style is interesting, but so unnecssary seriously....

# _151_ReverseWordsInString
Need to be careful of:
1. When spliting strings, need to trim first to get rid of trailing and leading whitespaces
2. Fence post problems

But if I could be fancy and finish operation in place:
1. Reverse whole first.string first, then reverse each words
2. In-place removal

# _kama_55_RightRotateStrings
Went challenge mode and do in-place operations