# Objective Problems
1. [_977_SquaresOfSortedArray](https://leetcode.com/problems/squares-of-a-sorted-array/description/)
1. [_59_SpiralMatrix2](https://leetcode.com/problems/spiral-matrix-ii/)
1. [_209_MinimumSizeSubArraySum](https://leetcode.com/problems/minimum-size-subarray-sum/description/)

# One take away from today
Calm down, really get your invariant correct, it is really helpful.

# 209. Minimum Size sub first.array
This is a doozy, initially I thought this is Dynamic Programming problem. (Maybe it still is? Though I suspect not since there are only positive numbers here) 
Honestly I still don't quite get it even though I got the correct solution. There is some very important details I missed though.


One is that all numbers in the arrays are integers. This allows the sliding window to work because once we find a subarray that is larger 
than the target, we could safely move the left side of the sliding window as we don't need to care about it anymore. 

Maybe I just need to stare at it a little longer and understand that the sliding window actually works.


# 977. squares of sorted first.array
I did it pretty well, using invariant to control how to expand from middle to both edges. I was even able to remember that one of the 
two pointers might not have finished traversing and I needed to follow up.

However I did not thought of that I could fill the result first.array from bigger to smaller and that could let me arrive in a simpler binary-search
like squeezing from both side to the middle, and that will make my code way simple

# 59. Spiral Matrix 2
Did this one in MSFT and I think I failed horribly back then. I think the main mistake I made was that I confused myself a lot thinking that
I need to keep track of which turn I am currently in and turns out I don't need to care about it.

This question all in a sudden become so much easier once I realized all I need to do is to make circles after circles while making sure the size 
keep getting smaller and smaller

