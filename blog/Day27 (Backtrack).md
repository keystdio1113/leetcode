# Problems

1. [Day27_39_CombinationSum]()
2. [Day27_40_CombinationSumII]()
3. [Day27_131_PalindromPartition]()

# Day27_39_CombinationSum

Pretty straight forward, just makes sure the startI is set to be repeatable on the same element and not moving forward.

# Day27_40_CombinationSumII 
Learn new things about combination sum dedupping. There are two style of dedupping:
1. Dedupping in branch
2. Dedupping in level

# Day27_131_PalindromPartition
I was able to do it using the match stick analogy, but the actual way seems like cheating but kinda hard to understand.